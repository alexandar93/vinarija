import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

declare var $:any;

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
    constructor(private elRef:ElementRef, private router: Router, public translate: TranslateService) {

        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('rs');

         // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use('rs');
    }
    ngOnInit(){
        let body = document.getElementsByTagName('body')[0];
        var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
        if (isWindows){
           // if we are on windows OS we activate the perfectScrollbar function
            body.classList.add("perfect-scrollbar-on");
        } else {
            body.classList.add("perfect-scrollbar-off");
        }
        $.material.init();
        
       
        this.router.events
        .filter(event => event instanceof NavigationEnd)
        .subscribe((event: NavigationEnd) => {
            const container = document.querySelector('.main-panel');
            if(container !== null) {
                container.scrollTop = 0;
            }
        });
    }
}
