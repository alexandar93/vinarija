import {NgModule, InjectionToken} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {HttpModule, Http} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';

import {SidebarModule} from './sidebar/sidebar.module';
import {FooterModule} from './shared/footer/footer.module';
import {NavbarModule} from './shared/navbar/navbar.module';
import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';
import {AppRoutes} from './app.routing';
import {HttpService} from './services/http.service';
import {LocalStorageService} from './services/localstorage.service';
import {Constants} from './services/endpoint.service';
import {NotificationsService} from './notifications/notifications.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { AgmCoreModule, InfoWindowManager, GoogleMapsAPIWrapper, MarkerManager } from "@agm/core";
import { AuthGuardService } from './auth/auth-guard.service';
import { RoleGuardService } from './auth/role-guard.service';
import { AuthService } from './auth/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { ImageVideoValidatorsService } from './services/image-video-validators.service';
import { Page404Component } from './page404/page404.component';
import { HelperService } from './services/helper.service';
import { Globals } from './model/globals';
import { NgxUploaderModule } from 'ngx-uploader';
import { GalleryModule } from '@ngx-gallery/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateHelperService } from './services/translate-helper.service';
import { CustomTranslateLoader } from './services/CustomTranslateLoader';


// import { DataTablesModule } from "angular-datatables";

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule.forRoot(AppRoutes),
        HttpModule,
        HttpClientModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        NgxUploaderModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useClass: CustomTranslateLoader,
                deps: [Http]
            }
        }),
        GalleryModule.forRoot(),
        // DataTablesModule,
        AgmCoreModule.forRoot({
            apiKey: "AIzaSyC-RpJTVJRs0GxeYnTz2baSNdYHSFaLsdw",
            libraries: ["places"]
        }),

    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
        Page404Component
    ],
    providers: [
        HttpService,
        LocalStorageService,
        InfoWindowManager,
        GoogleMapsAPIWrapper,
        MarkerManager,
        Constants,
        NotificationsService,
        HelperService,
        ImageVideoValidatorsService,
        Globals,
        AuthGuardService,
        AuthService,
        RoleGuardService,
        TranslateHelperService,
        {provide: LocationStrategy, useClass: HashLocationStrategy}
        ],
        // exports: [
        // TranslateModule
        // ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
