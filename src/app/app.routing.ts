import { Routes, CanActivate } from "@angular/router";

import { AdminLayoutComponent } from "./layouts/admin/admin-layout.component";
import { AuthLayoutComponent } from "./layouts/auth/auth-layout.component";
import { UsersModule } from "./users/users.module";
import { LoginComponent } from "./login/login.component";
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { RoleGuardService as RoleGuard } from './auth/role-guard.service';
import { Page404Component } from "./page404/page404.component";

export const AppRoutes: Routes = [
  {
    path: "",
    redirectTo: "dashboard",
    pathMatch: "full",
    canActivate: [AuthGuard]
  },
  {
    path: "",
    component: AdminLayoutComponent,
    children: [
      {
        path: "",
        loadChildren: "./dashboard/dashboard.module#DashboardModule",
        canActivate:  [AuthGuard]
      },
      {
        path: "components",
        loadChildren: "./components/components.module#ComponentsModule"
      },
      {
        path: "forms",
        loadChildren: "./forms/forms.module#Forms"
      },
      {
        path: "tables",
        loadChildren: "./tables/tables.module#TablesModule"
      },
      {
        path: "maps",
        loadChildren: "./maps/maps.module#MapsModule"
      },
      {
        path: "widgets",
        loadChildren: "./widgets/widgets.module#WidgetsModule"
      },
      {
        path: "calendar",
        loadChildren: "./calendar/calendar.module#CalendarModule"
      },
      {
        path: "",
        loadChildren: "./userpage/user.module#UserModule"
      },
      {
        path: "",
        loadChildren: "./timeline/timeline.module#TimelineModule"
      },
      {
        path: "users",
        loadChildren: "./users/users.module#UsersModule",
        canActivate: [AuthGuard]
      },
      {
        path: "winery",
        loadChildren: "./winery/winery.module#WineryModule",
        canActivate: [AuthGuard],
      },
      {
        path: "wines",
        loadChildren: "./wines/wines.module#WinesModule",
        canActivate: [AuthGuard]
      },
      {
        path: "events",
        loadChildren: "./events/events.module#EventsModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          expectedRole: 'admin'
        }
      },
      {
          path: 'wine-paths',
          loadChildren: './wine-path/wine-path.module#WinePathModule',
          canActivate: [AuthGuard]
      },
      {
          path: 'poi',
          loadChildren: './poi/poi.module#PoiModule',
          canActivate: [AuthGuard, RoleGuard],
          data: {
            expectedRole: 'admin'
          }
      },
      {
        path: "article",
        loadChildren: "./article/article.module#ArticleModule",
        canActivate: [AuthGuard, RoleGuard],
        data: {
          expectedRole: 'admin'
        }
      },
      {
        path: 'rate',
        loadChildren: './rate/rate.module#RateModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'push-notification',
        loadChildren: './push-notification/push-notification.module#PushNotificationModule',
        canActivate: [AuthGuard, RoleGuard],
        data: {
            expectedRole: 'admin'
          }
      },
      {
        path: 'create-region',
        loadChildren: './create-region/create-region.module#CreateRegionModule',
        canActivate: [AuthGuard, RoleGuard],
        data: {
            expectedRole: 'admin'
          }
      },
      {
        path: 'create-winesort',
        loadChildren: './create-winesort/create-winesort.module#CreateWinesortModule',
        canActivate: [AuthGuard, RoleGuard],
        data: {
            expectedRole: 'admin'
          }
      },
      {
        path: 'create-type',
        loadChildren: './create-type/create-type.module#CreateTypeModule',
        canActivate: [AuthGuard, RoleGuard],
        data: {
            expectedRole: 'admin'
          }
      },
      {
        path: 'create-language',
        loadChildren: './create-language/create-language.module#CreateLanguageModule',
        canActivate: [AuthGuard, RoleGuard],
        data: {
            expectedRole: 'admin'
          }
      }
    ] 
  },
  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      {
        path: "login",
        loadChildren: "./login/login.module#LoginModule"
      }
    ]
  },
  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      {
        path: "pages",
        loadChildren: "./pages/pages.module#PagesModule"
      }
    ]
  },
  {
    path: '**',
    component: Page404Component
  }
];
