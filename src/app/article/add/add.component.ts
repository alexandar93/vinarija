import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { NotificationsService } from '../../notifications/notifications.service';
import { ImageVideoValidatorsService } from '../../services/image-video-validators.service';
import { isTemplateExpression } from 'typescript';
import { Globals } from './../../model/globals';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var swal: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  /** variables */
  subscriptionData: Subscription;
  subscriptionLangs: Subscription;

  articleForm: FormGroup;
  languages: any[] = [];
  langs: any[] = [];
  defaultLanguage: number = 1;
  isFormValid = true;
  coverFile: any;
  defaultLangs: any = {
    id: '',
    name: ''
  };
  modules: any;
  urlPattern = "https?://.+"; // pattern for web site url

  /** translate strings */
  msg_server_error: string;
  msg_file_is_not_image: string;
  msg_file_is_not_format: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_success_created: string;
  msg_invalid_image: string;

  @ViewChild("f") myNgForm: any;
  @ViewChild("coverPath") coverPath: any;

  constructor(
    private http: HttpService,
    private fb: FormBuilder,
    private alert: NotificationsService,
    private validate: ImageVideoValidatorsService,
    private globals: Globals,
    private translate: TranslateHelperService
  ) { 
    this.modules = this.globals.editor_settings;
  }

  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_file_is_not_format = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('ARTICLES_ALERT_MSG_SUCCESS_CREATED').then((val: string) => {
      this.msg_success_created = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_INVALID_IMAGE').then((val: string) => {
      this.msg_invalid_image = val;
    });

    this.subscriptionLangs = this.http.get('dropdown/language', this.defaultLanguage).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        this.langs = httpResponse.json();
        this.langs.forEach(item => {
          if(item.id == this.defaultLanguage) {
            this.defaultLangs.id = item.id;
            this.defaultLangs.name = item.name;
          }
        });
        this.addItem(this.defaultLangs);
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });

    this.articleForm = this.fb.group({
      link: [{value:'', disabled: true}, Validators.compose([Validators.required, Validators.pattern(this.urlPattern)])],
      items: this.fb.array([]),
      languages: [""]
    });
  }
  // creating formGroup when pick language from dropdown
  createLanguage(languageId, languageName): FormGroup {
    return this.fb.group({
      name: ["", Validators.required],
      text: [{ value: "", disabled: false }, Validators.required],
      language: [languageId],
      language_name: [languageName]
    });
  }
  // adding new created language in FormArray
  addItem(value): void {
    let items = this.articleForm.get("items") as FormArray;
    items.push(this.createLanguage(value.id, value.name));
    // let index = this.langs.indexOf(value, 0);
    let index = this.langs.findIndex(e => e.id==value.id);
    this.langs.splice(index, 1);
  }
   // uploading image storage
   onUploadCover(event) {
    let file = <File>event.target.files[0];
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_file_is_not_image, 'danger', '');
      this.isFormValid = false;
      return false;
    }
    if(!this.validate.validateImage(file.name)) {
      this.alert.showNotification(this.msg_file_is_not_format, 'danger', '');
      this.isFormValid = false;
      return false;
    } else {
      this.isFormValid = true;
      this.coverFile = <File>event.target.files[0];
    }
  }
  // removing language
  onRemoveLangs(languageName, languageId, index) {
    let selected = this.articleForm.get("items") as FormArray;

    swal({
      title: this.msg_delete_lang_title + ` ${languageName}`,
      text: this.msg_delete_lang_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        selected.removeAt(index);
       swal({
          title: this.msg_delete_lang_success_title,
          text: `${languageName} ` + this.msg_delete_lang_success_text,
          type: "success",
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        });
        let renewLang = {
          name: languageName,
          id: languageId
        };
        this.langs.push(renewLang);
      }, (dismiss) => {

      }
    );
  }
  textCheckboxStatus(event, i) {
    let select = this.articleForm.get('items') as FormArray;
    let selectGroup = select.controls[i] as FormGroup;
    if(event.checked == true) {
      selectGroup.controls.text.disable();
    }
    if(event.checked == false) {
      selectGroup.controls.text.enable();
      this.articleForm.controls.link.disable();
    }
    select.controls.forEach(item => {
      let selectItem = item as FormGroup;
      if(selectItem.controls.text.disabled === true) {
        this.articleForm.controls.link.enable();
      }
    });
  }
  resetForm() {
    this.myNgForm.resetForm();
  } 
  onSubmit() {
    const fd: FormData = new FormData();
    let langForm = this.articleForm.get("items") as FormArray;
    let languages: any[] = [];

    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      if(formGroup.controls.text.disabled !== true) {
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "text",
        value: formGroup.controls.text.value
      });
    }
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "name",
        value: formGroup.controls.name.value
      });
    });
    this.articleForm.controls.languages.setValue(languages);
    let formInput = this.articleForm.value;
    if(this.articleForm.controls.link.disabled === true) {
      delete formInput.link;
    }
    delete formInput.items;
    if (this.coverFile !== undefined) {
      fd.append("cover", this.coverFile);
    }
    fd.append("json", JSON.stringify(formInput));
    if(this.isFormValid) {
       this.http.postFormData('create/article', fd).subscribe(httpResponse => {
         if(httpResponse.status == 201) {

          this.http.get('dropdown/language', 1).subscribe(lang => {
            if(lang.status == 200) {
              this.langs = lang.json();
              while (langForm.length !== 0) {
                langForm.removeAt(0);
              }
              this.addItem(this.defaultLangs);
            }
         
          });
          this.coverFile = null;
          languages = [];
           this.resetForm();
           this.coverPath.nativeElement.click();
           this.alert.showNotification(this.msg_success_created, 'success', '')
         }

       }, err => {
         this.alert.showNotification(this.msg_server_error, 'danger', '');
       })
    } else {
      this.alert.showNotification(this.msg_invalid_image, 'danger', '');
    }


  }
}
