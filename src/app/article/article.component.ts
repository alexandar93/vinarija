import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { SelectionModel, DataSource } from "@angular/cdk/collections";
import { HttpService } from '../services/http.service';
import { ArticleDataSource } from '../services/article.datasource';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { NotificationsService } from '../notifications/notifications.service';
import { Article } from './article.model';
import { TranslateHelperService } from './../services/translate-helper.service';

declare var swal;
declare var $;

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, OnDestroy {
  //variables
  subscriptionData: Subscription;
  subscriptionLangs: Subscription;
  languages: any[] = [];
  selectedLanguage: number;
  defaultLanguage: number = 1;
  total: number;
  pageSize: number;
  articleRow: Article;
  sort: string = 'asc';

  dataSource: ArticleDataSource;
  displayedColumns = ['created_at', 'name', 'text', 'link', 'actions'];

  /** translate strings */
  msg_server_error: string;
  msg_delete_article_title: string;
  msg_delete_article_text: string;
  msg_delete_article_success_title: string;
  msg_delete_article_success_text: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_delete_unsuccess: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private router: Router,
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {

    // translate lable for pagination
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('ARTICLES_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_delete_article_title = val;
    });
    this.translate.getTranslate('ARTICLES_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_delete_article_text = val;
    });
    this.translate.getTranslate('ARTICLES_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_article_success_title =  val;
    });
    this.translate.getTranslate('ARTICLES_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_article_success_text = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes =  val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('ARTICLES_ALERT_MSG_DELETE_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    })

    //
    this.dataSource = new ArticleDataSource(this.http);
    this.dataSource.loadArticle(1, this.sort, this.defaultLanguage);
    // languages
    this.selectedLanguage = this.defaultLanguage;
    this.subscriptionLangs = this.http.get('dropdown/language', this.defaultLanguage).subscribe(httpResponse => {
        this.languages = httpResponse.json();
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })
    // data for pagination
    this.subscriptionData = this.http.get('get/article', this.selectedLanguage).subscribe(httpResponse => {
      this.total = httpResponse.json().total;
      this.pageSize = httpResponse.json().per_page;
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })
  }
  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.LoadArticlePage())
    ).subscribe();
  }
  LoadArticlePage() {
    this.dataSource.loadArticle(
        this.paginator.pageIndex + 1, this.sort, this.selectedLanguage);
        // console.log('loadWineryPage triggered!: ', this.paginator.pageIndex);
  }
  openSelectedArticle(event, row) {
    // disable to click on button for delete and open modal
    if(event.target.nodeName == 'a') return false; 
    if(event.target.nodeName == 'I') return false;
    if(event.target.nodeName == 'BUTTON') return false; 
    // open modal
    $('.modal').modal({
    show: 'true'
    }); 
    // set value for intefrace commentRow
    this.articleRow = {
      name: row.name,
      text: row.text,
      image_path: row.image_path,
      link: row.link,
      created_at: row.created_at,
    }
  }

  sortData(event) {
    if(event.direction == 'asc') {
      this.sort = event.direction;
      this.loadDataSource();
    }
    if(event.direction == 'desc') {
      this.sort = event.direction;
      this.loadDataSource();
    }
  }

  loadDataSource() {
    this.dataSource.loadArticle(this.paginator.pageIndex, this.sort, this.selectedLanguage);
    this.paginator.firstPage();
  }
  onChangeLanguage(id: number, name: string) {
    // console.log(id, name);
    this.selectedLanguage = id;
    this.dataSource = new ArticleDataSource(this.http);
    
    this.http
      .get("get/happening", this.selectedLanguage)
      .subscribe(httpResponse => {
        // console.log(httpResponse.json());
        if(httpResponse.status === 200) {
            this.total = httpResponse.json().total;
            this.pageSize = httpResponse.json().per_page;
           }
        });
  }
  onAddArticle() {
    this.router.navigate(["article/add"]);
  }

  onEditArticle(id) {
    this.router.navigate(["article/edit", id]);
  }
  onDeleteArticle(id, name) {
    swal({
      title: this.msg_delete_article_title + ` ${name} ?`,
      text: this.msg_delete_article_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(() => {
      this.http.delete("delete/article/" + id).subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          swal({
            title: this.msg_delete_article_success_title,
            text: `${name} ` + this.msg_delete_article_success_text,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          this.http.get('get/happening', this.selectedLanguage).subscribe(data => {
            this.paginator.length = data.json().total;
          })
          
          this.dataSource = new ArticleDataSource(this.http);
          this.loadDataSource();
        }
        error => {
          this.alert.showNotification(
            this.msg_delete_unsuccess,
            "danger",
            "error"
          );
        };
      });
    }, (dismiss) => {
      
    });
  }
  ngOnDestroy() {
    this.subscriptionData.unsubscribe();
    this.subscriptionLangs.unsubscribe();
  }
}
