export interface Article {
    name: string,
    text: string,
    image_path: string,
    link: string,
    created_at: string
}