import { Routes } from '@angular/router';

import { ArticleComponent } from './article.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';

export const ArticleTable : Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: ArticleComponent
        }, 
        {
            path: 'edit/:id',
            component: EditComponent,
        }, 
        {
            path: 'add',
            component: AddComponent,
            pathMatch: 'full'
        }]
    }
];

