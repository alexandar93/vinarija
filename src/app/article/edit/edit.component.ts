import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Subscription } from 'rxjs';
import { NotificationsService } from '../../notifications/notifications.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ImageVideoValidatorsService } from '../../services/image-video-validators.service';
import { ActivatedRoute, Router, ActivationStart } from '@angular/router';
import { TranslateHelperService } from './../../services/translate-helper.service'; 
import { Globals } from '../../model/globals';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

declare var swal: any;
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, OnDestroy {
  //variables
  subscriptionLangs: Subscription; // subscription for languages
  subscriptionParams: Subscription; // subsciption for params
  articleForm: FormGroup; // formGroup
  defaultLanguage: number = 1; // default langauge Srpski id = 1
  langs: any[] = []; // languages
  isFormValid = true;
  coverFile: any; // cover image 
  id: number; // id for selected article
  coverImage: any = null; // storage image for initialize
  urlPattern = "https?://.+"; // pattern for web site url
  modules: any;

  /** translate strings */
  msg_server_error: string;
  msg_file_is_not_image: string;
  msg_file_is_not_format: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_success_edited: string;
  msg_invalid_image: string;
  msg_success_saved_lang: string;
  msg_save_previous_lang: string;
  msg_delete_unsuccess: string;

  constructor(
    private http: HttpService,
    private alert: NotificationsService,
    private fb: FormBuilder,
    private validate: ImageVideoValidatorsService,
    private route: ActivatedRoute,
    private router: Router,
    private globals: Globals,
    private translate: TranslateHelperService
  ) { 
    this.modules = this.globals.editor_settings;
  }

  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_file_is_not_format = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('ARTICLES_ALERT_MSG_SUCCESS_CREATED').then((val: string) => {
      this.msg_success_edited = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_INVALID_IMAGE').then((val: string) => {
      this.msg_invalid_image = val;
    });
    this.translate.getTranslate('LANG_ALERT_REQ_SAVE_PREVIOUS_LANG').then((val: string) => {
      this.msg_save_previous_lang = val;
    });
    this.translate.getTranslate('LANG_ALERT_SUCCESS_SAVED').then((val: string) => {
      this.msg_success_saved_lang = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });

    this.articleForm = this.fb.group({
      link: [{value:'', disabled: false}, Validators.compose([Validators.required, Validators.pattern(this.urlPattern)])],
      items: this.fb.array([]),
      languages: [""]
    });
    this.subscriptionLangs = this.http.get('dropdown/language', this.defaultLanguage).subscribe(httpResponse => {
      if(httpResponse.status == 200) {
        this.langs = httpResponse.json();
        this.initParamsData();
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });
   
  }
  initParamsData() {
  // get get url params for event ID
  this.subscriptionParams = this.route.params.subscribe(
  params => {
      this.id = params.id;
      this.removeItem(); 
      this.initLoadingData();
  });
  }
   // adding new item to FormArray items and splice item from langsArray
   addItem(value): void {
    let isSaved;
    let items = this.articleForm.get("items") as FormArray;
    items.controls.forEach(item => {
      let items = item as FormGroup;
      if (items.controls.flag.value === 1) {
        isSaved = true;
        swal({
          title: this.msg_save_previous_lang,
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success"
      });
      return;
      }
      isSaved = false;
    });
    if (!isSaved) {
      items.push(
        this.createLanguage(value.id, null, "", "", value.name, value.id)
      );
      let index = this.langs.indexOf(value, 0);
      this.langs.splice(index, 1);
    }
  }
  // data for initializing form
  initLoadingData(onSave = false) {
    this.http.get('patch/initialize/article/' + this.id, this.defaultLanguage).subscribe(httpResponse => {
      if(httpResponse.status == 200) {
        let serverData = httpResponse.json();
        this.articleForm.controls.link.disable();
        if(serverData.link) {
          this.articleForm.controls.link.setValue(serverData.link);
          this.articleForm.controls.link.enable();
        }
        this.coverImage = serverData.image_path;
        serverData.languages.forEach((lang, langIndex) => {
          let name = "";
          let text = "";
          let fieldsindex;
          let name_id;
          let text_id;
  
          lang.fields.forEach((field, fieldIndex) => {
            fieldsindex = fieldIndex;
            if (field.name === "name") {
              name = field.value;
              name_id = field.id;
            }
            if (field.name === "text") {
              text = field.value;
              text_id = field.id;
            }
          });
  
          if(!onSave) {
            var index = this.langs.findIndex(
              item => item.name === lang.language
            ); // SELECTING INDEX OF OBJECT IN ARRAY BY PROPERTY *(etc. name)
            this.langs.splice(index, 1);
            }
          let language_name = lang.language;
          let language_id = lang.language_id;
          this.createItem(
            name_id,
            text_id,
            name,
            text,
            language_name,
            language_id,
            false
          );
        });
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })
  }
   // remove all from items formArray
removeItem() {
  let controls = this.articleForm.get('items') as FormArray;
  while (controls.length !== 0) {
  controls.removeAt(0);
  }
}

// creating formGroup when pick language from dropdown
createLanguage(nameId, textId, articleName, articleText, langName, langId, isNew = true): FormGroup {
  let fg = this.fb.group({
    name: [articleName, Validators.required],
    text: [{ value: articleText, disabled: false }, Validators.required],
    name_id: nameId,
    text_id: textId,
    language_name: [langName],
    language_id: [langId],
    flag: ['']
  });
  if (isNew) fg.controls['flag'].setValue(1);
  return fg;
}
// generating new languages with data from server
createItem(nameId, textId, articleName, articleText, langName, langId, isNew = true): void {
  let items = this.articleForm.get("items") as FormArray;
  items.push(this.createLanguage(nameId, textId, articleName, articleText, langName, langId, isNew));
}

// saving when add new language
onSaveLanguage(value) {
  let languageFormFields: any[] = [];
  console.log(value)
  if(value.controls.text.disabled !== true) {
  languageFormFields.push({
    language_id: value.controls.name_id.value,
    name: "text",
    value: value.controls.text.value
  });
}
  languageFormFields.push({
    language_id: value.controls.name_id.value,
    name: "name",
    value: value.controls.name.value
  });
  let postData = {
    languages: languageFormFields
  };
  this.http
    .post("add/language/article/" + this.id, postData)
    .subscribe(httpResponse => {
      if (httpResponse.status === 204) {
        let itemArray = this.articleForm.controls['items'] as FormArray;
        itemArray.controls.forEach(element =>{
          element.markAsUntouched;
        });
        this.alert.showNotification(
          this.msg_success_saved_lang,
          "success",
          "notifications"
        );
        this.removeItem()
        this.initLoadingData(true);
      }
    });
}
textCheckboxStatus(event, i) {
  let select = this.articleForm.get('items') as FormArray;
  let selectGroup = select.controls[i] as FormGroup;
  if(event.checked == true) {
    selectGroup.controls.text.disable();
  }
  if(event.checked == false) {
    selectGroup.controls.text.enable();
    this.articleForm.controls.link.disable();
  }
  select.controls.forEach(item => {
    let selectItem = item as FormGroup;
    if(selectItem.controls.text.disabled === true) {
      this.articleForm.controls.link.enable();
    }
  });
}
   // uploading image storage
   onUploadCover(event) {
    let file = <File>event.target.files[0];
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_file_is_not_image, 'danger', '');
      this.isFormValid = false;
      return false;
    }
    if(!this.validate.validateImage(file.name)) {
      this.alert.showNotification(this.msg_file_is_not_format, 'danger', '');
      this.isFormValid = false;
      return false;
    } else {
      this.isFormValid = true;
      this.coverFile = <File>event.target.files[0];
    }
  }
  // removing language form
onRemoveLangs(language_name, language_id, index) {
  let selected = this.articleForm.get("items") as FormArray;

  swal({
    title: this.msg_delete_lang_title + ` ${language_name}`,
    text: this.msg_delete_lang_text,
    type: "warning",
    showCancelButton: true,
    cancelButtonText: this.msg_swal_button_no,
    confirmButtonClass: "btn btn-success",
    cancelButtonClass: "btn btn-danger",
    confirmButtonText: this.msg_swal_button_yes,
    buttonsStyling: false
  }).then(
    () => {
      this.http
        .delete("delete/language/article/" + this.id + "/" + language_id)
        .subscribe(httpResponse => {
          if (httpResponse.status === 204) {
            swal({
              title: this.msg_delete_lang_success_title,
              text: `${language_name} ` + this.msg_delete_lang_success_text,
              type: "success",
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            });
            selected.removeAt(index);
            let renewLang = {
              name: language_name,
              id: language_id
            };
            this.langs.push(renewLang);
          } else if (httpResponse.status !== 204) {
            this.alert.showNotification(this.msg_delete_unsuccess, 'danger', 'notifications');
          }
        }, error => {
          this.alert.showNotification(this.msg_server_error, 'danger', '');
        });
    }, (dismiss) => {

    });
}
onSubmit() {
  const fd: FormData = new FormData();
  let languages: any[] = [];
  let langForm = this.articleForm.get('items') as FormArray;
  // parse data from FormArray items to this.language array
  langForm.controls.forEach(element => {
    let formGroup = element as FormGroup;
    if(formGroup.controls.text.disabled !== true) {
    if(formGroup.controls.text_id.value === null) {
      languages.push({
        language_id: formGroup.controls.language_id.value,
        name: "text",
        value: formGroup.controls.text.value
      });
    } else {
    languages.push({
      id: formGroup.controls.text_id.value,
      name: "text",
      value: formGroup.controls.text.value
    });
  }
}
    languages.push({
      id: formGroup.controls.name_id.value,
      name: "name",
      value: formGroup.controls.name.value
    });
  });
  this.articleForm.controls.languages.setValue(languages);
  let formInput = this.articleForm.value;
  delete formInput.items;
  if (this.coverFile !== undefined) {
    fd.append("cover", this.coverFile);
  }
  fd.append("json", JSON.stringify(formInput));

  if(this.isFormValid) {
    this.http.postFormData('patch/article/' + this.id, fd).subscribe(httpResponse => {
      if(httpResponse.status == 204) {
        this.removeItem();
        this.initLoadingData(true);
        this.alert.showNotification(
          this.msg_success_edited,
          "success",
          "notification"
        );
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })
  } else {
    this.alert.showNotification(this.msg_invalid_image, 'dangeer', '');
  }
}

ngOnDestroy() {
  this.subscriptionLangs.unsubscribe();
  this.subscriptionParams.unsubscribe();
}
}
