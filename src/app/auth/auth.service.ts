import { Injectable } from '@angular/core';
import { LocalStorageService } from '../services/localstorage.service';

@Injectable()
export class AuthService {
    
  constructor(private ls: LocalStorageService) {}
  // ...
  public isAuthenticated(): boolean {
    const token = this.ls.get('token');
    const user = this.ls.get('user');
    if(token != null) {
        return true;
    }
    // Check whether the token is expired and return
    // true or false
    // return !this.jwtHelper.isTokenExpired(token);
    return false;
  }
}