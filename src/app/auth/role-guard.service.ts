import { Injectable } from '@angular/core';
import { 
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class RoleGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this will be passed from the route config
    // on the data property

    const expectedRole = route.data.expectedRole;
    // const expectedRole = 'winery_admin';

    const userData = JSON.parse(localStorage.getItem('user_data'));
    // decode the token to get its payload
    if (
      !this.auth.isAuthenticated() || 
      userData.type !== expectedRole
    ) {
    	if(this.auth.isAuthenticated()) {
    		this.router.navigate(['/']);
    		return false;
    	} else {
    		this.router.navigate(['login']);
      		return false;
    	}
     
    }
    return true;
  }
}