import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { HttpService } from './../../services/http.service';
import { NotificationsService } from './../../notifications/notifications.service';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var swal: any;

@Component({
  selector: 'app-add-language',
  templateUrl: './add-language.component.html',
  styleUrls: ['./add-language.component.css']
})
export class AddLanguageComponent implements OnInit {
  /** variables */
	addLangTableForm: FormGroup;
	mobileLangTableForm: FormGroup;
	frontEndLangTableForm: FormGroup;
	defaultLangs: number = 1;
	translateStringKeys: any = new Array();
  frontEndTranslateStringKeys: any = new Array();
	mobileDataSource: any;
  frontEndDataSource: any;
	displayedColumns = ['key', 'value', 'input'];
	code: string = '';
	name: string = '';
  /** check is tab clicked */
  isTabSelected: boolean = false;
  /** loaded data */
  isDataLoaded: boolean = false;

  listOfFlags: any[] = ['AD', 'AE', 'AG', 'AM' ,'AR', 'AT' ,'AU' , 'BE', 'BF', 'BG', 'BO', 'BR', 'CA', 'CD', 'CG', 'CH', 'CL', 'CM', 'CN', 'CO', 'CZ', 'DE', 'DJ', 'DK', 'DZ', 'EE', 'EG', 'ES', 'FI', 'FR', 'GA', 'GB', 'GM', 'GT', 'HN', 'HT', 'HU', 'ID', 'IE', 'IL', 'IN', 'IQ', 'IR', 'IT', 'JM', 'JO', 'JP', 'KG', 'KN', 'KP', 'KR', 'KW', 'KZ', 'LA', 'LB', 'LC', 'LS', 'LU', 'LV', 'MG', 'MK', 'ML', 'MM', 'MT', 'MX', 'NA', 'NE', 'NG', 'NI', 'NL', 'NO', 'OM', 'PA', 'PE', 'PG', 'PK', 'PL', 'PT', 'PY', 'QA', 'RO', 'RS', 'RU', 'RW', 'SA', 'SE', 'SG', 'SL', 'SN', 'SO', 'SV', 'TD', 'TJ', 'TL', 'TR', 'TZ', 'UA', 'US', 'VE', 'VN', 'YE' ];
  flag_name: string = '';

  /** translate strings */
  msg_server_error: string;
  msg_success_created: string;
  msg_swal_loading: string;

	@ViewChild('mobileForm') mobileForm: any;
	@ViewChild('frontEndForm') frontEndForm: any;
  @ViewChild('langName') langNameRef: any;
  @ViewChild('langCode') langCodeRef: any;
  @ViewChild('flag_code') flagCodeName: any;

  constructor(
  		private http: HttpService,
  		private fb: FormBuilder,
      private alert: NotificationsService,
      private translate: TranslateHelperService
  	) { }

  ngOnInit() {

    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('TRANSLATE_ALERT_MSG_SUCCESS_CREATED').then((val: string) => {
      this.msg_success_created = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });

  	this.mobileLangTableForm = this.fb.group({});
  	this.frontEndLangTableForm = this.fb.group({});
  	

    
  }
  initLoadingData() {
    swal({title: this.msg_swal_loading, allowOutsideClick: false});
    swal.showLoading();

    this.http.get('get/language/' + this.defaultLangs, 1).subscribe(res => {
      if(res.status === 200) {
        let data = res.json();
            for(let key in data.fields) {

              this.translateStringKeys.push({key: key, value: data.fields[key]});

              const control = <FormGroup>this.mobileLangTableForm;
              control.addControl(key, new FormControl(data.fields[key], Validators.required));
        }
        this.mobileDataSource = this.translateStringKeys;
      }
    }, err => {
      swal.close();
      console.log(err);
    });
    this.http.get('web/language', 1).subscribe(res => {
      if(res.status === 200) {
        swal.close();
        let data = res.json();
          for(let key in data) {
            this.frontEndTranslateStringKeys.push({key: key, value: data[key]});

            const control = <FormGroup>this.frontEndLangTableForm;
            control.addControl(key, new FormControl(data[key], Validators.required));
          }
          this.frontEndDataSource = this.frontEndTranslateStringKeys;
      }
    }, err => {
      swal.close();
      console.log(err);
    });
  }

  onTabSelect() {
    this.isTabSelected = true;
    if(!this.isDataLoaded) {
      this.isDataLoaded = true;
      this.initLoadingData();
    }
  }

  onSelectFlag(name: string) {
    this.flag_name = name;
    this.code = name;
  }

  /** reset form */
  resetForm() {
    // this.mobileForm.resetForm();
    // this.frontEndForm.resetForm();
    this.flagCodeName.reset();
    this.langNameRef.reset();
    this.langCodeRef.reset();
    this.name = '';
    this.code = '';
  }
  /** on submit forms */
  onSubmit() {
    	this.mobileForm.ngSubmit.emit();
      this.frontEndForm.ngSubmit.emit();

      let formsData = {
        name: this.name,
        code: this.code,
        mobile: this.mobileLangTableForm.value,
        web: this.frontEndLangTableForm.value
      }

      swal({ title: this.msg_swal_loading, allowOutsideClick: false });
      swal.showLoading();
      this.http.postLanguage('create/language', formsData).subscribe(res =>{
        if(res.status == 201) {
          this.resetForm();
          swal.close();
          this.alert.showNotification(this.msg_success_created, 'success', '');
        }
      }, err => {
        this.alert.showNotification(this.msg_server_error, 'danger', '');
        swal.close();
      });
  }
}

