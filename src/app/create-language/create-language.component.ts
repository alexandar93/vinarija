import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from './../services/http.service';
import { NotificationsService } from './../notifications/notifications.service'
import { Subscription } from 'rxjs';
import { TranslateHelperService } from './../services/translate-helper.service';


declare var swal: any;

@Component({
  selector: 'app-create-language',
  templateUrl: './create-language.component.html',
  styleUrls: ['./create-language.component.css']
})
export class CreateLanguageComponent implements OnInit {
  subscriptionLangs: Subscription;

  languages: any = new Array();
  /** translate strings */
  msg_server_error: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_unsuccess: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;

  constructor(
      private http: HttpService,
      private alert: NotificationsService,
      private router: Router,
      private translate: TranslateHelperService
    ) { }

  ngOnInit() {
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error  = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title  = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text  = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title  = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text  = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess  = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes  = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no  = val;
    });

    this.subscriptionLangs = this.http.get('dropdown/language', 1).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        this.languages = httpResponse.json();
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });
  }
  removeLanguage(name, id, index) {
    swal({
      title: this.msg_delete_lang_title + ` ${name}`,
      text: this.msg_delete_lang_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        this.http.delete('delete/language/' + id).subscribe(res => {
          if(res.status === 204) {
              this.languages.splice(index, 0);
              swal({
              title: this.msg_delete_lang_success_title,
              text: `${name} ` + this.msg_delete_lang_success_text,
              type: "success",
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
              }).then(() => {
                location.reload();
              });
            }
          });
          
      }, dismiss => {

      });
  }

  onAddNewLang() {
    this.router.navigate(["create-language/add-language"]);
  }
  onEditLang() {
    // this.router.navigate(["create-language/language-table"]);

  }
}
