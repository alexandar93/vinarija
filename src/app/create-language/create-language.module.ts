import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CreateLanguageRoute } from './create-language.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateLanguageComponent } from './create-language.component';
import { MatTableModule, MatExpansionModule, MatIconModule, MatTooltipModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatListModule } from '@angular/material';
import { FileDropDirective } from './../directives/file-drop.directive';
import { LanguageTableComponent } from './language-table/language-table.component';
import { AddLanguageComponent } from './add-language/add-language.component';
import {MatTabsModule} from '@angular/material/tabs';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateLanguageRoute),
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatTooltipModule,
    MatSelectModule,
    MatTabsModule,
    MatListModule,
    MatExpansionModule,
    MatIconModule,
    TranslateModule
  ],
  declarations: [
    CreateLanguageComponent,
    FileDropDirective,
    LanguageTableComponent,
    AddLanguageComponent
  ],
  exports: [
  	MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatTooltipModule,
    MatSelectModule,
    MatTabsModule,
    MatListModule,
    MatExpansionModule,
    MatIconModule
    ]
})
export class CreateLanguageModule { }
