import { Routes } from "@angular/router";
import { CreateLanguageComponent } from "./create-language.component";
import { AddLanguageComponent } from './add-language/add-language.component'; 

export const CreateLanguageRoute: Routes = [
    {
        path: '',
        component: CreateLanguageComponent
    },
    {
    	path: 'add-language',
    	component: AddLanguageComponent
    },
]