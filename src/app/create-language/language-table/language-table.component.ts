import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpService } from './../../services/http.service';
import { NotificationsService } from './../../notifications/notifications.service';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var swal: any;

@Component({
  selector: 'app-language-table',
  templateUrl: './language-table.component.html',
  styleUrls: ['./language-table.component.css']
})
export class LanguageTableComponent implements OnInit {

	frontEndLangTableForm: FormGroup;
	mobileLangTableForm: FormGroup;
	translateStringKeys: any = new Array();
	/**  */
	/** language name */
	name: string;
	/** language code */
	code: string;
  /** check is tab clicked */
  isTabSelected: boolean = false;
  /** loaded data */
  isDataLoaded: boolean = false;
  /** flags name array */
  listOfFlags: any[] = ['AD', 'AE', 'AG', 'AM' ,'AR', 'AT' ,'AU' , 'BE', 'BF', 'BG', 'BO', 'BR', 'CA', 'CD', 'CG', 'CH', 'CL', 'CM', 'CN', 'CO', 'CZ', 'DE', 'DJ', 'DK', 'DZ', 'EE', 'EG', 'ES', 'FI', 'FR', 'GA', 'GB', 'GM', 'GT', 'HN', 'HT', 'HU', 'ID', 'IE', 'IL', 'IN', 'IQ', 'IR', 'IT', 'JM', 'JO', 'JP', 'KG', 'KN', 'KP', 'KR', 'KW', 'KZ', 'LA', 'LB', 'LC', 'LS', 'LU', 'LV', 'MG', 'MK', 'ML', 'MM', 'MT', 'MX', 'NA', 'NE', 'NG', 'NI', 'NL', 'NO', 'OM', 'PA', 'PE', 'PG', 'PK', 'PL', 'PT', 'PY', 'QA', 'RO', 'RS', 'RU', 'RW', 'SA', 'SE', 'SG', 'SL', 'SN', 'SO', 'SV', 'TD', 'TJ', 'TL', 'TR', 'TZ', 'UA', 'US', 'VE', 'VN', 'YE' ];
  flag_name: string;

	mobileDataSource: any;
	frontEndDataSource: any;
	displayedColumns = ['value', 'input'];

  /** translate strings */
  msg_success_edited: string;
  msg_server_error: string;
  msg_swal_loading: string;

	@Input('languageName') languageName: any;
	@Input('languageID') languageId: any;

  constructor(
  		private http: HttpService,
  		private fb: FormBuilder,
      private alert: NotificationsService,
      private translate: TranslateHelperService
  	) { }

  ngOnInit() {
    /** translate */
    this.translate.getTranslate('TRANSLATE_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
      this.msg_success_edited = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });

  	this.frontEndLangTableForm = this.fb.group({});
  	this.mobileLangTableForm = this.fb.group({});

    if(this.isTabSelected) {
      this.initLoadingData();
    }
    this.http.get('patch/initialize/language/' + this.languageId, 1).subscribe(res => {
      if(res.status === 200) {
        let data = res.json();
        this.name = data.name;
        this.code = data.code;
        this.flag_name = data.code;
      }
    });
  }
  onTabSelect() {
    this.isTabSelected = true;
    if(!this.isDataLoaded) {
      this.isDataLoaded = true;
      this.initLoadingData();
    }
  }

  onSelectFlag(name: string) {
    this.flag_name = name;
    this.code = name;
  }

  initLoadingData() {
    swal({title: this.msg_swal_loading, allowOutsideClick: false});
    swal.showLoading();
  	this.http.get('patch/initialize/language/' + this.languageId, 1).subscribe(res => {
  		if(res.status === 200) {
        swal.close();
  			let data = res.json();
  			
  			data.mobile.forEach(obj => {
  				const control = <FormGroup>this.mobileLangTableForm;

  				let idControl = new FormControl(obj.id, Validators.required);
  				let valueControl = new FormControl(obj.value, Validators.required);
				  control.addControl(obj.name, new FormGroup({id: idControl, value: valueControl}));
  			});

  			data.web.forEach(obj => {
  				const control = <FormGroup>this.frontEndLangTableForm;

  				let idControl = new FormControl(obj.id, Validators.required);
  				let valueControl = new FormControl(obj.value, Validators.required);
  				control.addControl(obj.name, new FormGroup({id: idControl, value: valueControl}));
  			});
  			this.frontEndDataSource = data.web;
				this.mobileDataSource = data.mobile;
  		}
  	}, err => {
      swal.close();
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });
  }

  onSubmit() {
  	let mobileData: any = new Array();
  	for(let key in this.mobileLangTableForm.value) {
  		mobileData.push(this.mobileLangTableForm.value[key]);
  	}
    for(let key in this.frontEndLangTableForm.value) {
      mobileData.push(this.frontEndLangTableForm.value[key]);
    }
  
  	let formsData = {
  		name: this.name,
  		code: this.code,
  		fields: mobileData
  	}
    swal({title: this.msg_swal_loading, allowOutsideClick: false});
    swal.showLoading();
  	this.http.postLanguage('patch/language/' + this.languageId, formsData).subscribe(res => {
      if(res.status === 204) {
        swal.close();
        this.alert.showNotification(this.msg_success_edited, 'success', '');
      }
  	}, err => {
      swal.close();
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });

  }


}
