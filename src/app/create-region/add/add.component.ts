import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NotificationsService } from '../../notifications/notifications.service';
import { ActivatedRoute } from '@angular/router';
import { RegionDataSource } from '../../services/region.datasource';
import { ImageVideoValidatorsService } from '../../services/image-video-validators.service';
import { Globals } from './../../model/globals';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var swal: any;
declare var google: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit, OnDestroy {
  /** Variables */
  /** Subscription for list of region, reon, vinogorja */
  subscripionDropDown: Subscription;
  /** Subscription for params */
  subscriptionparams: Subscription;
  /** FormGroup */
  addRegionForm: FormGroup;
  /** list of regions */
  regionsList: any[] = [];
  /** list of reons */
  reonsList: any[] = [];
  /** list of vinogorja */
  vinogorjaList: any[] = [];
  /** value of param type */
  type: string = '';
  /** id for region */
  region_id: number;
  /** id for reon */
  reon_id: number;
  /** value of input  */
  inputValue: string;
  /** value of description */
  descValue: string;
  /** is form valid */
  isFormValid: boolean = true;
  /** cover image */
  coverFile: any = null;
  /** lantitude */
  lat: number;
  /** longitude */
  lng: number;
  /** markers array */
  markersArray = new Array();
  /** google DircetionsSevice ??? */
  service = new google.maps.DirectionsService();
  /** google maps geocoder */
  geocoder = new google.maps.Geocoder();
  /** google map  */
  map: google.maps.Map;
  /** MVC Array for path */
  path = new google.maps.MVCArray();
  /** points for polygon array */
  pointsArray: any[] = [];
  /** polygon coords */
  polyCoords: any[] = [];
  /** polygon */
  polygon = new google.maps.Polygon({
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35
  });
  /** modules setup for editor */
  modules: any;
  /** */
  /** test */
  // test: any = {
  //   "parent_id": 18,
  //   "type": "reon",
  //   "languages": [{
	// 	"language_id": 1,
	// 	"name": "name",
	// 	"value": "aaaaaa"
	// }]
  // }

  /** translate strings */
  msg_server_error: string;
  msg_file_is_not_image: string;
  msg_file_is_not_format: string;
  msg_cannot_add_marker: string;
  msg_min_required_markers: string;
  msg_swal_loading: string;
  msg_success_created: string;
  msg_image_invalid: string;

  /** ViewChild */
  @ViewChild('f') myNgForm: any;
  @ViewChild('coverPath') coverPath: any;
  
  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private validate: ImageVideoValidatorsService,
    private globals: Globals,
    private translate: TranslateHelperService
  ) { 
    this.modules = globals.editor_settings;
  }
  /** ngOnInit */
  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_file_is_not_format = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_WINDOWS_ALERT').then((val: string) => {
      this.msg_cannot_add_marker = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_POLYGON_PATTERN').then((val: string) => {
      this.msg_min_required_markers = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_SUCCESS_CREATE').then((val: string) => {
      this.msg_success_created = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_INVALID_IMAGE').then((val: string) => {
      this.msg_image_invalid = val;
    });

    this.subscriptionparams = this.route.params.subscribe(
      params => (this.type = params.type)
    );

    this.subscripionDropDown = this.http.get('area/dropdown/nested', 1).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        this.regionsList = httpResponse.json();
        // this.reonsList = httpResponse.json().children;
        // this.vinogorjaList = httpResponse.json().children.children;
        // console.log(this.regionsList);
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });
    this.addRegionForm = this.fb.group({
      region: [{value: "", disabled: true}, Validators.required],
      regionDesc: [{value: "", disabled: true}, Validators.required],
      reon: [{value: "", disabled: true}, Validators.required],
      reonDesc: [{value: "", disabled: true}, Validators.required],
      vinogorje: [{value: "", disabled: true}, Validators.required],
      vinogorjeDesc: [{value: "", disabled: true}, Validators.required],
      listOfRegions: [{value: "", disabled: true}, Validators.required],
      listOfReons: [{value: "", disabled: true}, Validators.required],
      listOfVinogorja: [{value: "", disabled: true}, Validators.required]
    }); 
    this.disableInput(this.type);
    var element = document.getElementById("map");
    if(element){
      this.initMap();
    }
  }
  /** reset form */
  resetForm() {
    this.myNgForm.resetForm();
  }
  /** disable inputs */
  disableInput(type) {
    if(type == 'regija') {
      this.descValue = this.addRegionForm.controls.regionDesc.value;
      this.inputValue = this.addRegionForm.controls.region.value;
      this.addRegionForm.controls.region.enable();
      this.addRegionForm.controls.regionDesc.enable();
    }
    if(type == 'reon') {
      this.descValue = this.addRegionForm.controls.reonDesc.value;
      this.inputValue = this.addRegionForm.controls.reon.value;
      this.addRegionForm.controls.reon.enable();
      this.addRegionForm.controls.reonDesc.enable();
      this.addRegionForm.controls.listOfRegions.enable();
    }
    if(type == 'vinogorje') {
      this.descValue = this.addRegionForm.controls.vinogorjeDesc.value;
      this.inputValue = this.addRegionForm.controls.vinogorje.value;
      this.addRegionForm.controls.vinogorje.enable();
      this.addRegionForm.controls.vinogorjeDesc.enable();
      this.addRegionForm.controls.listOfRegions.enable();
      this.addRegionForm.controls.listOfReons.enable();
    }
  }
  /** setting values of dropdowns */
  dropdownValueRegion(id) {
    // console.log(id)
    if(this.regionsList !== undefined) {
      this.regionsList.forEach(region => {
        if(region.id == id) {
          this.reonsList = region.children;
          this.region_id = id;
        }
      });
    }
  }
  dropdownValueReon(id) {
    // console.log(id)
    if(this.reonsList !== undefined) {
      this.reonsList.forEach(reon => {
        if(reon.id == id) {
          this.vinogorjaList = reon.children;
          this.reon_id = reon.id;
        }
      });
    }
  }
  onUploadCover(event) {
    let file = <File>event.target.files[0];
    if (file !== undefined) {
      if (file.type.indexOf("image") == -1) {
        this.alert.showNotification(
          this.msg_file_is_not_image,
          "danger",
          ""
        );
        this.isFormValid = false;
        return false;
      }
      if (!this.validate.validateImage(file.name)) {
        this.alert.showNotification(
          this.msg_file_is_not_format,
          "danger",
          ""
        );
        this.isFormValid = false;
        return false;
      } else {
        this.isFormValid = true;
        this.coverFile = <File>event.target.files[0];
      }
    }
  }
  /** test */
  // CreateTest() {
  //   const fd: FormData = new FormData();
  //   fd.append("json", JSON.stringify(this.test));
  //   this.http.postFormData('create/area', fd).subscribe(res => {

  //   })
  // }
  /** formating data */
  data() {
    let podaci: any = {
      parent_id: null,
      type: '',
      languages: [{
            name: '',
            value: '',
            language_id: null
          }, {
            name: '',
            value: '',
            language_id: null
          }],
      pins: this.pointsArray
    }
    let parent;
    if(this.type === 'regija'){
      parent = null;
    }
    if(this.type === 'reon') {
      parent = this.region_id;
    }
    if(this.type === 'vinogorje') {
      parent = this.reon_id;
    }
    this.disableInput(this.type);
    podaci.parent_id = parent;
    podaci.type = this.type;
    podaci.languages[0].name = 'name';
    podaci.languages[0].value = this.inputValue,
    podaci.languages[0].language_id = 1;
    podaci.languages[1].name = 'description';
    podaci.languages[1].value = this.descValue, 
    podaci.languages[1].language_id = 1;
    // console.log(podaci, parent, this.region_id);
    return podaci;
  }
  initMap() {
      this.map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: {lat: 44.016521, lng: 20.865859},
        streetViewControl: false,
        scrollwheel: false
      });

      this.polygon.setMap(this.map);

      // Add a listener for the click event
      this.map.addListener('click', (event)=>{ return this.addLatLng(event) });

    this.drawPolygon(this.pointsArray);
  }
   /** Clearing all waypoints and markers from map */
   clearMap(){
    this.path.clear();
    this.polygon.setMap(null);
    for(var i=0; i < this.markersArray.length; i++){
        this.markersArray[i].setMap(null);
    }
    this.markersArray = [];
    this.polygon.setMap(this.map);
  };

  drawPolygon(data) {
    this.clearMap();

    let labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let labelIndex = 0;

    if(!data.length) return;

    let markers = data;

    for (let i = 0; i < markers.length; i++) {
        let data = markers[i];
        let myLatlng = new google.maps.LatLng(data.lat, data.lng);
        markers[i].letter = labels[labelIndex % labels.length];

        let marker = new google.maps.Marker({
            position: myLatlng,
            map: this.map,
            label: labels[labelIndex++ % labels.length],
            draggable: true,
            id : i
        });

        this.markersArray.push(marker);
        // console.log('markers array',this.markersArray);
        ((marker, data)=>{
          // Marker drop handler
          google.maps.event.addListener(marker, "dragend", (event)=>{
            this.clearMap();

            let lat = event.latLng.lat(); 
            let lng = event.latLng.lng();

            this.pointsArray[marker['id']].lat = lat;
            this.pointsArray[marker['id']].lng = lng;

            this.geocoder.geocode({
              'latLng': event.latLng
              }, (results, status)=>{
                if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                    this.drawPolygon(this.pointsArray);
                  }
                } else {

                  // this.drawPolygon(this.oldPointsArray);
                  window.alert(this.msg_cannot_add_marker);
                }
             });
          });
      })(marker, data);
      }
        this.polygon.setPaths(data); //add the ways to the polyine 
        // console.log(this.path)
        // console.log('point', this.pointsArray)
  }
  /** Handles click events on a map, and adds a new point map */
  addLatLng(event) {
    // console.log(event);
    this.geocoder.geocode({
    'latLng': event.latLng
    }, (results, status)=>{
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          //alert(results[0].formatted_address);
          this.pointsArray.push({
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
          });
          // console.log(this.pointsArray)
        }

        this.drawPolygon(this.pointsArray);
      } else {
        window.alert(this.msg_cannot_add_marker);
      }
    });
  }
  /** Handle remove button click in dragable list */
  removePoint(elemIndex, array) {
    array.splice(elemIndex, 1);
    this.drawPolygon(this.pointsArray);
  }
  /** on submit form */
  onSubmit() {
    if(this.pointsArray.length < 3 && this.type === 'regija') {
        return this.alert.showNotification(this.msg_min_required_markers, 'danger', '');
    }
    // console.log(this.addRegionForm.value);
    const fd: FormData = new FormData();
    let data = this.data();
    fd.append('cover', this.coverFile);
    fd.append('json', JSON.stringify(data));
    
    if(this.isFormValid) {
      swal({ title: this.msg_swal_loading, allowOutsideClick: false });
      swal.showLoading();
      this.http.postFormData('create/area', fd).subscribe(httpResponse => {
        if(httpResponse.status == 201) {
          this.resetForm();
          this.coverPath.nativeElement.click();
          this.coverFile = null;
          this.pointsArray = [];
          this.clearMap();
          swal.close();
          this.alert.showNotification(this.msg_success_created + this.type, 'success', '');
        }
      }, err => {
        swal.close();
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      });
    } else {
      this.alert.showNotification(this.msg_image_invalid, 'danger', '');
    }
  }
  /** destroying subscripion */
  ngOnDestroy() {
    this.subscripionDropDown.unsubscribe();
  }
}
