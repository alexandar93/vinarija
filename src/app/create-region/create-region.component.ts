import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { RegionDataSource } from '../services/region.datasource';
import { HttpService } from '../services/http.service';
import { NotificationsService } from '../notifications/notifications.service';
import { tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ReonDataSource } from '../services/reon.datasource';
import { VinogorjeDataSource } from '../services/vinogorje.datasource';
import { Subscription } from 'rxjs';
import { TranslateHelperService } from './../services/translate-helper.service';


declare var swal: any;

@Component({
  selector: 'app-create-region',
  templateUrl: './create-region.component.html',
  styleUrls: ['./create-region.component.css']
})
export class CreateRegionComponent implements OnInit {
  /** Variables */

  /** subscription for data */
  subscriptionData: Subscription;
  /** default language */
  defaultLanguage: number = 1;
  /** region id */
  regionId: number = null;
  /** reon id */
  reonId: number = null;
  /** selected row in table region */
  selectedElement : HTMLElement;
  /** selected row in table reon */
  selectedElement2 : HTMLElement;
  /** datasource for region table */
  dataRegion: RegionDataSource;
  /** datasource for reon table */
  dataReon: ReonDataSource;
  /** datasource for vinogorje table */
  dataVinogorje: VinogorjeDataSource;
  /** column headers for table */
  displayedColumns = ["name", "actions"];
  /**  */
  total: number;
  pageSize: number;
  total2: number;
  pageSize2: number;
  total3: number;
  pageSize3: number;

  /** translate strings */
  msg_server_error: string;
  msg_delete_region_title: string;
  msg_delete_region_text: string;
  msg_delete_region_success_title: string;
  msg_delete_region_success_text: string;
  msg_delete_unsuccess: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_not_allowed_delete_regions: string;
  msg_not_allowed_delete_reons: string;

  @ViewChild('regionPaginator') paginator: MatPaginator;
  @ViewChild('reonPaginator') paginator2: MatPaginator;
  @ViewChild('vinogorjePaginator') paginator3: MatPaginator;
  
  constructor(
    private http: HttpService,
    private alert: NotificationsService,
    private router: Router,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {
    this.regionId = 1;
    // translate for pagination
    // translate lable for pagination
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_delete_region_title = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_delete_region_text = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_region_success_title = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_region_success_text = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_DELETE_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_NOT_ALLOWED_DELETE_REGIONS').then((val: string) => {
      this.msg_not_allowed_delete_regions = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_NOT_ALLOWED_DELETE_REONS').then((val: string) => {
      this.msg_not_allowed_delete_reons = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });

    // init regije data table
    this.dataRegion = new RegionDataSource(this.http);
    this.dataReon = new ReonDataSource(this.http);
    this.dataVinogorje = new VinogorjeDataSource(this.http);
    //
    this.dataRegion.loadRegions(1, this.defaultLanguage);
     // get total and per page for table 
     this.subscriptionData = this.http.get('area/regija', 1).subscribe((httpResponse) => {
      if(httpResponse.status === 200) {
          this.total = httpResponse.json().total;
          this.pageSize = httpResponse.json().per_page;
      }
  }, error => {
    this.alert.showNotification(this.msg_server_error, 'danger', '');
  });
  }
  
  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => {
        this.loadRegionPage();
      })
    ).subscribe();
    this.paginator2.page.pipe(
      tap(() => {
        this.loadReonPage();
      })
    ).subscribe();
    this.paginator3.page.pipe(
      tap(() => {
        this.loadVinogorjePage();
      })
    ).subscribe();

  }
  loadRegionPage() {
    // this.dataVinogorje.loadVinogorje(
    //   this.paginator3.pageIndex + 1, this.areaId, this.defaultLanguage, true);
    this.dataRegion.loadRegions(
        this.paginator.pageIndex + 1, this.defaultLanguage);
  }
  loadReonPage() {
    this.dataReon.loadReons(
        this.paginator2.pageIndex + 1, this.regionId, this.defaultLanguage);
  }
  loadVinogorjePage() {
    this.dataVinogorje.loadVinogorje(
        this.paginator3.pageIndex + 1, this.reonId, this.defaultLanguage);
  }
  /** on select regije */
  onSelectedRegion(event, row) {
    // console.log(event, row);
    // disable to click on button for delete and open modal
    if(event.target.nodeName == 'a') return false; 
    if(event.target.nodeName == 'I') return false;
    if(event.target.nodeName == 'BUTTON') return false;

    if(this.selectedElement!=null){
      this.selectedElement.style.backgroundColor = "#ffffff";
      this.selectedElement.style.borderLeft = "0px solid #ffffff";
    }
    if(event.target.nodeName !== 'MAT-CELL') {
      this.selectedElement = event.target;
    } else {
      this.selectedElement = event.target.parentElement;
    }
    this.selectedElement.style.backgroundColor = "#92000015";
    this.selectedElement.style.borderLeft = "5px solid #920000";
    this.regionId = row.id;
    this.dataVinogorje.loadVinogorje(1, 1, this.defaultLanguage);
    this.dataReon.loadReons(1, row.id, this.defaultLanguage);
    this.http.get('area/children/' + row.id, 1).subscribe((httpResponse) => {
      if(httpResponse.status === 200) {
        // console.log(httpResponse.json().children.total)
          this.total2 = httpResponse.json().children.total;
          this.pageSize2 = httpResponse.json().children.per_page;
      }
  }, error => {
    this.alert.showNotification(this.msg_server_error, 'danger', '');
  });
  }
  
  onSelectedReon(event, row) {
    // disable to click on button for delete and open modal
    if(event.target.nodeName == 'a') return false; 
    if(event.target.nodeName == 'I') return false;
    if(event.target.nodeName == 'BUTTON') return false;

    if(this.selectedElement2 !=null){
      this.selectedElement2.style.backgroundColor = "#ffffff";
      this.selectedElement2.style.borderLeft = "0px solid #ffffff";
    }
    if(event.target.nodeName !== 'MAT-CELL') {
      this.selectedElement2 = event.target;
    } else {
      this.selectedElement2 = event.target.parentElement;
    }
    this.selectedElement2.style.backgroundColor = "#92000015";
    this.selectedElement2.style.borderLeft = "5px solid #920000";
    this.reonId = row.id;
     // init vinogorje data table
     this.dataVinogorje.loadVinogorje(1, row.id, this.defaultLanguage);
     this.http.get('area/children/' + row.id, 1).subscribe((httpResponse) => {
      if(httpResponse.status === 200) {
          this.total3 = httpResponse.json().children.total;
          this.pageSize3 = httpResponse.json().children.per_page;
      }
  }, error => {
    this.alert.showNotification(this.msg_server_error, 'danger', '');
  });
  }
  pageIndexData(type) {
    let parentId: number = null;
    if(type === 'reon') parentId = this.regionId;
    if(type === 'vinogorje') parentId = this.reonId;
    this.http.get('area/children/' + parentId, 1).subscribe((httpResponse) => {
      if(httpResponse.status === 200) {
          if(type === 'reon') {
            this.total2 = httpResponse.json().children.total;
            this.pageSize2 = httpResponse.json().children.per_page;
          }
          if(type === 'vinogorje') {
            this.total3 = httpResponse.json().children.total;
            this.pageSize3 = httpResponse.json().children.per_page;
          }
      }
  }, error => {
    this.alert.showNotification(this.msg_server_error, 'danger', '');
  });
  }
  onAddRegions(type: string) {
    this.router.navigate(["create-region/add/" + type]);
  }
  onEditRegion(id: number, type: string) {
    this.router.navigate(["create-region/edit/" + id + '/' + type]);
  }
  onDeleteRegion(id, name, regionType) {
    swal({
      title: this.msg_delete_region_title + ` ${name} ?`,
      text: this.msg_delete_region_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(() => {
      this.http.delete("delete/area/" + id).subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          let total;
          let pageSize;
          swal({
            title: this.msg_delete_region_success_title,
            text: `${regionType} ${name} ` + this.msg_delete_region_success_text,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          this.http.get('area/' + regionType, 1).subscribe((httpResponse) => {
            if(httpResponse.status === 200) {
                total = httpResponse.json().total;
                pageSize = httpResponse.json().per_page;
            }
          }, error => {
            this.alert.showNotification(this.msg_server_error, 'danger', '');
          });
        if(regionType === 'regija') {
          this.dataRegion = new RegionDataSource(this.http);
          this.loadRegionPage();
          this.total = total;
          this.pageSize = pageSize;
        }
        if(regionType === 'reon') {
          this.dataReon = new ReonDataSource(this.http);
          this.loadReonPage();
          this.pageIndexData('reon');
        }
        if(regionType === 'vinogorje') {
          this.dataVinogorje = new VinogorjeDataSource(this.http);
          this.loadVinogorjePage();
          this.pageIndexData('vinogorje');
        }
          
        }
      }, (err) => {
        if(err.status === 409) {    
            if(regionType == 'regija') {
              this.alert.showNotification(`${name} ` + this.msg_not_allowed_delete_regions, 'danger', '');
            }
            if(regionType == 'reon') {
              this.alert.showNotification(`${name} ` + this.msg_not_allowed_delete_reons, 'danger', '');
            }
        } else {
          this.alert.showNotification(
            this.msg_delete_unsuccess,
            "danger",
            "error"
          )}
        });
    }, (dismiss) => {
      
    });
  }

}
