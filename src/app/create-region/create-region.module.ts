import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateRegionComponent } from './create-region.component';
import { RouterModule } from '@angular/router';
import { CreateRegionRoute } from './create-region.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { QuillModule } from 'ngx-quill';
import { TranslateModule } from '@ngx-translate/core';
import { MatSelectModule, MatInputModule, MatTableModule, MatSortModule, MatPaginatorModule, MatIconModule, MatTooltipModule, MatProgressSpinnerModule, MatMenuModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateRegionRoute),
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    QuillModule,
    TranslateModule
  ],
  declarations: [
    CreateRegionComponent,
    AddComponent,
    EditComponent
  ],
  exports: [
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatMenuModule
  ]
})
export class CreateRegionModule { }
