import { Routes } from "@angular/router";
import { CreateRegionComponent } from "./create-region.component";
import { AddComponent } from "./add/add.component";
import { EditComponent } from "./edit/edit.component";

export const CreateRegionRoute: Routes = [
    {
        path: '',
        component: CreateRegionComponent
    }, {
        path: 'add/:type',
        component: AddComponent
    }, {
        path: 'edit/:id/:type',
        component: EditComponent
    }
]