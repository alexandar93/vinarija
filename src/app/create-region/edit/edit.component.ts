import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { NotificationsService } from '../../notifications/notifications.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { isTemplateExpression } from 'typescript';
import { ImageVideoValidatorsService } from '../../services/image-video-validators.service';
import { Globals } from './../../model/globals';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var google: any;
declare var swal: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  /** variables */
  /** subscription for params */
  subscriptionparams: Subscription;
  /** subscription for dropdown */
  subscriptionDropDown: Subscription;
  /** FormGroup */
  editRegionForm: FormGroup;
  /** param type regija/reon/vinogorje */
  type: string;
  /** param id for selected region */
  id: number;
  /** is form valid */
  isFormValid: boolean = true;
  /** cover image */
  coverFile: any = null;
  /** list of regions */
  regionsList: any[] = [];
  /** list of reons */
  reonsList: any[] = [];
  /** list of vinogorije */
  vinogorjeList: any[] = [];
  /** formating language to send */
  languages: any = new Array();
  /** lantitude */
  lat: number;
  /** longitude */
  lng: number;
  /** markers array */
  markersArray = new Array();
  /** google DircetionsSevice ??? */
  service = new google.maps.DirectionsService();
  /** google maps geocoder */
  geocoder = new google.maps.Geocoder();
  /** google map  */
  map: google.maps.Map;
  /** MVC Array for path */
  path = new google.maps.MVCArray();
  /** points for polygon array */
  pointsArray: any[] = [];
  /** polygon */
  polygon = new google.maps.Polygon({
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#920000',
    fillOpacity: 0.35
  });
  /** modules setup for text editor */
  modules: any;
  /** translate strings */
  msg_server_error: string;
  msg_file_is_not_image: string;
  msg_file_is_not_format: string;
  msg_cannot_add_marker: string;
  msg_min_required_markers: string;
  msg_swal_loading: string;
  msg_success_edited: string;
  msg_image_invalid: string;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private validate: ImageVideoValidatorsService,
    private globals: Globals,
    private translate: TranslateHelperService
  ) {
    this.modules = globals.editor_settings;
   }
  /** ngOnInit */
  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_file_is_not_format = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_WINDOWS_ALERT').then((val: string) => {
      this.msg_cannot_add_marker = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_POLYGON_PATTERN').then((val: string) => {
      this.msg_min_required_markers = val;
    });
    this.translate.getTranslate('REGIONS_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
      this.msg_success_edited = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_INVALID_IMAGE').then((val: string) => {
      this.msg_image_invalid = val;
    });

    this.subscriptionparams = this.route.params.subscribe(
      params => {this.type = params.type; this.id = parseInt(params.id)}
      
    );
    this.subscriptionDropDown = this.http.get('area/dropdown/nested', 1).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        this.regionsList = httpResponse.json();
        this.initLoadingData();
        // this.reonsList = httpResponse.json().children;
        // this.vinogorjaList = httpResponse.json().children.children;
        // console.log(this.regionsList);
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });
    this.editRegionForm = this.fb.group({
      region: [{value: "", disabled: true}, Validators.required],
      regionDesc: [{value: "", disabled: true}, Validators.required],
      reon: [{value: "", disabled: true}, Validators.required],
      reonDesc: [{value: "", disabled: true}, Validators.required],
      vinogorje: [{value: "", disabled: true}, Validators.required],
      vinogorjeDesc: [{value: "", disabled: true}, Validators.required],
      listOfRegions: [{value: "", disabled: true}, Validators.required],
      listOfReons: [{value: "", disabled: true}, Validators.required],
      listOfVinogorja: [{value: "", disabled: true}, Validators.required],
      languages: [this.languages],
      pins: [''],
    })
    this.disableInput(this.type);
    var element = document.getElementById("map");
    if(element){
      this.initMap();
    }
  }
  /** disable inputs */
  disableInput(type) {
    if(type == 'regija') {
      this.editRegionForm.controls.region.enable();
      this.editRegionForm.controls.regionDesc.enable();
    }
    if(type == 'reon') {
      this.editRegionForm.controls.reon.enable();
      this.editRegionForm.controls.reonDesc.enable();
      // this.editRegionForm.controls.listOfRegions.enable();
    }
    if(type == 'vinogorje') {
      this.editRegionForm.controls.vinogorje.enable();
      this.editRegionForm.controls.vinogorjeDesc.enable();
      // this.editRegionForm.controls.listOfRegions.enable();
      // this.editRegionForm.controls.listOfReons.enable();
    }
  }
  /** loading data in fields */
  initLoadingData() {
    this.http.get('patch/initialize/area/' + this.id, 1).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        let data = httpResponse.json();
        this.coverFile = data.cover_image;
        this.setPolygon(data);
        let langs = new Array();
        data.languages.forEach(element => {
          element.fields.forEach(field => {
            let data = field;
            data.language_id = 1;
            this.languages.push(data);
            // console.log(this.languages);
          });
        });
        // console.log(this.languages);
        this.languages.forEach(lang => {
          if(this.type === 'regija') {
            if(lang.name == 'name') {
              this.editRegionForm.controls.region.setValue(lang.value);
            }
            if(lang.name == 'description') {
              this.editRegionForm.controls.regionDesc.setValue(lang.value);
            }
          }
          if(this.type === 'reon') {
            if(lang.name == 'name') {
              this.editRegionForm.controls.reon.setValue(lang.value);
            }
            if(lang.name == 'description') {
              this.editRegionForm.controls.reonDesc.setValue(lang.value);
            }
            this.editRegionForm.controls.listOfRegions.setValue(data.parent.id);
          }
          if(this.type === 'vinogorje') {
            if(lang.name == 'name') {
              this.editRegionForm.controls.vinogorje.setValue(lang.value);
            }
            if(lang.name == 'description') {
              this.editRegionForm.controls.vinogorjeDesc.setValue(lang.value);
            }
            this.regionsList.forEach(parent => {
              parent.children.forEach(children => {
                if(children.id === data.parent.id) {
                  this.reonsList = parent.children;
                }
                if(children.id === data.parent.id) {
                  this.editRegionForm.controls.listOfRegions.setValue(parent.id);
                }
              });
            });
            this.editRegionForm.controls.listOfReons.setValue(data.parent.id);
          }
        });
      }
    }, err =>{

    });
  }
  /** uploading cover image */
  onUploadCover(event) {
    let file = <File>event.target.files[0];
    if (file !== undefined) {
      if (file.type.indexOf("image") == -1) {
        this.alert.showNotification(
          this.msg_file_is_not_image,
          "danger",
          ""
        );
        this.isFormValid = false;
        return false;
      }
      if (!this.validate.validateImage(file.name)) {
        this.alert.showNotification(
          this.msg_file_is_not_format,
          "danger",
          ""
        );
        this.isFormValid = false;
        return false;
      } else {
        this.isFormValid = true;
        this.coverFile = <File>event.target.files[0];
      }
    }
  }
  /** initialize map */
  initMap() {
    this.map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: {lat: 44.016521, lng: 20.865859},
      streetViewControl: false,
      scrollwheel: false
    });

    this.polygon.setMap(this.map);

    // Add a listener for the click event
    this.map.addListener('click', (event)=>{ return this.addLatLng(event) });

    this.drawPolygon(this.pointsArray);
  }
 /** Clearing all waypoints and markers from map */
 clearMap(){
    this.path.clear();
    this.polygon.setMap(null);
    for(var i=0; i < this.markersArray.length; i++){
        this.markersArray[i].setMap(null);
    }
    this.markersArray = [];
    this.polygon.setMap(this.map);
  };
  /** draw polygon */
  drawPolygon(data) {
    this.clearMap();

    let labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let labelIndex = 0;

    if(!data.length) return;

    let markers = data;

    for (let i = 0; i < markers.length; i++) {
        let data = markers[i];
        let myLatlng = new google.maps.LatLng(data.lat, data.lng);
        markers[i].letter = labels[labelIndex % labels.length];

        let marker = new google.maps.Marker({
            position: myLatlng,
            map: this.map,
            label: labels[labelIndex++ % labels.length],
            draggable: true,
            id : i
        });

        this.markersArray.push(marker);
        // console.log('markers array',this.markersArray);
        ((marker, data)=>{
          // Marker drop handler
          google.maps.event.addListener(marker, "dragend", (event)=>{
            this.clearMap();

            let lat = event.latLng.lat(); 
            let lng = event.latLng.lng();

            this.pointsArray[marker['id']].lat = lat;
            this.pointsArray[marker['id']].lng = lng;

            this.geocoder.geocode({
              'latLng': event.latLng
              }, (results, status)=>{
                if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                    this.drawPolygon(this.pointsArray);
                  }
                } else {

                  // this.drawPolygon(this.oldPointsArray);
                  window.alert(this.msg_cannot_add_marker);
                }
            });
          });
      })(marker, data);
      }
        this.polygon.setPaths(data); //add the ways to the polyine 
        // console.log(this.path)
        // console.log('point', this.pointsArray)
  }
  /** Handles click events on a map, and adds a new point map */
  addLatLng(event) {
    // console.log(event);
    this.geocoder.geocode({
    'latLng': event.latLng
    }, (results, status)=>{
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          //alert(results[0].formatted_address);
          this.pointsArray.push({
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
          });
          // console.log(this.pointsArray)
        }

        this.drawPolygon(this.pointsArray);
      } else {
        window.alert(this.msg_cannot_add_marker);
      }
    });
  }
  /** Handle remove button click in dragable list */
  removePoint(elemIndex, array) {
    array.splice(elemIndex, 1);
    this.drawPolygon(this.pointsArray);
  }
  /** set polygon */
  setPolygon(data) {
    this.pointsArray = data.pins;
    this.drawPolygon(this.pointsArray);
  }
  /** set language */
  setLanguage(type, name) {
    let language: any = new Array();
    
  }
  /** on submit form */
  onSubmit() {
    if(this.pointsArray.length < 3 && this.type === 'regija') {
      return this.alert.showNotification(this.msg_min_required_markers, 'danger', '');
    }
    // console.log('NAZIV: ', this.languages);
    let languages: any = new Array();
    this.editRegionForm.controls.pins.setValue(this.pointsArray);
    this.languages.forEach((lang, langIndex) => {
      if(this.type === 'regija') {
        if(lang.name === 'name') {
          languages.push({
            id: this.languages[langIndex].id,
            name: 'name',
            value: this.editRegionForm.controls.region.value,
            language_id: 1
          });
        }
        if(lang.name === 'description') {
          languages.push({
            id: this.languages[langIndex].id,
            name: 'description',
            value: this.editRegionForm.controls.regionDesc.value,
            language_id: 1
          });
        }
      }
      if(this.type === 'reon') {
        if(lang.name === 'name') {
          languages.push({
            id: this.languages[langIndex].id,
            name: 'name',
            value: this.editRegionForm.controls.reon.value,
            language_id: 1
          });
        }
        if(lang.name === 'description') {
          languages.push({
            id: this.languages[langIndex].id,
            name: 'description',
            value: this.editRegionForm.controls.reonDesc.value,
            language_id: 1
          });
        }
      }
      if(this.type === 'vinogorje') {
        if(lang.name === 'name') {
          languages.push({
            id: this.languages[langIndex].id,
            name: 'name',
            value: this.editRegionForm.controls.vinogorje.value,
            language_id: 1
          });
        }
        if(lang.name === 'description') {
          languages.push({
            id: this.languages[langIndex].id,
            name: 'description',
            value: this.editRegionForm.controls.vinogorjeDesc.value,
            language_id: 1
          });
        }
      }
    });
    this.editRegionForm.controls.languages.setValue(languages);
    const fd: FormData = new FormData();
    fd.append('cover', this.coverFile);
    fd.append('json', JSON.stringify(this.editRegionForm.value));
    // console.log('FORMA:', this.editRegionForm.value);
  
    if(this.isFormValid) {
      swal({ title: this.msg_swal_loading, allowOutsideClick: false });
      swal.showLoading();
      this.http.postFormData('patch/area/' + this.id, fd).subscribe(httpResponse => {
        if(httpResponse.status == 204) {
          this.languages = [];
          this.pointsArray = [];
          this.clearMap();
          this.initLoadingData();
          swal.close();
          this.alert.showNotification(this.msg_success_edited + ' ' + this.type, 'success', '');
        }
      }, err => {
        swal.close();
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      });
    } else {
      this.alert.showNotification(this.msg_image_invalid, 'danger', '');
    }
  }

}
