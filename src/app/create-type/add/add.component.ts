import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { NotificationsService } from '../../notifications/notifications.service';
import { Subscription } from 'rxjs';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var swal: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit, OnDestroy {
  // variables
  subscriptionLang: Subscription
  addTypeForm: FormGroup;
  langs: any[] = [];
  items: FormArray;
  defaultLanguage: number = 1;
  defaultLangs: any = {
    id: '',
    name: ''
  };

  /** translate strings */
  msg_server_error: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_unsuccess: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_swal_loading: string;
  msg_success_created: string;
 
  @ViewChild("f") myNgForm: any;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('WINE_SORT_ALERT_MSG_SUCCESS_CREATED').then((val: string) => {
      this.msg_success_created = val;
    });



    this.subscriptionLang = this.http.get('dropdown/language', 1)
      .subscribe(httpRequest => {
        this.langs = httpRequest.json();
        this.langs.forEach(item => {
          if(item.id == this.defaultLanguage) {
            this.defaultLangs.id = item.id;
            this.defaultLangs.name = item.name;
          }
        });
        this.addItem(this.defaultLangs);
    });

    this.addTypeForm = this.fb.group({
      items: this.fb.array([]),
      languages: [""] 
    });
  }

  createLanguage(languageId, languageName): FormGroup {
    return this.fb.group({
      name: ["", Validators.required],
      description: ["", Validators.required],
      language: languageId,
      language_name: [languageName]
    });
  }

  addItem(value): void {
    this.items = this.addTypeForm.get("items") as FormArray;
    this.items.push(this.createLanguage(value.id, value.name));
    // let index = this.langs.indexOf(value, 0);
    let index = this.langs.findIndex(e => e.id==value.id);
    this.langs.splice(index, 1);
  }
  // remove language
  onRemoveLangs(languageName, languageId, index) {
    let selected = this.addTypeForm.get("items") as FormArray;

    swal({
      title: this.msg_delete_lang_title + ` ${languageName}`,
      text: this.msg_delete_lang_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        swal({
          title: this.msg_delete_lang_success_title,
          text: `${languageName} ` + this.msg_delete_lang_success_text,
          type: "success",
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        });
        selected.removeAt(index);
        let renewLang = {
          name: languageName,
          id: languageId
        };
        this.langs.push(renewLang);
      }, (dismiss) => {

      }
    );
  }

  resetForm() {
    this.myNgForm.resetForm();
  }
  onSubmit() {
    let langForm = this.addTypeForm.get('items') as FormArray;
    let languages: any[] = [];

    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "description",
        value: formGroup.controls.description.value
      });
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "name",
        value: formGroup.controls.name.value
      });
    });
    this.addTypeForm.controls.languages.setValue(languages);

    let formInput = this.addTypeForm.value;
    delete formInput.items;

     // loading spinner on submit 
     swal({title: 'Molimo sačekajte', allowOutsideClick: false});
     swal.showLoading();
     // patch 
     this.http.post("create/wineClass", this.addTypeForm.value).subscribe(
       httpResponse => {
        swal.close();
          let controls = this.addTypeForm.get('items') as FormArray;
          while (controls.length !== 0) {
            controls.removeAt(0);
          }
          this.http.get('dropdown/language', 1).subscribe(lang => {
            this.langs = lang.json();
            this.addItem(this.defaultLangs);
          });
          languages = [];
          this.resetForm();
          this.alert.showNotification(
            this.msg_success_created,
            "success",
            "notification"
          );
       },
       error => {
         swal.close();
         this.alert.showNotification(
           this.msg_server_error,
           "danger",
           "error"
         );
       }
     );



  }

  ngOnDestroy() {

  }

}
