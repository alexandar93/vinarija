import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { TypeDataSource } from '../services/createtype.datasource';
import { HttpService } from '../services/http.service';
import { Router } from '@angular/router';
import { NotificationsService } from '../notifications/notifications.service';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TranslateHelperService } from './../services/translate-helper.service';

declare var swal: any;

@Component({
  selector: 'app-create-type',
  templateUrl: './create-type.component.html',
  styleUrls: ['./create-type.component.css']
})
export class CreateTypeComponent implements OnInit {
  //variables
  subscriptionLang: Subscription;
  subscriptionData: Subscription;
  total: number = null;
  pageSize: number = null;
  defaultLanguage: number = 1;
  selectedLanguage: number = null;
  languages: any[] = [];
  sort: string = 'asc';

  dataSource: TypeDataSource;
  displayedColumns = ["id", "name", "description", "actions"];

  /** translate strings */
  msg_server_error: string;
  msg_delete_type_title: string;
  msg_delete_type_text: string;
  msg_delete_type_success_title: string;
  msg_delete_type_success_text: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_delete_unsuccess: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(
    private http: HttpService,
    private router: Router,
    private alert: NotificationsService,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {

    // translate lable for pagination
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('WINE_SORT_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_delete_type_title = val;
    });
    this.translate.getTranslate('WINE_SORT_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_delete_type_text = val;
    });
    this.translate.getTranslate('WINE_SORT_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_type_success_title = val;
    });
    this.translate.getTranslate('WINE_SORT_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_type_success_text = val;
    });
    this.translate.getTranslate('WINE_SORT_ALERT_MSG_DELETE_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });

    // init table data
    this.selectedLanguage = this.defaultLanguage;
    this.dataSource = new TypeDataSource(this.http);
    this.dataSource.loadTypes(1, this.sort, this.defaultLanguage);
    // get all languages
    this.subscriptionLang = this.http.get("dropdown/language", 1).subscribe(httpResponse => {
      this.languages = httpResponse.json();
    });
    // get total and per page for table 
    this.subscriptionData = this.http.get('get/wineClass', 1).subscribe(httpResponse => {
        if(httpResponse.status === 200) {
            this.total = httpResponse.json().total;
            this.pageSize = httpResponse.json().per_page;
        }
    }, error => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })

  }
  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.loadTypePage())
    ).subscribe();
  }
  loadTypePage() {
    this.dataSource.loadTypes(
        this.paginator.pageIndex + 1, this.sort, this.selectedLanguage);
  }
  onAddType() {
    this.router.navigate(["create-type/add"]);
  }

  onEditType(id) {
    this.router.navigate(["create-type/edit", id]);
  }
   
  sortData(event) {
    if(event.direction == 'asc') {
      this.sort = event.direction;
      this.loadDataSource();
    }
    if(event.direction == 'desc') {
      this.sort = event.direction;
      this.loadDataSource();
    }
  }

  loadDataSource() {
    this.dataSource.loadTypes(this.paginator.pageIndex, this.sort, this.selectedLanguage);
    this.paginator.firstPage();
  }
  // change language for table
  onChangeLanguage(id: number, name: string) {
    this.selectedLanguage = id;
    this.dataSource = new TypeDataSource(this.http);
    
    this.loadDataSource();
    this.http
      .get("get/wineClass", this.selectedLanguage)
      .subscribe(httpResponse => {
        if(httpResponse.status === 200) {
          this.total = httpResponse.json().total;
          this.pageSize = httpResponse.json().per_page;
        }
      });
  }
  // delete type
  onDeleteType(id, name) {
    swal({
      title: this.msg_delete_type_title + ` ${name} ?`,
      text: this.msg_delete_type_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(() => {
      this.http.delete("delete/wineClass/" + id).subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          swal({
            title: this.msg_delete_type_success_title,
            text: `${name} ` + this.msg_delete_type_success_text,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          this.http.get('get/wineClass', this.selectedLanguage).subscribe(data => {
            this.paginator.length = data.json().total;
          })
          
          this.dataSource = new TypeDataSource(this.http);
          this.loadDataSource();
        }
        error => {
          this.alert.showNotification(
            this.msg_delete_unsuccess,
            "danger",
            "error"
          );
        };
      });
    }, (dismiss) => {
      
    });
  }

}
