import { Routes } from '@angular/router';

import { CreateTypeComponent } from './create-type.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
// import { ViewComponent } from './view/view.component';

export const TypeTable: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: CreateTypeComponent
        }]
    },
    {
        path: '',
        children: [{
            path: 'edit/:id',
            component: EditComponent
        }]
    },
    {
        path: '',
        children: [{
            path: 'add',
            component: AddComponent
        }]
    },
    // {
    //     path: '',
    //     children: [{
    //         path: 'view/:id',
    //         component: ViewComponent
    //     }]
    // }
];

