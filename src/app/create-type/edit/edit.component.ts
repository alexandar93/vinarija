import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { NotificationsService } from '../../notifications/notifications.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var swal: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  //variables
  subscriptionParams: Subscription;
  subscriptionLang: Subscription;
  subscriptionData: Subscription;
  editTypeForm: FormGroup;
  langs: any[] = [];
  items: FormArray;
  id: number;

  /** translate strings */
  msg_server_error: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_unsuccess: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_swal_loading: string;
  msg_success_edited: string;
  msg_save_previous_lang: string;
  msg_success_saved_previous_lang: string;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {

    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('WINE_SORT_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
      this.msg_success_edited = val;
    });
    this.translate.getTranslate('LANG_ALERT_REQ_SAVE_PREVIOUS_LANG').then((val: string) => {
      this.msg_save_previous_lang = val;
    });
    this.translate.getTranslate('LANG_ALERT_SUCCESS_SAVED').then((val: string) => {
      this.msg_success_saved_previous_lang = val;
    });


    // get url params for wine ID
    this.subscriptionParams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    // get languages
    this.subscriptionLang = this.http.get('dropdown/language', 1).subscribe(httpResponse => {
      this.langs = httpResponse.json();
    })
    // formGroup
    this.editTypeForm = this.fb.group({
      items: this.fb.array([]),
      languages: [""]
    });

    this.initLoadingData();
  }
  createLanguage(nameId, descId, typeName, typeDesc, lName, lId, isNew = true): FormGroup {
    let fg = this.fb.group({
      name: [typeName, Validators.required],
      description: [typeDesc, Validators.required],
      name_id: nameId,
      desc_id: descId,
      language_name: [lName],
      language_id: [lId],
      flag: ['']
    });
    if (isNew) fg.controls['flag'].setValue(1);
    return fg;
  }

  // adding new item to FormArray items and splice item from langsArray
  addItem(value): void {
    let isSaved;
    this.items = this.editTypeForm.get("items") as FormArray;
    this.items.controls.forEach(item => {
      let items = item as FormGroup;
      if (items.controls.flag.value === 1) {
        isSaved = true;
        swal({
          title: this.msg_save_previous_lang,
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success"
      });
      return;
      }
      isSaved = false;
    });
    if (!isSaved) {
    this.items.push(
      this.createLanguage(value.id, null , "", "", value.name, value.id)
    );
    // let index = this.langs.indexOf(value, 0);
    let index = this.langs.findIndex(e => e.id==value.id);
    this.langs.splice(index, 1);
  }
  }
  // remove all from items formArray
  removeItem() {
    let controls = this.editTypeForm.get('items') as FormArray;
    while (controls.length !== 0) {
      controls.removeAt(0);
    }
  }
  // generating new languages with data from server
  createItem(nameId, descId, typeName, typeDesc, lname, lid, isNew = true): void {
    let items = this.editTypeForm.get("items") as FormArray;
    items.push(this.createLanguage(nameId, descId, typeName, typeDesc, lname, lid, isNew));
  }
  // removing language form
  onRemoveLangs(language_name, language_id, index) {
    let selected = this.editTypeForm.get("items") as FormArray;

    swal({
      title: this.msg_delete_lang_title + ` ${language_name}`,
      text: this.msg_delete_lang_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        this.http
          .delete("delete/language/wineClass/" + this.id + "/" + language_id)
          .subscribe(httpResponse => {
            if (httpResponse.status === 204) {
              swal({
                title: this.msg_delete_lang_success_title,
                text: `${language_name} ` + this.msg_delete_lang_success_text,
                type: "success",
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
              });
              selected.removeAt(index);
              let renewLang = {
                name: language_name,
                id: language_id
              };
              this.langs.push(renewLang);
            } else if (httpResponse.status !== 204) {
              this.alert.showNotification(this.msg_delete_unsuccess, 'danger', 'notifications');
            }
          }, error => {
            this.alert.showNotification(this.msg_server_error, 'danger', '');
          });
      }, (dismiss) => {

      });
  }
  // saving when add new language
  onSaveLanguage(value) {
    let languageFormFields: any[] = [];
    languageFormFields.push({
      language_id: value.controls.name_id.value,
      name: "description",
      value: value.controls.description.value
    });
    languageFormFields.push({
      language_id: value.controls.name_id.value,
      name: "name",
      value: value.controls.name.value
    });
    let postData = {
      languages: languageFormFields
    };
    this.http
      .post("add/language/wineClass/" + this.id, postData)
      .subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          let itemArray = this.editTypeForm.controls['items'] as FormArray;
          itemArray.controls.forEach(element =>{
            element.markAsUntouched;
         });
          this.alert.showNotification(
            this.msg_success_saved_previous_lang,
            "success",
            "notifications"
          );
          this.removeItem()
          this.initLoadingData(true);
        }
      });
  }

  onSubmit() {
    let languages: any[] = [];
    let langForm = this.editTypeForm.get('items') as FormArray;

    // parse data from FormArray items to this.language array
    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      languages.push({
        id: formGroup.controls.desc_id.value,
        name: "description",
        value: formGroup.controls.description.value
      });
      languages.push({
        id: formGroup.controls.name_id.value,
        name: "name",
        value: formGroup.controls.name.value
      });
    });
    this.editTypeForm.controls.languages.setValue(languages);

    let formInput = this.editTypeForm.value;
    delete formInput.items;

    // loading spinner on submit 
    swal({title: this.msg_swal_loading, allowOutsideClick: false});
    swal.showLoading();
    // patch 
    this.http.postFormData("patch/wine/" + this.id, this.editTypeForm.value).subscribe(
      httpResponse => {
        //   if (event.type === HttpEventType.UploadProgress) {
        //     console.log(
        //       "Upload progress:" +
        //         Math.round(event.loaded / event.total * 100) +
        //         "%"
        //     )
        //     console.log(event);
        //   }
        if (httpResponse.status === 204) {
          swal.close();
          this.removeItem();
          this.initLoadingData(true);
          this.alert.showNotification(
            this.msg_success_edited,
            "success",
            "notification"
          );
        }
      },
      error => {
        swal.close();
        this.alert.showNotification(
          this.msg_server_error,
          "danger",
          "error"
        );
      }
    );

  }

  initLoadingData(onSave = false) {
    this.subscriptionData = this.http
      .get("patch/initialize/wineClass/" + this.id, 1)
      .subscribe(httpResponse => {
        if (httpResponse.status === 200) {
          let serverData = httpResponse.json();
          
          serverData.languages.forEach((lang, langIndex) => {
            let name = "";
            let desc = "";
            let fieldsindex;
            let name_id;
            let desc_id;

            lang.fields.forEach((field, fieldIndex) => {
              fieldsindex = fieldIndex;
              if (field.name === "name") {
                name = field.value;
                name_id = field.id;
              }
              if (field.name === "description") {
                desc = field.value;
                desc_id = field.id;
              }
            });
            if(!onSave) {
              var index = this.langs.findIndex(
                item => item.name === lang.language
              ); // SELECTING INDEX OF OBJECT IN ARRAY BY PROPERTY *(etc. name)
              this.langs.splice(index, 1);
              }
            let language_name = lang.language;
            let language_id = lang.language_id;
            this.createItem(
              name_id,
              desc_id,
              name,
              desc,
              language_name,
              language_id,
              false
            );
          });
        }
      }, error => {
          
      });
  }
}
