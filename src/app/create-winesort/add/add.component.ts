import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { NotificationsService } from '../../notifications/notifications.service';
import { Subscription } from 'rxjs';
import { ImageVideoValidatorsService } from '../../services/image-video-validators.service';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var swal: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit, OnDestroy {
  /** variables */
  
  /** subscription for list of languages */
  subscriptionLangs: Subscription;
  /** FormGroup for Adding wine-sort */
  addWineSortForm: FormGroup;
  /** language array */
  items: FormArray;
  /** array of langauges */
  langs: any[] = [];
  /** default language id */
  defaultLanguage: number = 1;
  /** cover image */
  coverImage: any = null;
  /** if form is valid */
  isFormValid;
  /** used for set name and id when add new language */
  defaultLangs: any = {
    id: '',
    name: ''
  };

  /** translate strings */
  msg_server_error: string;
  msg_file_is_not_image: string;
  msg_file_is_not_format: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_unsuccess: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_swal_loading: string;
  msg_success_created: string;
  msg_image_invalid: string;

  /** attached Form */
  @ViewChild('f') myNgForm: any;
  @ViewChild('coverPath') coverPath: any;

  constructor(
    private http: HttpService,
    private fb: FormBuilder,
    private alert: NotificationsService,
    private validate: ImageVideoValidatorsService,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_file_is_not_format = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('WINE_CATEGORIES_ALERT_MSG_SUCCESS_CREATED').then((val: string) => {
      this.msg_success_created = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_INVALID_IMAGE').then((val: string) => {
      this.msg_image_invalid = val;
    });

    this.addWineSortForm = this.fb.group({
      items: this.fb.array([]),
      cover: [""],
      languages: [""]
    });
    this.subscriptionLangs = this.http
      .get("dropdown/language", 1)
      .subscribe(httpResponse => {
        this.langs = httpResponse.json();
        this.langs.forEach(item => {
          if(item.id == this.defaultLanguage) {
            this.defaultLangs.id = item.id;
            this.defaultLangs.name = item.name;
          }
        });
        this.addItem(this.defaultLangs);
      }, err => {
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      });
  }
  /** creating formgroup with values for language */
  createLanguage(languageId, languageName): FormGroup {
    return this.fb.group({
      name: ["", Validators.required],
      language: languageId,
      language_name: [languageName]
    });
  }
  /** adding new items to language formArray */
  addItem(value): void {
    let items = this.addWineSortForm.get('items') as FormArray;
    items.push(this.createLanguage(value.id, value.name));
    // let index = this.langs.indexOf(value, 0);
    let index = this.langs.findIndex(e => e.id==value.id);
    this.langs.splice(index, 1);
  }
  /** remove language */
  onRemoveLangs(languageName, languageId, index) {
    let items = this.addWineSortForm.get("items") as FormArray;
    swal({
      title: this.msg_delete_lang_title + ` ${languageName}`,
      text: this.msg_delete_lang_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        swal({
          title: this.msg_delete_lang_success_title,
          text: `${languageName} ` + this.msg_delete_lang_success_text,
          type: "success",
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        });
        items.removeAt(index);
        let renewLang = {
          name: languageName,
          id: languageId
        };
        this.langs.push(renewLang);
      }, (dismiss) => {

      }
    ), error => {
        this.alert.showNotification(
          this.msg_delete_unsuccess,
          "danger",
          "error"
        );
      };
  }
  /** Upload image (cover) */
  onUploadCoverImage(event) {
    let file = <File>event.target.files[0];
    if(file !== undefined) {
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_file_is_not_image, 'danger', '');
      this.isFormValid = false;
      return false;
    }
    if(!this.validate.validateImage(file.name)) {
      this.alert.showNotification(this.msg_file_is_not_format, 'danger', '');
      this.isFormValid = false;
      return false;
    } else {
      this.isFormValid = true;
      this.coverImage = <File>event.target.files[0];
    }
    }
  }
  /** Reseting fields in form */
  resetForm() {
    this.myNgForm.resetForm();
  }
  /** submit form */
  onSubmit() {
    let items = this.addWineSortForm.get('items') as FormArray;
    const fd: FormData = new FormData();
    let languages: any[] = [];

    items.controls.forEach(element => {
      let formGroup = element as FormGroup;
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "name",
        value: formGroup.controls.name.value
      });
    });
    this.addWineSortForm.controls.languages.setValue(languages);

    let formInput = this.addWineSortForm.value;
    delete formInput.items;

     
     /** append data */
     if (this.coverImage !== undefined) {
      fd.append("cover", this.coverImage);
    }
    fd.append("json", JSON.stringify(formInput));
     /** create */
     if(this.isFormValid) {
       // loading spinner on submit 
     swal({title: this.msg_swal_loading, allowOutsideClick: false});
     swal.showLoading();
     this.http.postFormData("create/category", fd).subscribe(
       httpResponse => {
        swal.close();
          let controls = this.addWineSortForm.get('items') as FormArray;
          while (controls.length !== 0) {
            controls.removeAt(0);
          };
          this.coverPath.nativeElement.click();
          this.coverImage = null;
          this.http.get('dropdown/language', 1).subscribe(lang => {
            this.langs = lang.json();
            this.addItem(this.defaultLangs);
          });
          languages = [];
          this.resetForm();
          this.alert.showNotification(
            this.msg_success_created,
            "success",
            "notification"
          );
       },
       error => {
         swal.close();
         this.alert.showNotification(
           this.msg_server_error,
           "danger",
           "error"
         );
       }
     );
    } else {
      this.alert.showNotification(this.msg_image_invalid, 'danger', '');
    }
  }
  /** destroying subscriptions */
  ngOnDestroy() {
    this.subscriptionLangs.unsubscribe();
  }
}
