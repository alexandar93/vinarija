import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWinesortComponent } from './create-winesort.component';

describe('CreateWinesortComponent', () => {
  let component: CreateWinesortComponent;
  let fixture: ComponentFixture<CreateWinesortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateWinesortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWinesortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
