import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { WinesortDataSource } from '../services/winesort.datasource';
import { MatPaginator } from '@angular/material';
import { NotificationsService } from '../notifications/notifications.service';
import { Router } from '@angular/router';
import { HttpService } from '../services/http.service';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TranslateHelperService } from './../services/translate-helper.service';

declare var swal: any;
 
@Component({
  selector: 'app-create-winesort',
  templateUrl: './create-winesort.component.html',
  styleUrls: ['./create-winesort.component.css']
})
export class CreateWinesortComponent implements OnInit, OnDestroy {
    //variables
  subscriptionData: Subscription;
  subscriptionLang: Subscription;
  selectedLanguage: number = null;
  defaultLanguage: number = 1;
  sort: string = 'asc';
  total: number = null;
  pageSize: number = null;
  languages: any[] = [];

  dataSource: WinesortDataSource;
  displayedColumns = ["id", "name", "actions"];

  /** translate strings */
  msg_server_error: string;
  msg_delete_winesort_title: string;
  msg_delete_winesort_text: string;
  msg_delete_winesort_success_title: string;
  msg_delete_winesort_success_text: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_delete_unsuccess: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private http: HttpService,
    private router: Router,
    private alert: NotificationsService,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {

    // translate lable for pagination
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('WINE_CATEGORIES_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_delete_winesort_title = val;
    });
    this.translate.getTranslate('WINE_CATEGORIES_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_delete_winesort_text = val;
    });
    this.translate.getTranslate('WINE_CATEGORIES_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_winesort_success_title = val;
    });
    this.translate.getTranslate('WINE_CATEGORIES_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_winesort_success_text = val;
    });
    this.translate.getTranslate('WINE_CATEGORIES_ALERT_MSG_DELETE_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });



    // init table data
    this.selectedLanguage = this.defaultLanguage;
    this.dataSource = new WinesortDataSource(this.http);
    this.dataSource.loadWinesort(1, this.sort, this.defaultLanguage);
    // get all languages
    this.subscriptionLang = this.http.get("dropdown/language", 1).subscribe(httpResponse => {
      this.languages = httpResponse.json();
    });
    // get total and per page for table 
    this.subscriptionData = this.http.get('get/category/paginate', 1).subscribe(httpResponse => {
        if(httpResponse.status === 200) {
            this.total = httpResponse.json().total;
            this.pageSize = httpResponse.json().per_page;
        }
    }, error => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });

  }

  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.loadWinesortPage())
    ).subscribe();
  }
  loadWinesortPage() {
    this.dataSource.loadWinesort(
        this.paginator.pageIndex + 1, this.sort, this.selectedLanguage);
  }
  sortData(event) {
     if(event.direction == 'asc') {
      this.sort = event.direction;
      this.loadDataSource();
    }
    if(event.direction == 'desc') {
      this.sort = event.direction;
      this.loadDataSource();
    }
  }

  onAddWinesort() {
    this.router.navigate(["create-winesort/add"]);
  }

  onEditWinesort(id) {
    this.router.navigate(["create-winesort/edit", id]);
  }

  onDeleteWinesort(id, name) {
    swal({
      title: this.msg_delete_winesort_title + ` ${name} ?`,
      text: this.msg_delete_winesort_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(() => {
      this.http.delete("delete/category/" + id).subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          swal({
            title: this.msg_delete_winesort_success_title,
            text: `${name} ` + this.msg_delete_winesort_success_text,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          this.http.get('get/category', this.selectedLanguage).subscribe(data => {
            this.paginator.length = data.json().total;
          })
          
          this.dataSource = new WinesortDataSource(this.http);
          this.loadDataSource();
        }
        error => {
          this.alert.showNotification(
            this.msg_delete_unsuccess,
            "danger",
            "error"
          );
        };
      });
    }, (dismiss) => {
      
    });
  }

  loadDataSource() {
    this.dataSource.loadWinesort(this.paginator.pageIndex, this.sort, this.selectedLanguage);
    this.paginator.firstPage();
  }
  // change language for table
  onChangeLanguage(id: number, name: string) {
    this.selectedLanguage = id;
    this.dataSource = new WinesortDataSource(this.http);
    
    this.loadDataSource();
    this.http
      .get("get/category/paginate", this.selectedLanguage)
      .subscribe(httpResponse => {
        if(httpResponse.status === 200) {
          this.total = httpResponse.json().total;
          this.pageSize = httpResponse.json().per_page;
        }
      });
  }

  ngOnDestroy() {
    // this.subscriptionData.unsubscribe();
    // this.subscriptionLang.unsubscribe();
  }

}
