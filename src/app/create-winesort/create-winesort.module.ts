import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CreateWinesortRoute } from './create-winesort.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateWinesortComponent } from './create-winesort.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatInputModule, MatFormFieldModule, MatTableModule, MatSortModule, MatPaginatorModule, MatProgressSpinnerModule, MatTooltipModule, MatSelectModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateWinesortRoute),
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatSelectModule,
    TranslateModule
  ],
  declarations: [
    CreateWinesortComponent,
    AddComponent,
    EditComponent
  ],
  exports: [
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatSelectModule
  ]
})
export class CreateWinesortModule { }
