import { Routes } from "@angular/router";
import { CreateWinesortComponent } from "./create-winesort.component";
import { AddComponent } from "./add/add.component";
import { EditComponent } from "./edit/edit.component";

export const CreateWinesortRoute: Routes = [
    {
        path: '',
        component: CreateWinesortComponent
    }, {
        path: 'add',
        component: AddComponent
    }, {
        path: 'edit/:id',
        component: EditComponent
    }
]