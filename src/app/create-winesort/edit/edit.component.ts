import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { NotificationsService } from '../../notifications/notifications.service';
import { ImageVideoValidatorsService } from '../../services/image-video-validators.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { TranslateHelperService } from './../../services/translate-helper.service';


declare var swal: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, OnDestroy {

  /** variables */
  /** Subscription for languages */
  subscriptionLangs: Subscription;
  /** Subscription for params */
  subscriptionParams: Subscription;
  /** Subscription for server data */
  subscriptionData: Subscription;
  /** data from server  */
  serverData: any;
  /** language array */
  items: FormArray;
  /** cover image */
  coverImage: any = null;
  /** id of category */
  id: number = null;
  /** is form valid */
  isFormValid;
  /** list of languages */
  langs: any[] = [];
  /** edit formGroup */
  editWineSortForm: FormGroup;

  /** translate strings */
  msg_server_error: string;
  msg_file_is_not_image: string;
  msg_file_is_not_format: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_unsuccess: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_swal_loading: string;
  msg_success_edited: string;
  msg_image_invalid: string;
  msg_success_saved_previous_lang: string;
  msg_save_previous_lang: string;

  constructor(
    private http: HttpService,
    private alert: NotificationsService,
    private fb: FormBuilder,
    private validate: ImageVideoValidatorsService,
    private route: ActivatedRoute,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {

    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_file_is_not_format = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('WINE_CATEGORIES_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
      this.msg_success_edited = val;
    });
    this.translate.getTranslate('LANG_ALERT_REQ_SAVE_PREVIOUS_LANG').then((val: string) => {
      this.msg_save_previous_lang = val;
    });
    this.translate.getTranslate('LANG_ALERT_SUCCESS_SAVED').then((val: string) => {
      this.msg_success_saved_previous_lang = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_INVALID_IMAGE').then((val: string) => {
      this.msg_image_invalid = val;
    });

     // get get url params for wine ID
     this.subscriptionParams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    // form group
    this.editWineSortForm = this.fb.group({
      items: this.fb.array([]),
      cover: [''],
      languages: ['']
    });
    // list of available langs
    this.subscriptionLangs = this.http
      .get("dropdown/language", 1)
      .subscribe(httpResponse => {
        this.langs = httpResponse.json();
      }, err => {
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      });
      this.initLoadingData();
  }

  /** init loading data */
  initLoadingData(onSave = false) {
    this.subscriptionData = this.http
      .get("patch/initialize/category/" + this.id, 1)
      .subscribe(
        httpResponse => {
          if (httpResponse.status === 200) {
            let serverData = (this.serverData = httpResponse.json());
            
            this.coverImage = this.serverData.cover_image;
            serverData.languages.forEach((lang, langIndex) => {
              let name = "";
              let fieldsindex;
              let name_id;

              lang.fields.forEach((field, fieldIndex) => {
                fieldsindex = fieldIndex;
                if (field.name === "name") {
                  name = field.value;
                  name_id = field.id;
                }
              });
              if (!onSave) {
                var index = this.langs.findIndex(
                  item => item.name === lang.language
                ); // SELECTING INDEX OF OBJECT IN ARRAY BY PROPERTY *(etc. name)
                this.langs.splice(index, 1);
              }
              let language_name = lang.language;
              let language_id = lang.language_id;
              this.createItem(
                name_id,
                name,
                language_name,
                language_id,
                false
              );
            });
          }
        },
        error => {}
      );
  }
  /** Upload image (cover) */
  onUploadCoverImage(event) {
    let file = <File>event.target.files[0];
    if(file !== undefined) {
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_file_is_not_image, 'danger', '');
      this.isFormValid = false;
      return false;
    }
    if(!this.validate.validateImage(file.name)) {
      this.alert.showNotification(this.msg_file_is_not_format, 'danger', '');
      this.isFormValid = false;
      return false;
    } else {
      this.isFormValid = true;
      this.coverImage = <File>event.target.files[0];
    }
    }
  }

  /** creating new formGroup for languages inputs */
  createLanguage(
    nameId,
    categoryName,
    lName,
    lId,
    isNew = true
  ): FormGroup {
    let fg = this.fb.group({
      name: [categoryName, Validators.required],
      name_id: nameId,
      language_name: [lName],
      language_id: [lId],
      flag: [""]
    });
    if (isNew) fg.controls["flag"].setValue(1);
    return fg;
  } 
  /** adding new item to FormArray items and splice item from langsArray */
  addItem(value): void {
    let isSaved;
    let items = this.editWineSortForm.get("items") as FormArray;
    items.controls.forEach(item => {
      let items = item as FormGroup;
      if (items.controls.flag.value === 1) {
        isSaved = true;
        swal({
          title: this.msg_save_previous_lang,
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success"
        });
        return;
      }
      isSaved = false;
    });
    if (!isSaved) {
      items.push(
        this.createLanguage(value.id, "", value.name, value.id)
      );
      let index = this.langs.indexOf(value, 0);
      this.langs.splice(index, 1);
    }
  }

  /** remove all from items formArray */
  removeItem() {
    let controls = this.editWineSortForm.get("items") as FormArray;
    while (controls.length !== 0) {
      controls.removeAt(0);
    }
  }

  /** generating new languages with data from server */
  createItem(nameId, categoryName, lname, lid, isNew = true): void {
    let items = this.editWineSortForm.get("items") as FormArray;
    items.push(
      this.createLanguage(nameId, categoryName, lname, lid, isNew)
    );
  }

  /** saving when add new language */
  onSaveLanguage(value) {
    let languageFormFields: any[] = [];
    languageFormFields.push({
      language_id: value.controls.name_id.value,
      name: "name",
      value: value.controls.name.value
    });
    let postData = {
      languages: languageFormFields
    };
    this.http
      .post("add/language/category/" + this.id, postData)
      .subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          let itemArray = this.editWineSortForm.controls["items"] as FormArray;
          itemArray.controls.forEach(element => {
            element.markAsUntouched;
          });
          this.alert.showNotification(
            this.msg_success_saved_previous_lang,
            "success",
            "notifications"
          );
          this.removeItem();
          this.initLoadingData(true);
        }
      });
  }
  /** removing language form */
onRemoveLangs(language_name, language_id, index) {
  let selected = this.editWineSortForm.get("items") as FormArray;

  swal({
    title: this.msg_delete_lang_title + ` ${language_name}`,
    text: this.msg_delete_lang_text,
    type: "warning",
    showCancelButton: true,
    cancelButtonText: this.msg_swal_button_no,
    confirmButtonClass: "btn btn-success",
    cancelButtonClass: "btn btn-danger",
    confirmButtonText: this.msg_swal_button_yes,
    buttonsStyling: false
  }).then(
    () => {
      this.http
        .delete("delete/language/category/" + this.id + "/" + language_id)
        .subscribe(httpResponse => {
          if (httpResponse.status === 204) {
            swal({
              title: this.msg_delete_lang_success_title,
              text: `${language_name} ` + this.msg_delete_lang_success_text,
              type: "success",
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            });
            selected.removeAt(index);
            let renewLang = {
              name: language_name,
              id: language_id
            };
            this.langs.push(renewLang);
          } else if (httpResponse.status !== 204) {
            this.alert.showNotification(this.msg_delete_unsuccess, 'danger', 'notifications');
          }
        }, error => {
          this.alert.showNotification(this.msg_server_error, 'danger', '');
        });
    }, (dismiss) => {

    });
}
  /** submit form */
  onSubmit(){
    const fd: FormData = new FormData();
    let languages: any[] = [];
    let langForm = this.editWineSortForm.get('items') as FormArray;
    // parse data from FormArray items to this.language array
    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      languages.push({
        id: formGroup.controls.name_id.value,
        name: "name",
        value: formGroup.controls.name.value
      });
    });
    this.editWineSortForm.controls.languages.setValue(languages);
    let formInput = this.editWineSortForm.value;
    delete formInput.items;
    if (this.coverImage !== undefined) {
      fd.append("cover", this.coverImage);
    }
    fd.append("json", JSON.stringify(formInput));
  
    if(this.isFormValid) {
      this.http.postFormData('patch/category/' + this.id, fd).subscribe(httpResponse => {
        if(httpResponse.status == 204) {
          this.removeItem();
          this.initLoadingData(true);
          this.alert.showNotification(
            this.msg_success_edited,
            "success",
            "notification"
          );
        }
      }, err => {
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      })
    } else {
      this.alert.showNotification(this.msg_image_invalid, 'dangeer', '');
    }
  }
  /** destroying subscriptions */
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptionLangs.unsubscribe();
    this.subscriptionParams.unsubscribe();
    this.subscriptionData.unsubscribe();
  }

}
