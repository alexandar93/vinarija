import {
  Component,
  OnInit,
  OnDestroy,
  NgZone,
  ElementRef,
  ViewChild
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl,
  AbstractControl
} from "@angular/forms";
import { Subscription } from "rxjs/Subscription";
import { HttpService } from "../../services/http.service";

import { AgmCoreModule, MapsAPILoader, GoogleMapsAPIWrapper } from "@agm/core";
import {} from "googlemaps";
import { forEach } from "@angular/router/src/utils/collection";
import { NotificationsService } from "../../notifications/notifications.service";
import { MatDatepicker } from "@angular/material";
import * as moment from "moment";
import { ImageVideoValidatorsService } from "../../services/image-video-validators.service";
import { Globals } from "../../model/globals";
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var google;
declare var swal;
declare var $;

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.css"]
})
export class AddComponent implements OnInit, OnDestroy {
  // variables
  subscriptionLang: Subscription; // subscription for languages

  eventForm: FormGroup; // form
  languages: any[] = []; // storage formated name and description/link for send to backend
  defaultLanguage: number = 1; // default language Srpski = 1
  langs: any[] = []; // storage of all languages
  lat: number;
  lng: number;
  coverFile;
  items: any;
  minDate: any;
  isFormValid = true;
  urlPattern = "https?://.+"; // pattern for web site url
  defaultLangs: any = {
    id: '',
    name: ''
  }
  modules: any;

  /** translate strings */
  msg_server_error: string;
  msg_image_file_is_not_valid: string;
  msg_image_file_format: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_lang_unsuccess: string;
  msg_uploaded_image_invalid: string;
  msg_success_created: string;

  @ViewChild(MatDatepicker) datepicker: MatDatepicker<Date>;
  @ViewChild("address") public searchElementRef: ElementRef;
  @ViewChild("f") myNgForm: any;
  @ViewChild("coverPath") coverPath: any;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private alert: NotificationsService,
    private validate: ImageVideoValidatorsService,
    private globals: Globals,
    private translate: TranslateHelperService
  ) {
    this.modules = this.globals.editor_settings;
  }

  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_lang_unsuccess = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_image_file_is_not_valid = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_image_file_format = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('EVENTS_ALERT_MSG_SUCCESS_CREATED').then((val: string) => {
      this.msg_success_created = val;
    });
    

    this.lat = 44;
    this.lng = 19;
    this.subscriptionLang = this.http
      .get("dropdown/language", this.defaultLanguage)
      .subscribe(httpResponse => {
        this.langs = httpResponse.json();
        this.langs.forEach(item => {
          if(item.id == this.defaultLanguage) {
            this.defaultLangs.id = item.id;
            this.defaultLangs.name = item.name;
          }
      });
        this.addItem(this.defaultLangs);
      }, err => {
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      });
    this.eventForm = this.fb.group({
      start: ["", Validators.required],
      end: ["", Validators.required],
      location: ["", Validators.required],
      lat: [""],
      lng: [""],
      link: [{ value: "", disabled: true }, Validators.compose([Validators.pattern(this.urlPattern), Validators.required])],
      items: this.fb.array([
        // this.createLanguage(this.defaultLanguage.id, this.defaultLanguage.name)
      ]),
      languages: [""]
    });

    //set current position
    this.setCurrentPosition();
    //get lng and lat for map
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement,
        { types: ["address"] }
      );
       // Set initial restrict to the greater list of countries.
       autocomplete.setComponentRestrictions(
        {'country': ['rs']});
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        });
      });
    });

    if ($(".selectpicker").length !== 0) {
      $(".selectpicker").selectpicker();
    }

    $(".timepicker").datetimepicker({
      format: "H:mm", // use this format if you want the 24hours timepicker
      // format: 'h:mm A',    //use this format if you want the 12hours timpiecker with AM/PM toggle
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: "fa fa-chevron-left",
        next: "fa fa-chevron-right",
        today: "fa fa-screenshot",
        clear: "fa fa-trash",
        close: "fa fa-remove",
        inline: true,
        sideBySide: true
      }
    });
  }
  addEvent(title, event) {
    let selectedDate = event.target.value;
    selectedDate = moment(selectedDate).format('YYYY-MM-DD');
    this.minDate = selectedDate;
  }
  isDisabled(value, index) {
    let select = this.eventForm.get("items") as FormArray;

    let formGroup = select.controls[index] as FormGroup;
    if (value == true) {
      formGroup.controls.description.disable();
    }
    if (value == false) {
      formGroup.controls.description.enable();
      this.eventForm.controls.link.disable();
    }
    select.controls.forEach(element => {
      let item = element as FormGroup;
      if(item.controls.description.disabled === true) {
        this.eventForm.controls.link.enable();
      }
    });
  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      });
    }
  }
   private onDropModel(args) {
    let [el, target, source] = args;
    // do something else
  }

  private onRemoveModel(args) {
    let [el, source] = args;
    // do something else
  }
  // setting marker on map
  onChoseLocation(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
  }
  // uploading image storage
  onUploadCover(event) {
    let file = <File>event.target.files[0];
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_image_file_is_not_valid, 'danger', '');
      this.isFormValid = false;
      return false;
    }
    if(!this.validate.validateImage(file.name)) {
      this.alert.showNotification(this.msg_image_file_format, 'danger', '');
      this.isFormValid = false;
      return false;
    } else {
      this.isFormValid = true;
      this.coverFile = <File>event.target.files[0];
    }
  }
  // creating formGroup when pick language from dropdown
  createLanguage(languageId, languageName): FormGroup {
    return this.fb.group({
      name: ["", Validators.required],
      description: [{ value: "", disabled: false }, Validators.required],
      language: [languageId],
      language_name: [languageName]
    });
  }
  // adding new created language in FormArray
  addItem(value): void {
    this.items = this.eventForm.get("items") as FormArray;
    this.items.push(this.createLanguage(value.id, value.name));
    // let index = this.langs.indexOf(value, 0);
    let index = this.langs.findIndex(e => e.id==value.id);
    this.langs.splice(index, 1);
  }
  // removing language
  onRemoveLangs(languageName, languageId, index) {
    let selected = this.eventForm.get("items") as FormArray;

    swal({  
      title: this.msg_delete_lang_title + ` ${languageName}`,
      text: this.msg_delete_lang_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        swal({
          title: this.msg_delete_lang_success_title,
          text: `${languageName} ` + this.msg_delete_lang_success_text,
          type: "success",
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        });
        selected.removeAt(index);
        let renewLang = {
          name: languageName,
          id: languageId
        };
        this.langs.push(renewLang);
      }, (dismiss) => {

      }
    );
  }
  resetForm() {
    this.myNgForm.resetForm();
  }
  onSubmit() {
    let time: string[] = [];

    $("input.timepicker").each(function(index, value) {
      time[index] = $(value).data("date");
    });
    let formatStart = this.eventForm.controls.start.value;
    let formatEnd = this.eventForm.controls.end.value;

    formatStart = moment(formatStart).format("YYYY-MM-DD"); // formatiranje datuma
    formatEnd = moment(formatEnd).format("YYYY-MM-DD"); // formatiranje datuma
    let startDate = formatStart + " " + time[0];
    let endDate = formatEnd + " " + time[1];

    const fd: FormData = new FormData();
    let langForm = this.eventForm.get("items") as FormArray;
    let languages: any[] = [];

    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      if(formGroup.controls.description.disabled !== true) {
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "description",
        value: formGroup.controls["description"].value
      });
    } else {
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "description",
        value: ""
      });
    }
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "name",
        value: formGroup.controls["name"].value
      });
    });
    
    this.eventForm.controls.location.setValue(this.searchElementRef.nativeElement.value);
    this.eventForm.controls.languages.setValue(languages);
    this.eventForm.controls.start.setValue(startDate);
    this.eventForm.controls.end.setValue(endDate);
    this.eventForm.controls.lat.setValue(this.lat);
    this.eventForm.controls.lng.setValue(this.lng);

    let formInput = this.eventForm.value;
    
    if(this.eventForm.controls.link.disabled === true) {
      delete formInput.link;
    }
    delete formInput.items;
    if (this.coverFile !== undefined) {
      fd.append("cover", this.coverFile);
    }

    fd.append("json", JSON.stringify(formInput));
    if(this.isFormValid) {
    this.http.postFormData("create/happening", fd).subscribe(httpResponse => {
      if (httpResponse.status === 201) {

            this.http.get('dropdown/language', 1).subscribe(lang => {
              if(lang.status == 200) {
                this.langs = lang.json();
                while (langForm.length !== 0) {
                  langForm.removeAt(0);
                }
                this.addItem(this.defaultLangs);
              }
            });
            this.coverPath.nativeElement.click();
            this.resetForm();
            this.coverFile = null;

            $("input.timepicker").each((index, value) => {
              $($(value).data("date", '')).val('');
             });
            this.alert.showNotification(this.msg_success_created, 'success', 'notifications');
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, "danger", "");
    });
  } else {
    // preventing red fields after creating with invalid picture failed
    this.eventForm.controls.start.setValue(startDate);
    this.eventForm.controls.end.setValue(endDate);
    this.alert.showNotification(this.msg_uploaded_image_invalid, 'danger', '');
    return false;
  }
  }

  ngOnDestroy() {
    this.subscriptionLang.unsubscribe();
  }
}
