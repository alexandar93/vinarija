import {
  Component,
  OnInit,
  OnDestroy,
  NgZone,
  ElementRef,
  ViewChild
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl,
  AbstractControl
} from "@angular/forms";
import { Subscription } from "rxjs/Subscription";
import { HttpService } from "../../services/http.service";

import { AgmCoreModule, MapsAPILoader, GoogleMapsAPIWrapper } from "@agm/core";
import {} from "googlemaps";
import { forEach } from "@angular/router/src/utils/collection";
import { NotificationsService } from "../../notifications/notifications.service";
import { MatDatepicker } from "@angular/material";
import * as moment from "moment";
import { ActivatedRoute } from "@angular/router";
import { ImageVideoValidatorsService } from "../../services/image-video-validators.service";
import { Globals } from "../../model/globals";
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var $;
declare var swal;
declare var google;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, OnDestroy {
  //variables
  subscriptionLang: Subscription;
  subscriptionParams: Subscription;
  subscriptionData: Subscription;
  eventForm: FormGroup; // form
  languages: any[] = []; // storage formated name and description/link for send to backend
  defaultLanguage: number = 1; // default language Srpski = 1
  langs: any[] = []; // storage of all languages
  lat: number;
  lng: number;
  coverImage: any = null;
  coverFile: any;
  items: any;
  minDate: any;
  id: number; // event ID;
  urlPattern = "https?://.+"; // pattern for web site url
  // checkDesc: any;
  // checkLink: any;
  isFormValid = true;
  modules: any;
  /** translate strings */
  msg_server_error: string;
  msg_alert_lang_is_not_saved: string;
  msg_alert_lang_success_saved: string;
  msg_file_is_not_image: string;
  msg_file_is_not_image_format: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_lang_unsuccess: string;
  msg_button_no: string;
  msg_button_yes: string;
  msg_success_edited: string;
  msg_unsuccess_edit: string;
  msg_uploaded_file_invalid: string;
  msg_swal_loading: string;

  @ViewChild(MatDatepicker) datepicker: MatDatepicker<Date>;
  @ViewChild("address") public searchElementRef: ElementRef;
  
  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private validate: ImageVideoValidatorsService,
    private globals: Globals,
    private translate: TranslateHelperService
  ) { 
    this.modules = this.globals.editor_settings;
  }

  ngOnInit() {

     /** translate */
     this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
       this.msg_server_error = val;
     });
     this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
       this.msg_button_yes = val;
     });
     this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
       this.msg_button_no = val;
     });
     this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
       this.msg_delete_lang_title = val;
     });
     this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
       this.msg_delete_lang_text = val;
     });
     this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
       this.msg_delete_lang_success_title = val;
     });
     this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
       this.msg_delete_lang_success_text = val;
     });
     this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
       this.msg_delete_lang_unsuccess = val;
     });
     this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
       this.msg_file_is_not_image = val;
     });
     this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
       this.msg_file_is_not_image_format = val;
     });
     this.translate.getTranslate('EVENTS_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
       this.msg_success_edited = val;
     });
     this.translate.getTranslate('EVENTS_ALERT_MSG_UNSUCCESS').then((val: string) => {
       this.msg_unsuccess_edit = val;
     });
     this.translate.getTranslate('LANG_ALERT_REQ_SAVE_PREVIOUS_LANG').then((val: string) => {
       this.msg_alert_lang_is_not_saved = val;
     });
     this.translate.getTranslate('LANG_ALERT_SUCCESS_SAVED').then((val: string) => {
       this.msg_alert_lang_success_saved = val;
     });
     this.translate.getTranslate('FILES_ALERT_MSG_INVALID_IMAGE').then((val: string) => {
       this.msg_uploaded_file_invalid = val;
     });
     this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
       this.msg_swal_loading = val;
     });
    // get get url params for event ID
    this.subscriptionParams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    // get list of all languages
    this.subscriptionLang = this.http
      .get("dropdown/language", this.defaultLanguage)
      .subscribe(httpResponse => {
        this.langs = httpResponse.json();
      });

    this.eventForm = this.fb.group({
      start: ["", Validators.required],
      end: ["", Validators.required],
      location: ["", Validators.required],
      lat: [""],
      lng: [""],
      link: [{ value: "", disabled: true }, Validators.compose([Validators.required, Validators.pattern(this.urlPattern)])],
      items: this.fb.array([
        // this.createLanguage(this.defaultLanguage.id, this.defaultLanguage.name)
      ]),
      languages: [""]
    });
    this.initLoadingData();
    //set current position
    this.setCurrentPosition();
    //get lng and lat for map
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement,
        { types: ["address"] }
      );
       // Set initial restrict to the greater list of countries.
       autocomplete.setComponentRestrictions(
        {'country': ['rs']});
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        });
      });
    });

    if ($(".selectpicker").length !== 0) {
      $(".selectpicker").selectpicker();
    }

    $(".timepicker").datetimepicker({
      format: "H:mm", // use this format if you want the 24hours timepicker
      // format: 'h:mm A',    //use this format if you want the 12hours timpiecker with AM/PM toggle
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: "fa fa-chevron-left",
        next: "fa fa-chevron-right",
        today: "fa fa-screenshot",
        clear: "fa fa-trash",
        close: "fa fa-remove",
        inline: true,
        sideBySide: true
      }
    });
    //set current position
    this.setCurrentPosition();
    //get lng and lat for map
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        });
      });
    });
  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      });
    }
  }
  private onDropModel(args) {
    let [el, target, source] = args;
    // do something else
  }

  private onRemoveModel(args) {
    let [el, source] = args;
    // do something else
  }
  onChoseLocation(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
  }
  // adding new item to FormArray items and splice item from langsArray
  addItem(value): void {
  let isSaved;
  let items = this.eventForm.get("items") as FormArray;
  items.controls.forEach(item => {
    let items = item as FormGroup;
    if (items.controls.flag.value === 1) {
      isSaved = true;
      swal({
        title: this.msg_alert_lang_is_not_saved,
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success"
    });
    return;
    }
    isSaved = false;
  });
  if(!isSaved) {
    items.push(
      this.createLanguage(value.id, null, "", "", value.name, value.id)
    );
    // let index = this.langs.indexOf(value, 0);
    let index = this.langs.findIndex(e => e.id==value.id);
    this.langs.splice(index, 1);
  }
}
  // remove all from items formArray
removeItem() {
  let controls = this.eventForm.get('items') as FormArray;
  while (controls.length !== 0) {
  controls.removeAt(0);
  }
}

// creating formGroup when pick language from dropdown
createLanguage(nameId, descId, eventName, eventDesc, langName, langId, isNew = true): FormGroup {
  let fg = this.fb.group({
    name: [eventName, Validators.required],
    description: [{ value: eventDesc, disabled: false }, Validators.required],
    name_id: nameId,
    desc_id: descId,
    language_name: [langName],
    language_id: [langId],
    flag: ['']
  });
  if (isNew) fg.controls['flag'].setValue(1);
  return fg;
}
// generating new languages with data from server
createItem(nameId, descId, eventName, eventDesc, langName, langId, isNew = true): void {
  let items = this.eventForm.get("items") as FormArray;
  items.push(this.createLanguage(nameId, descId, eventName, eventDesc, langName, langId, isNew));
}

// saving when add new language
onSaveLanguage(value) {
  let languageFormFields: any[] = [];
  languageFormFields.push({
    language_id: value.controls.name_id.value,
    name: "description",
    value: value.controls.description.value
  });
  languageFormFields.push({
    language_id: value.controls.name_id.value,
    name: "name",
    value: value.controls.name.value
  });
  let postData = {
    languages: languageFormFields
  };
  this.http
    .post("add/language/happening/" + this.id, postData)
    .subscribe(httpResponse => {
      if (httpResponse.status === 204) {
        let itemArray = this.eventForm.controls['items'] as FormArray;
        itemArray.controls.forEach(element =>{
          element.markAsUntouched;
        });
        this.alert.showNotification(
          this.msg_alert_lang_success_saved,
          "success",
          "notifications"
        );
        this.removeItem()
        this.initLoadingData(true);
      }
    });
}
// removing language form
onRemoveLangs(language_name, language_id, index) {
  let selected = this.eventForm.get("items") as FormArray;

  swal({
    title: this.msg_delete_lang_title + ` ${language_name}`,
    text: this.msg_delete_lang_text,
    type: "warning",
    showCancelButton: true,
    cancelButtonText: this.msg_button_no,
    confirmButtonClass: "btn btn-success",
    cancelButtonClass: "btn btn-danger",
    confirmButtonText: this.msg_button_yes,
    buttonsStyling: false
  }).then(
    () => {
      this.http
        .delete("delete/language/happening/" + this.id + "/" + language_id)
        .subscribe(httpResponse => {
          if (httpResponse.status === 204) {
            console.log(language_name, language_id, index);
            swal({
              title: this.msg_delete_lang_success_title,
              text: `${language_name} ` + this.msg_delete_lang_success_text,
              type: "success",
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            });
            selected.removeAt(index);
            let renewLang = {
              name: language_name,
              id: language_id
            };
            this.langs.push(renewLang);
          } else if (httpResponse.status !== 204) {
            this.alert.showNotification(this.msg_delete_lang_unsuccess, 'danger', 'notifications');
          }
        }, error => {
          this.alert.showNotification(this.msg_server_error, 'danger', '');
        });
    }, (dismiss) => {

    });
}
initLoadingData(onSave = false) {
  this.subscriptionData = this.http.get('patch/initialize/happening/' + this.id, 1).subscribe(httpResponse => {
    if(httpResponse.status == 200) {
      let serverData = httpResponse.json();
      
      let formatStart = serverData.start;
      let formatEnd = serverData.end;
      let formatStartTime = serverData.start;
      let formatEndTime = serverData.end;
      formatStart = moment(formatStart).format("YYYY-MM-DD"); // formatiranje datuma
      formatEnd = moment(formatEnd).format("YYYY-MM-DD"); // formatiranje datuma
      formatStartTime = moment(formatStartTime).format("HH:mm"); // formatiranje vremena
      formatEndTime = moment(formatEndTime).format("HH:mm"); // formatiranje vremena
      this.eventForm.controls.start.setValue(formatStart);
      this.eventForm.controls.end.setValue(formatEnd);
      this.eventForm.controls.location.setValue(serverData.location);
      this.coverImage = serverData.image_path;
      this.lat = serverData.lat;
      this.lng = serverData.lng;
      // console.log(formatStart, formatEnd)
      if(serverData.link != null) {
        this.eventForm.controls.link.enable();
        this.eventForm.controls.link.setValue(serverData.link);
      }

      $("input.timepicker.start").each((index, value) => {
        $($(value).data("date", formatStartTime)).val(formatStartTime);
        });
        $("input.timepicker.end").each((index, value) => {
          // console.log(formatEndTime);
        $($(value).data("date", formatEndTime)).val(formatEndTime);
        });

        serverData.languages.forEach((lang, langIndex) => {
        let name = "";
        let desc = "";
        let fieldsindex;
        let name_id;
        let desc_id;

        lang.fields.forEach((field, fieldIndex) => {
          fieldsindex = fieldIndex;
          if (field.name === "name") {
            name = field.value;
            name_id = field.id;
          }
          if (field.name === "description") {
            desc = field.value;
            desc_id = field.id;
          }
        });

        if(!onSave) {
          var index = this.langs.findIndex(
            item => item.name === lang.language
          ); // SELECTING INDEX OF OBJECT IN ARRAY BY PROPERTY *(etc. name)
          this.langs.splice(index, 1);
          }
        let language_name = lang.language;
        let language_id = lang.language_id;
        this.createItem(
          name_id,
          desc_id,
          name,
          desc,
          language_name,
          language_id,
          false
        );
      });
    }
  }, err => {
    this.alert.showNotification(this.msg_server_error, 'danger', '');
  })
}
 // on uploading image 
 onUploadCoverImage(event) {
  let file = <File>event.target.files[0];
  if(file.type.indexOf('image') == -1) {
    this.alert.showNotification(this.msg_file_is_not_image, 'danger', '');
    this.isFormValid = false;
    return false;
  }
  if(!this.validate.validateImage(file.name)) {
    this.alert.showNotification(this.msg_file_is_not_image_format, 'danger', '');
    this.isFormValid = false;
    return false;
  } else {
    this.isFormValid = true;
    this.coverFile = <File>event.target.files[0];
  }
}
// radio buttons for desc or link selected
isDisabled(value, index) {
  let select = this.eventForm.get("items") as FormArray;

  let formGroup = select.controls[index] as FormGroup;
  if (value == true) {
    formGroup.controls.description.disable();
  }
  if (value == false) {
    formGroup.controls.description.enable();
    this.eventForm.controls.link.disable();
  }
  select.controls.forEach(element => {
    let item = element as FormGroup;
    if(item.controls.description.disabled === true) {
      this.eventForm.controls.link.enable();
    }
  });
}
formattingDate(date) {
  let value = moment(date).format("YYYY-MM-DD"); // formatiranje datuma
  return value;
}
addEvent(value, event) {
  let selectedDate = event.target.value;
  selectedDate = moment(selectedDate).format('YYYY-MM-DD');
  this.minDate = selectedDate;
}
onSubmit() {
  const fd: FormData = new FormData();
  let time: any[] = [];
  let languages: any[] = [];
  let langForm = this.eventForm.get('items') as FormArray;
  let startDate = this.eventForm.controls.start.value;
  let endDate = this.eventForm.controls.end.value;

  // parse data from FormArray items to this.language array
  langForm.controls.forEach(element => {
    let formGroup = element as FormGroup;
    // if(formGroup.controls.description.disabled === true) {
    //   languages.push({
    //     language_id: formGroup.controls.desc_id.value,
    //     name: "descripiton",
    //     value: ""
    //   });
    // }
    if(formGroup.controls.description.disabled !== true) {
    if(formGroup.controls.desc_id.value === null) {
      languages.push({
        language_id: formGroup.controls.language_id.value,
        name: "description",
        value: formGroup.controls.description.value
      });
    } else {
    languages.push({
      id: formGroup.controls.desc_id.value,
      name: "description",
      value: formGroup.controls.description.value
    });
  }
}
    languages.push({
      id: formGroup.controls.name_id.value,
      name: "name",
      value: formGroup.controls.name.value
    });
  });
  this.eventForm.controls.languages.setValue(languages);
  this.eventForm.controls.lat.setValue(this.lat);
  this.eventForm.controls.lng.setValue(this.lng);

  $("input.timepicker").each((index, value) => {
    let d = $(value).data('date');
    time.push(d);
  });

  let mergeStartDateTime = this.formattingDate(startDate) + ' ' + time[0];
  let mergeEndDateTime = this.formattingDate(endDate) + ' ' + time[1];
  this.eventForm.controls.start.setValue(mergeStartDateTime);
  this.eventForm.controls.end.setValue(mergeEndDateTime);
  let formInput = this.eventForm.value;
    delete formInput.items;
    if (this.coverFile !== undefined) {
      fd.append("cover", this.coverFile);
    }
    fd.append("json", JSON.stringify(formInput));

    if(this.isFormValid) {
      swal({ title: this.msg_swal_loading, allowOutsideClick: false });
      swal.showLoading();
      this.http.postFormData("patch/happening/" + this.id, fd).subscribe(httpResponse => {
        if(httpResponse.status == 204) {
          swal.close();
          this.removeItem();
          this.initLoadingData(true);
          this.alert.showNotification(
            this.msg_success_edited,
            "success",
            "notification"
          );
        }
      }, error =>  {
          swal.close();
          this.alert.showNotification(
            this.msg_unsuccess_edit,
            "danger",
            "error");
      })
    } else {
      // preventing invalid data to date fields if patch failed
      this.eventForm.controls.start.setValue(this.formattingDate(startDate));
      this.eventForm.controls.end.setValue(this.formattingDate(endDate));
      this.alert.showNotification(this.msg_uploaded_file_invalid, 'danger', '');
    }
}

ngOnDestroy() {
  this.subscriptionLang.unsubscribe();
  this.subscriptionParams.unsubscribe();
  this.subscriptionData.unsubscribe();
}
}
