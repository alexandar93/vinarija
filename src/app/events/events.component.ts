import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource } from "@angular/material";
import { SelectionModel, DataSource } from "@angular/cdk/collections";

import { HttpService } from '../services/http.service';
import { NotificationsService } from '../notifications/notifications.service';
import { EventsDataSource } from '../services/event.datasource';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';
import { TranslateHelperService } from './../services/translate-helper.service';

declare var swal;

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  subscription: Subscription;
  subscriptionLang: Subscription;
  defaultLanguage: number = 1;
  selectedLanguage: number;
  languages: any[] = [];
  total;
  pageSize;
  sort: string = 'asc';

  dataSource: EventsDataSource;
  displayedColumns = ['name', 'start', 'end', 'location', 'actions'];
  /** translate */
  msg_server_error: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_alert_delete_event_title: string;
  msg_alert_delete_event_text: string;
  msg_alert_delete_event_success_title: string;
  msg_alert_delete_event_success_text: string;
  msg_alert_delete_event_unsuccess: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private router: Router,
    private http: HttpService,
    private alert: NotificationsService,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {
    //translate label for pagination
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('EVENTS_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_alert_delete_event_title = val;
    });
    this.translate.getTranslate('EVENTS_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_alert_delete_event_text = val;
    });
    this.translate.getTranslate('EVENTS_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_alert_delete_event_success_title = val;
    });
    this.translate.getTranslate('EVENTS_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_alert_delete_event_success_text = val;
    });
    this.translate.getTranslate('EVENTS_ALERT_MSG_DELETE_UNSUCCESS').then((val: string) => {
      this.msg_alert_delete_event_unsuccess = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });

    
    //
    this.dataSource = new EventsDataSource(this.http);
    this.dataSource.loadEvents(1, this.sort, this.defaultLanguage);
    this.selectedLanguage = this.defaultLanguage;
    // get all languages
    this.subscriptionLang = this.http.get('dropdown/language', this.defaultLanguage).subscribe(httpResponse => {
      this.languages = httpResponse.json();
    })
    // get total and page size
    this.subscription = this.http.get('get/happening', this.selectedLanguage).subscribe(httpResponse => {
      if(httpResponse.status == 200) {
        this.total = httpResponse.json().total;
        this.pageSize = httpResponse.json().per_page;
      }
    });

  }
  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.LoadEventPage())
    ).subscribe();
  }
  LoadEventPage() {
    this.dataSource.loadEvents(
        this.paginator.pageIndex + 1, this.sort, this.selectedLanguage);
        // console.log('loadWineryPage triggered!: ', this.paginator.pageIndex);
  }
  sortData(event) {
    if(event.direction == 'asc') {
      this.sort = 'asc';
      this.loadDataSource();
    }
    if(event.direction == 'desc') {
      this.sort = 'desc';
      this.loadDataSource();
    }
  }
  onChangeLanguage(id: number, name: string) {
    // console.log(id, name);
    this.selectedLanguage = id;
    this.dataSource = new EventsDataSource(this.http);
    this.loadDataSource();
    this.subscription = this.http
      .get("get/happening", this.selectedLanguage)
      .subscribe(httpResponse => {
        // console.log(httpResponse.json());
        if(httpResponse.status === 200) {
            this.total = httpResponse.json().total;
            this.pageSize = httpResponse.json().per_page;
           }
        });
  }
  loadDataSource() {
    this.dataSource.loadEvents(this.paginator.pageIndex, this.sort, this.selectedLanguage);
    this.paginator.firstPage();
  }
  onAddEvent() {
    this.router.navigate(["events/add"]);
  }

  onEditEvent(id) {
    this.router.navigate(["events/edit", id]);
  }
  onViewEvent(id) {
    this.router.navigate(["events/view", id]);
  }
  onDeleteEvent(id, name) {
    swal({
      title: this.msg_alert_delete_event_title + ` ${name}`,
      text: this.msg_alert_delete_event_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(() => {
      this.http.delete("delete/happening/" + id).subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          swal({
            title: this.msg_alert_delete_event_success_title,
            text: `${name} ` + this.msg_alert_delete_event_success_text,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          this.http.get('get/happening', this.selectedLanguage).subscribe(data => {
            this.paginator.length = data.json().total;
          })
          
          this.dataSource = new EventsDataSource(this.http);
          this.loadDataSource();
        }
        error => {
          this.alert.showNotification(
            this.msg_alert_delete_event_unsuccess,
            "danger",
            "error"
          );
        };
      }, err => {
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      });
    }, (dismiss) => {
      
    });
  }
}
