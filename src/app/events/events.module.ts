import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { EventsTable } from './events.routing';

import { EventsComponent } from './events.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { MatTableModule, MatSelectModule, MatPaginatorModule, MatIconModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatTooltipModule, MatSortModule, MatInputModule, MatProgressSpinnerModule, MatRadioModule, MatButtonModule } from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { ViewComponent } from './view/view.component';
import { QuillModule } from 'ngx-quill';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EventsTable),
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule,
      
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatButtonModule,
    QuillModule,
    TranslateModule
  ],
  declarations: [
    EventsComponent,
    AddComponent,
    EditComponent,
    ViewComponent
  ],
  exports: [
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatButtonModule
  ]
})
export class EventsModule { }
