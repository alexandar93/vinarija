import { Routes } from '@angular/router';

import { EventsComponent } from './events.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ViewComponent } from './view/view.component';

export const EventsTable: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: EventsComponent
        }]
    },
    {
        path: '',
        children: [{
            path: 'edit/:id',
            component: EditComponent
        }]
    },
    {
        path: '',
        children: [{
            path: 'add',
            component: AddComponent
        }]
    },
    {
        path: '',
        children: [{
            path: 'view/:id',
            component: ViewComponent
        }]
    }
];

