import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { NotificationsService } from '../../notifications/notifications.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TranslateHelperService } from './../../services/translate-helper.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit, OnDestroy {
  //variables
  subscriptionparams: Subscription;
  subscriptionData: Subscription;

  coverImage: string = null;
  serverData: any;
  id: number;
  languages: any[] = [];
  lat: number;
  lng: number;

  /** translate string */
  msg_server_error: string;

  constructor(
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });

    this.subscriptionparams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    this.subscriptionData = this.http.get('patch/initialize/happening/' + this.id, 1).subscribe(httpResponse => {
      this.serverData = httpResponse.json();

      this.coverImage = this.serverData.image_path;
      this.lat = this.serverData.lat;
      this.lng = this.serverData.lng;

      this.serverData.languages.forEach(lang => {
        this.languages.push(lang);
      });
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })
  }
  editEvent() {
    this.router.navigate(['events/edit/' + this.id]);
  }

  ngOnDestroy() {
    this.subscriptionData.unsubscribe();
    this.subscriptionparams.unsubscribe();
  }

}
