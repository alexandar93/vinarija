import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from '../services/localstorage.service';
import { NotificationsService } from '../notifications/notifications.service';
import { Router, CanActivate } from '@angular/router';
import { TranslateHelperService } from './../services/translate-helper.service';

declare var $;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  test : Date = new Date(); // date year 
  loginForm: FormGroup;
  emailPattern: any = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
  /** translate strings */
  msg_welcome: string;
  msg_email_or_password_not_correct: string;

    checkFullPageBackgroundImage(){
        var $page = $('.full-page');
        var image_src = $page.data('image');

        if(image_src !== undefined){
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
            $page.append(image_container);
        }
    };

  constructor(
    private http: HttpService,
    private fb: FormBuilder,
    private alert: NotificationsService,
    private router: Router,
    private ls: LocalStorageService, // importing custom localstorage
    private translate: TranslateHelperService

  ) { }

  ngOnInit() {
    this.translate.getTranslate('LOGIN_ALERT_MSG_UNSUCCESS_LOGIN').then((val: string) => {
      this.msg_email_or_password_not_correct = val;
    });
    this.translate.getTranslate('LOGIN_ALERT_MSG_SUCCESS_LOGIN').then((val: string) => {
      this.msg_welcome = val;
    });
    // init form group for login form 
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      password: ['', Validators.required]
    })

    // setting background image 
    this.checkFullPageBackgroundImage();

    setTimeout(function(){
        // after 1000 ms we add the class animated to the login/register card
        $('.card').removeClass('card-hidden');
    }, 700)
  }
  // on submitting form func
  onSubmit() {
    // post method with saving token in localstorage as JSON
    // console.log(this.loginForm.value);
    this.http.post('login', this.loginForm.value).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
      // console.log(httpResponse.json());
      this.ls.set('token', httpResponse.json().token);
      this.ls.set('user', httpResponse.json().user);
      this.ls.set('user_id', httpResponse.json().user_id);
      this.ls.set('user_data', httpResponse.json().user_data);
      this.router.navigate(['']);
      this.alert.showNotification(this.msg_welcome + ', ' + httpResponse.json().user, 'success', '');
      // console.log(httpResponse);
      }
    }, error => {
      if(error.status == 401) {
          this.alert.showNotification(this.msg_email_or_password_not_correct,'danger', '');
      }
      // console.log(error);
    })
  }

}
