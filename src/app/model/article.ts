export interface Article {
    created_at: string,
    name: string,
    text: string,
    link: string,
}