export interface Event {
    id: number,
    name: string,
    start: string,
    end: string,
    location: string,
    link: string
}