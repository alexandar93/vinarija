// globals.ts
import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  editor_settings = {
    toolbar: [ 
        ['bold', 'italic', 'underline'], 
        [ 'blockquote'], 
        [{ size: ['small', false, 'large', 'huge'] }],    
        // [
        //   { color: [] },
        //   { background: [] }
        // ],
        [{ script: 'sub' }, { script: 'super' }],
        [{ header: [1, 2, 3, 4, 5, 6, false] }],
        [{ align: [] }],
        [{ list: 'ordered' }, { list: 'bullet' }],
        ['link'],
        ['clean']
      ]
  };
  language = {
    id: "1"
  };
}

