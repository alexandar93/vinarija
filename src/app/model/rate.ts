export interface Rate {
    id: number,
    created_at: string,
    comment: string,
    name: string,
    rate: number,
    type: string
}