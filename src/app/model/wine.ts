export interface Wine {
    id: number,
    name: string,
    address: string
}