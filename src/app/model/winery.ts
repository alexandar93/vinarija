export interface Winery {
    id: number,
    name: string,
    address: string
}