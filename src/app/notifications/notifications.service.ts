import { Injectable } from '@angular/core';

declare var $:any;

@Injectable()
export class NotificationsService {

  constructor() { }

    showNotification(msg, color, icona) {

        $.notify({
            icon: "add_alert",
            message: msg,

        }, {
            type: color,
            timer: 3000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
    }
}

