import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormArray, FormBuilder, FormGroup, Validators }   from '@angular/forms';
import { Response } from '@angular/http';
import { } from '@types/googlemaps';

import { PoiModel } from '../poi.model';
import { HelperService } from '../../services/helper.service';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var google: any;
declare var $: any;

@Component({
  selector: 'app-add-poi',
  templateUrl: './add-poi.component.html',
  styleUrls: ['./add-poi.component.css']
})
export class AddPoiComponent implements OnInit {

  map: google.maps.Map;
  data : PoiModel = new PoiModel();
  placeTypes : Array<Object> = [];

  // initial center position for the map
  lat: number = 44.016521;
  lng: number = 20.865859;

  geocoder = new google.maps.Geocoder();
  infoWindow = new google.maps.InfoWindow();
  addPoiForm: FormGroup;
  languagesArray = [];
  usedLanguages = [];
  /** formgroup items and his controls used for *for loop in html template */

  /** translate strings */
  msg_server_error: string;
  msg_marker_is_required: string;
  msg_success_created: string;
  msg_cannot_add_marker: string;

  constructor(private http: HelperService, private fb : FormBuilder, private translate: TranslateHelperService) {
    this.placeTypes = http.placeTypes;
  }

  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_MARKER_ERR').then((val: string) => {
      this.msg_marker_is_required = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_SUCCESS_CREATED').then((val: string) => {
      this.msg_success_created = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_CANNOT_ADD_MARKER').then((val: string) => {
      this.msg_cannot_add_marker = val;
    });

    this.http.getLanguages().subscribe(res=>{
      this.languagesArray = res.json();
    }, (err : Response) => {
      console.log(err);
      this.http.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
    })

    this.initMap();

    this.addPoiForm = this.createForm();
  }

  createForm(){
    return this.fb.group({
      languages : this.fb.array([
        this.initLanguage(1),
      ]),
      address : [''],
      type : [''],
      lat: [''],
      lng: ['']
    })
  }

  onSubmit(){
    console.log(this.addPoiForm.value)
    if(!this.data.lat || !this.data.lng) {
      return this.http.showNotification({
        status: 'error',
        message: this.msg_marker_is_required
      })
    }

    this.addPoiForm.controls['lat'].setValue(this.data.lat);
    this.addPoiForm.controls['lng'].setValue(this.data.lng);
    
    this.http.postPoi(this.addPoiForm.value).subscribe(res=>{
      this.http.showNotification({
        status: 'success',
        message: this.msg_success_created
      });

      this.addPoiForm = this.createForm();

      this.marker.setMap(null);

    }, err=>{
      this.http.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
      console.log(err)
    });
  }

  initLanguage(lang_id?){
    //console.log(lang_id)
    this.usedLanguages[lang_id] = lang_id;

    return this.fb.group({
        language_id : [lang_id, Validators.required],
        name : ["name"],
        value: ["", Validators.required]
    });

  }

  addLanguage(lang_id : number) {
    const control = <FormArray>this.addPoiForm.controls['languages'];
    control.push(this.initLanguage(lang_id))
  }

  removeLanguage(event, i, lang_id: number) {
    event.stopPropagation();

    let indexInUsedLanguages = this.usedLanguages.indexOf(lang_id);
    this.usedLanguages[indexInUsedLanguages] = null;

    const control = <FormArray>this.addPoiForm.controls['languages'];
    control.removeAt(i);
  }

  langAlreadyChoosen(lang_id){
    //console.log(lang_id)
    for(let i=0; i< this.langAlreadyChoosen.length; i++) {
      //console.log(lang_id + ' - ' + this.usedLanguages[i])
      return this.usedLanguages[i] == lang_id;
    }
  }

  test() {
    console.log(this.addPoiForm.value)
  }

  initMap() {
    this.map = new google.maps.Map(document.getElementById('map_demo'), {
      zoom: 7,
      center: {lat: 44.016521, lng: 20.865859},
      streetViewControl: false,
      scrollwheel: false
    });

    // Add a listener for the click event
    this.map.addListener('click', (event)=>{ return this.addLatLng(event) });
  }

  marker = null;
  oldMarker;
  oldData;
  // Handles click events on a map, and adds a new point map
  addLatLng(event) {
    this.geocoder.geocode({
    'latLng': event.latLng
    }, (results, status)=>{
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {

          if(this.marker) this.marker.setMap(null);

          this.marker = new google.maps.Marker({
            position: event.latLng, 
            map: this.map,
            draggable : true
          });

          //alert(results[0].formatted_address);
          this.data = {
            name: "",
            address: results[0].formatted_address,
            lat: event.latLng.lat(),
            lng: event.latLng.lng(),
            type : 0
          };

          this.markerListeners(this.marker, this.data);
          this.oldData = this.data;
          this.oldMarker = this.marker;
          
          this.addPoiForm.controls['address'].setValue(this.data.address);
        }
      } else {
        window.alert(this.msg_cannot_add_marker);
        this.marker = this.oldMarker;
      }
    });
  }

  markerListeners(marker, data) {

    // Marker drop handler
    google.maps.event.addListener(marker, "click", (event)=>{

      let pointType = data.type ? (' | <i> ' + data.type + ' </i>') : '';

      if(data.address.length) {
        this.infoWindow.setContent('<p><strong>' + data.name + '</strong>' + pointType + '</p><span>' + data.address + '</span>');
      } else {
        this.infoWindow.setContent('');
      }

      this.infoWindow.open(this.map, marker);
    });

    // Marker drop handler
    google.maps.event.addListener(marker, "dragend", (event)=>{

      let lat = event.latLng.lat(); 
      let lng = event.latLng.lng();

      this.data.lat = lat;
      this.data.lng = lng;

      this.geocoder.geocode({
        'latLng': event.latLng
        }, (results, status)=>{
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              //alert(results[0].formatted_address);
              this.data.address = results[0].formatted_address;
              this.addPoiForm.controls['address'].setValue(this.data.address);
            }
          } else {
            window.alert(this.msg_cannot_add_marker);
            this.oldData = this.data;
            this.marker = this.oldMarker;
          }
        });
    });
  }

  ngOnDestroy() { }

}
