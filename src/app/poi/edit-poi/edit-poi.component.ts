import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { FormsModule, ReactiveFormsModule, FormArray, FormBuilder, FormGroup, Validators }   from '@angular/forms';
import { Response } from '@angular/http';
import { } from '@types/googlemaps';

import { PoiModel } from '../poi.model';
import { HelperService } from '../../services/helper.service';
import { TranslateHelperService } from './../../services/translate-helper.service';


declare var google: any;
declare var $: any;

@Component({
  selector: 'app-edit-poi',
  templateUrl: './edit-poi.component.html',
  styleUrls: ['./edit-poi.component.css']
})
export class EditPoiComponent implements OnInit {

  data = new PoiModel;
  map: google.maps.Map;
  placeTypes : Array<Object> = [];

  // initial center position for the map
  lat: number = 44.016521;
  lng: number = 20.865859;

  geocoder = new google.maps.Geocoder();
  infoWindow = new google.maps.InfoWindow();
  editPoiForm: FormGroup;
  languagesArray = [];
  usedLanguages = [];
  poiId: string;
  /** formgroup array and controls */

  /** translate strings */
  msg_server_error: string;
  msg_marker_is_required: string;
  msg_success_edited: string;
  msg_cannot_add_marker: string;


  constructor(
    private activatedRoute: ActivatedRoute, 
    private http: HelperService, 
    private fb : FormBuilder,
    private translate: TranslateHelperService
    ) {
    
    this.poiId = this.activatedRoute.snapshot.paramMap.get('id');

    this.getPoi(parseInt(this.poiId));

    this.placeTypes = http.placeTypes;
  }

  ngOnInit() {

    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_MARKER_ERR').then((val: string) => {
      this.msg_marker_is_required = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
      this.msg_success_edited = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_CANNOT_ADD_MARKER').then((val: string) => {
      this.msg_cannot_add_marker = val;
    });

    this.http.getLanguages().subscribe(res=>{
      this.languagesArray = res.json();
    }, (err : Response) => {
      console.log(err);
      this.http.showNotification({
        status: 'error',
        message: 'Something went wrong.'
      })
    })

    this.initMap();

    this.editPoiForm = this.createForm();
  }

  getPoi(id : number){
    this.http.getPoi(id).subscribe(res=>{
      this.http.handleResponse(res);
      this.data = res;

      let latlng = new google.maps.LatLng(this.data.lat, this.data.lng);

      this.addLatLng({'latLng' : latlng}, true);

      this.existingLangs();
      
    }, err=>{
      console.log(err)
    })
  }

  showData(data, field) {
    const control = <FormArray>this.editPoiForm.controls[field];
    return control.push(data);
  }

  langList = [];
  existingLangs() {
    if(this.editPoiForm) {
      const control = <FormArray>this.editPoiForm.controls['languages'];
      control.controls = [];
    }

    //console.log(this.data.languages)
    if(this.data.languages) {
      for (let lang_obj of this.data.languages) {
        this.langList.push(lang_obj);
      };
      
      if (this.langList.length>0) {
        for(let lang of this.langList){

          this.usedLanguages[lang.language_id] = lang.language_id;

          this.showData(this.fb.group({
            language_id : [lang.language_id, Validators.required],
            name : ["name"],
            value: [lang.fields[0].value, Validators.required],
            id : [lang.fields[0].id, Validators.required]
          }), 'languages');
        }
      }
    } else {
      return false;
    }
  }

  createForm(){
    return this.fb.group({
      languages : this.fb.array([
        this.initLanguage(1),
      ]),
      address : [''],
      type : [''],
      lat: [''],
      lng: ['']
    })
  }

  onSubmit(){
    if(!this.data.lat || !this.data.lng) {
      return this.http.showNotification({
        status: 'error',
        message: this.msg_marker_is_required
      })
    }

    this.editPoiForm.controls['lat'].setValue(this.data.lat);
    this.editPoiForm.controls['lng'].setValue(this.data.lng);
    if(this.editPoiForm.controls['type'].value === "") {
      // console.log(this.data.type)
      this.editPoiForm.controls['type'].setValue(this.data.type);
    }
    
    this.http.updatePoi(this.editPoiForm.value, this.poiId).subscribe(res=>{
      this.http.showNotification({
        status: 'success',
        message: this.msg_success_edited
      });

    }, err=>{
      this.http.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
      console.log(err)
    });
  }

  initLanguage(lang_id?){
    //console.log(lang_id)
    this.usedLanguages[lang_id] = lang_id;

    return this.fb.group({
        language_id : [lang_id, Validators.required],
        name : ["name"],
        value: ["", Validators.required]
    });

  }

  addLanguage(lang_id : number) {
    const control = <FormArray>this.editPoiForm.controls['languages'];
    control.push(this.initLanguage(lang_id))
  }

  removeLanguage(event, i, lang_id: number) {
    event.stopPropagation();

    let indexInUsedLanguages = this.usedLanguages.indexOf(lang_id);
    this.usedLanguages[indexInUsedLanguages] = null;

    const control = <FormArray>this.editPoiForm.controls['languages'];
    control.removeAt(i);
  }

  langAlreadyChoosen(lang_id){
    //console.log(lang_id)
    for(let i=0; i< this.langAlreadyChoosen.length; i++) {
      //console.log(lang_id + ' - ' + this.usedLanguages[i])
      return this.usedLanguages[i] == lang_id;
    }
  }

  // test() {
  //   console.log(this.editPoiForm)
  // }

  initMap() {
    this.map = new google.maps.Map(document.getElementById('map_demo'), {
      zoom: 7,
      center: {lat: 44.016521, lng: 20.865859},
      streetViewControl: false,
      scrollwheel: false
    });

    // Add a listener for the click event
    this.map.addListener('click', (event)=>{ return this.addLatLng(event) });
  }

  marker = null;
  oldMarker;
  oldData;
  // Handles click events on a map, and adds a new point map
  addLatLng(event, existingPoi? : boolean) {
    console.log(event)
    this.geocoder.geocode({
    'latLng': event.latLng
    }, (results, status)=>{
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {

          if(this.marker) this.marker.setMap(null);

          this.marker = new google.maps.Marker({
            position: event.latLng, 
            map: this.map,
            draggable : true
          });

          //alert(results[0].formatted_address);
          this.data = {
            name: "",
            address: existingPoi ? this.data.address : results[0].formatted_address,
            lat: event.latLng.lat(),
            lng: event.latLng.lng(),
            type : this.data.type
          };

          this.markerListeners(this.marker, this.data);
          this.oldData = this.data;
          this.oldMarker = this.marker;
          
          this.editPoiForm.controls['address'].setValue(this.data.address);
        }
      } else {
        window.alert(this.msg_cannot_add_marker);
        this.marker = this.oldMarker;
      }
    });
  }

  markerListeners(marker, data) {

    // Marker drop handler
    google.maps.event.addListener(marker, "click", (event)=>{

      //let pointType = data.type ? (' | <i> ' + data.type + ' </i>') : '';

      if(data.address.length) {
        this.infoWindow.setContent('<span>' + data.address + '</span>');
      } else {
        this.infoWindow.setContent('');
      }

      this.infoWindow.open(this.map, marker);
    });

    // Marker drop handler
    google.maps.event.addListener(marker, "dragend", (event)=>{

      let lat = event.latLng.lat(); 
      let lng = event.latLng.lng();

      this.data.lat = lat;
      this.data.lng = lng;

      this.geocoder.geocode({
        'latLng': event.latLng
        }, (results, status)=>{
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              //alert(results[0].formatted_address);
              this.data.address = results[0].formatted_address;
              this.editPoiForm.controls['address'].setValue(this.data.address);
            }
          } else {
            window.alert(this.msg_cannot_add_marker);
            this.oldData = this.data;
            this.marker = this.oldMarker;
          }
        });
    });
  }

  ngOnDestroy() { }

}
