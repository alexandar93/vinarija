import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SelectionModel, DataSource } from "@angular/cdk/collections";

import { MatPaginator, MatTableDataSource } from "@angular/material";
import { PoiDataSource } from '../services/poi.datasource';
import { HttpService } from '../services/http.service';
import { HelperService } from '../services/helper.service';
import { Subscription } from 'rxjs/Subscription';
import { PoiModel } from './poi.model';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { TranslateHelperService } from './../services/translate-helper.service';

declare var swal:any;

@Component({
  selector: 'app-poi',
  templateUrl: './poi.component.html',
  styleUrls: ['./poi.component.css']
})
export class PoiComponent implements OnInit {
  subscription: Subscription;
  subscriptionLang: Subscription;
  defaultLanguage: number = 1;
  selectedLanguage: number;
  languages: any[] = [];
  total;
  pageSize;
  // sort: string = 'asc';

  dataSource: PoiDataSource;
  displayedColumns = ['id', 'name', 'address', 'type', 'actions'];
  placeTypes = this.helperService.placeTypes;
  model: string = '';
  modelChanged: Subject<string> = new Subject<string>();

  /** translate strings */
  msg_server_error: string;
  msg_delete_poi_title: string;
  msg_delete_poi_text: string;
  msg_delete_poi_success_title: string;
  msg_delete_poi_success_text: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor( 
    private router: Router, 
    private http: HttpService, 
    private helperService : HelperService,
    private translate: TranslateHelperService
    ) {
      this.modelChanged
        .debounceTime(400)
        .distinctUntilChanged()
        .subscribe(model => {
            this.model = model
            this.searchPoi(1, model, this.selectedLanguage)
        });
  }

  ngOnInit() {
    // translate lable for pagination
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_delete_poi_title = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_delete_poi_text = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_poi_success_title = val;
    });
    this.translate.getTranslate('POI_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_poi_success_text = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });

    // 
    this.dataSource = new PoiDataSource(this.helperService);
    this.dataSource.loadPois(1, '', this.defaultLanguage);
    this.selectedLanguage = this.defaultLanguage;

    // get all languages
    this.subscriptionLang = this.http.get('dropdown/language', this.defaultLanguage).subscribe(httpResponse => {
      this.languages = httpResponse.json();
    }, (err) => {
      console.log(err);
      this.helperService.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
    })

    // get total and page size
    this.subscription = this.http.get('poi/get/paginate', this.selectedLanguage).subscribe(httpResponse => {
      if(httpResponse.status == 200) {
        this.total = httpResponse.json().total;
        this.pageSize = httpResponse.json().per_page;
      }
    });

  }
  /** za sortiranje treba da se promeni datasource http request da se posalje header : Sorting/ Sort */
  // sortData(event) {
  //   if(event.direction == 'asc') {
  //     this.sort = 'asc';
  //     this.loadDataSource();
  //   }
  //   if(event.direction == 'desc') {
  //     this.sort = 'desc';
  //     this.loadDataSource();
  //   }
  // }

  loadDataSource() {
    this.dataSource.loadPois( this.paginator.pageIndex + 1, this.model, this.selectedLanguage);
    this.paginator.firstPage();
  }

  changed(text: string) {
    this.modelChanged.next(text);
  }

  searchPoi(currentPage, searchTerm, lang ) {
    this.loadDataSource();
    
    this.subscription = this.helperService.searchPointsOfInterest(currentPage, searchTerm, lang)
      .subscribe(res=>{
        this.total = res.total;
        this.pageSize = res.per_page;
      }, err=>{
        console.log(err)
      })
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.LoadPoiPage())
    ).subscribe();
  }

  LoadPoiPage() {
    this.dataSource.loadPois( this.paginator.pageIndex + 1, this.model, this.selectedLanguage);
  }

  onChangeLanguage(id: number, name: string) {
    this.selectedLanguage = id;
    //this.dataSource = new PoiDataSource(this.helperService);
    this.dataSource.loadPois(1, this.model, this.selectedLanguage);
    this.paginator.firstPage();
    this.subscription = this.helperService.searchPointsOfInterest(1, this.model, this.selectedLanguage)
      .subscribe(res=>{
        this.total = res.total;
        this.pageSize = res.per_page;
      }, err=>{
        console.log(err)
      })
  }

  onEditPoi(id){
    this.router.navigate(["poi/edit-poi/", id]);
  }

  onDeletePoi(id, name){
    swal({
        title: this.msg_delete_poi_title + ` ${name} ?`,
        text: this.msg_delete_poi_text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: this.msg_swal_button_yes,
        cancelButtonText: this.msg_swal_button_no,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false

    }).then(()=>{

        this.helperService.deletePoi(id).subscribe(res=>{
            //console.log(id)
            //let response = res.json();
            this.helperService.handleResponse(res);
            this.searchPoi(1, this.model, this.selectedLanguage)
            swal({
                title: this.msg_delete_poi_success_title,
                text: this.msg_delete_poi_success_text,
                type: 'success',
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
            })
    
        }, err=>{
            console.log(err);
        })
        
    }, (dismiss)=>{
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        // if (dismiss === 'cancel') {
        // swal({
        //     title: 'Cancelled',
        //     text: 'Your imaginary file is safe :)',
        //     type: 'error',
        //     confirmButtonClass: "btn btn-info",
        //     buttonsStyling: false
        // })
        // }
    })
    
  }

}
