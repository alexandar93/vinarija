export class PoiModel {
    id?: number;
    name?: string;
    address: string;
    lat : number;
    lng : number;
    type : number;
    languages? : Array<any>;
}