import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { DragulaModule } from 'ng2-dragula';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http'

//import { HelperService } from '../services/helper.service';

import { PoiComponent } from './poi.component';
import { PoiRoutes } from './poi.routing';
import { AddPoiComponent } from './add-poi/add-poi.component';
import { EditPoiComponent } from './edit-poi/edit-poi.component';

import { MatTableModule, MatSelectModule, MatPaginatorModule, MatIconModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatTooltipModule, MatSortModule, MatInputModule, MatProgressSpinnerModule, MatRadioModule } from '@angular/material';
//import { DirectionsMapDirective } from './agm-directions.directive'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PoiRoutes),
        FormsModule,
        ReactiveFormsModule,
        DragulaModule,
        HttpClientModule,
        HttpModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        TranslateModule
    ],
    declarations: [
        PoiComponent,
        AddPoiComponent,
        EditPoiComponent,
        //FilterPipe
        //DirectionsMapDirective
    ],
    exports :[
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatIconModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatSortModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatRadioModule
    ],
    providers : [
        //GoogleMapsAPIWrapper
        //HelperService
    ]
})

export class PoiModule {}
