import { Routes } from '@angular/router';

import { PoiComponent } from './poi.component';
import { EditPoiComponent } from './edit-poi/edit-poi.component';
import { AddPoiComponent } from './add-poi/add-poi.component';

export const PoiRoutes: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: PoiComponent
        },
        {
            path: 'edit-poi/:id',
            component: EditPoiComponent
        },
        {
            path: 'add-poi',
            component: AddPoiComponent,
            pathMatch: 'full'
        }]
    }
];