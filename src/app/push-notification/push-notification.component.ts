import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../services/http.service';
import { NotificationsService } from '../notifications/notifications.service';
import { textSpanIntersectsWithPosition } from 'typescript';
import { TranslateHelperService } from './../services/translate-helper.service';

@Component({
  selector: 'app-push-notification',
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.css']
})

export class PushNotificationComponent implements OnInit {
  pushForm: FormGroup;

  /** translate strings */
  msg_server_error: string;
  msg_success_send: string;

  @ViewChild("f") myNgForm: any;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private translate: TranslateHelperService
  ) { }

  ngOnInit() {
    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('PUSH_ALERT_MSG_SUCCES_CREATED').then((val: string) => {
      this.msg_success_send = val;
    });

    this.pushForm = this.fb.group({
      title: ['', Validators.required],
      body: ['', Validators.required],
      android: [''],
      ios: [''],
      device: [[], Validators.required],
      critical: [false]
    })
    
  }
  /** reset form fields */
  resetForm() {
    this.myNgForm.resetForm();
  }
  /** on submit form */
  onSubmit() {
    let route: string;
    if(this.pushForm.controls.critical.value) {
      route = 'push/critical';
    }
    if(!this.pushForm.controls.critical.value) {
      route = 'push/send';
    } 
    this.pushForm.controls.android.setValue(0);
    this.pushForm.controls.ios.setValue(0);
    this.pushForm.controls.device.value.forEach(list => {
      if(list === 'android') {
        this.pushForm.controls.android.setValue(1);
      }
      if(list === 'ios') {
        this.pushForm.controls.ios.setValue(1);
      }
    });
    this.http.post(route, this.pushForm.value).subscribe(httpResponse => {
      if(httpResponse.status === 204) {
        this.resetForm();
        this.alert.showNotification(this.msg_success_send, 'success', '');
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })
  }

}
