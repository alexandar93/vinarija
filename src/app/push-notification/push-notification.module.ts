import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PushNotificationComponent } from './push-notification.component';
import { PushRoute } from './push-notification.routing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule, MatCheckboxModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PushRoute),
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatOptionModule,
    MatSelectModule,
    TranslateModule
  ],
  declarations: [
    PushNotificationComponent
  ],
  exports: [
    MatInputModule,
    MatCheckboxModule,
    MatOptionModule,
    MatSelectModule
  ]
})
export class PushNotificationModule { }
