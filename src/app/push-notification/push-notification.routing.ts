import { Routes } from '@angular/router';
import { PushNotificationComponent } from './push-notification.component';



export const PushRoute: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: PushNotificationComponent
        }]
    }
];

