export interface Comment {
    full_name: string,
    social_type: string,
    comment: string,
    cover_image: string,
    created_at: string,
    rate: number,
    status: string
}