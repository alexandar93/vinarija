import { Component, OnInit, ViewChild } from "@angular/core";
import { RateDataSource } from "../services/rate.datasource";
import { MatPaginator, MatTableDataSource } from "@angular/material";
import { SelectionModel, DataSource } from "@angular/cdk/collections";
import { HttpService } from "../services/http.service";
import { NotificationsService } from "../notifications/notifications.service";
import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  tap,
  delay
} from "rxjs/operators";
import { Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { Comment } from "./comment.model";
import { TranslateHelperService } from './../services/translate-helper.service';
import { HelperService } from './../services/helper.service';

declare var swal: any;
declare var $: any;

@Component({
  selector: "app-rate",
  templateUrl: "./rate.component.html",
  styleUrls: ["./rate.component.css"]
})
export class RateComponent implements OnInit {
  //variables
  subscription: Subscription;
  subscriptionWines: Subscription;
  subscriptionParams: Subscription;
  sections: any[] = ["vina", "vinarije"];
  total: any;
  pageSize: any;
  selectedType: string; // selected type vino or vinarija
  sectionId: number;
  selection;
  results;
  selectedResult: number = null;
  commentRow: Comment;
  commentId: number;
  selectedFilter: string;
  selectStatus: any;
  filterStatus: any;
  /** route for wines */
  http_route_wines: string;
  /** route for wineries */
  http_route_wineries: string;

  dataSource: RateDataSource;
  displayedColumns = [
    "id",
    "created_at",
    "name",
    "comment",
    "rate",
    // "type",
    "status",
    "actions"
  ];

  /** translate strings */
  msg_server_error: string;
  msg_delete_rate_title: string;
  msg_delete_rate_text: string;
  msg_delete_rate_success_title: string;
  msg_delete_rate_success_text: string;
  msg_delete_unsuccess: string;
  msg_deapproved_title: string;
  msg_deapproved: string;
  msg_approved_title: string;
  msg_approved: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateHelperService,
    private helper: HelperService
  ) {
    if(helper.isWineryAdmin()) {
      this.http_route_wineries = 'my/wineries/dropdown';
      this.http_route_wines = 'my/wines/dropdown';
    } else {
      this.http_route_wineries = 'dropdown/winery';
      this.http_route_wines = 'dropdown/wine';
    }
  }

  ngOnInit() {

    // translate lable for pagination
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_DELETE_RATE_TITLE').then((val: string) => {
      this.msg_delete_rate_title = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_DELETE_RATE_TEXT').then((val: string) => {
      this.msg_delete_rate_text = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_DELETE_RATE_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_rate_success_title = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_DELETE_RATE_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_rate_success_text = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_DELETE_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_DEAPPROVED').then((val: string) => {
      this.msg_deapproved = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_APPROVED').then((val: string) => {
      this.msg_approved = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_DEAPPROVED_TITLE').then((val: string) => {
      this.msg_deapproved_title = val;
    });
    this.translate.getTranslate('RATE_ALERT_MSG_APPROVED_TITLE').then((val: string) => {
      this.msg_approved_title = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });



    //
    this.dataSource = new RateDataSource(this.http);
    //
    this.subscriptionParams = this.route.params.subscribe(param => {
      if (param.id !== undefined && param.type !== undefined) {
        if (param.type === "wine") {
          this.selection = "vina";
          this.http.get(this.http_route_wines, 1).subscribe(httpResponse => {
            this.results = httpResponse.json();
            this.sectionId = param.id;
            this.selectedResult = Number(param.id);
            this.selectedFilter = 'all';
            this.dataSourceInit();
          });
        }
        if (param.type === "winery") {
          this.selection = "vinarije";
          this.http.get(this.http_route_wineries, 1).subscribe(httpResponse => {
            this.results = httpResponse.json();
            this.sectionId = param.id;
            this.selectedResult = Number(param.id);
            this.selectedFilter = 'all';
            this.dataSourceInit();
          });
        }
        this.selectedType = param.type;
      } else {
        this.selectedType = "wine";
        this.selection = "vina";

        this.subscriptionWines = this.http
          .get(this.http_route_wines, 1)
          .subscribe(httpResponse => {
            if (httpResponse.status == 200) {
              this.results = httpResponse.json();
            }
          });
        }
    });
  }
  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.loadRatePage())).subscribe();
  }
  loadRatePage() {
    this.dataSource.loadRates(
      this.selectStatus,
      this.paginator.pageIndex + 1,
      this.selectedType,
      this.sectionId,
      this.filterStatus
    );
  }

  onChangeSelection(value) {
    this.selectedResult = null;
    this.selectedFilter = '';

    let sectionWine: string = "wine";
    let sectionWinery: string = "winery";
    if (value === "vina") {
      this.selection = "vina";
      this.selectedType = sectionWine;
      this.http.get(this.http_route_wines, 1).subscribe(httpResponse => {
        if (httpResponse.status == 200) {
          this.results = httpResponse.json();
        }
      });
    }
    if (value === "vinarije") {
      this.selection = "vinarije";
      this.selectedType = sectionWinery;
      this.http.get(this.http_route_wineries, 1).subscribe(httpResponse => {
        this.results = httpResponse.json();
      });
    }
  }

  onChangeResults(value) {
    this.selectedFilter = "all";
    this.sectionId = value.id;
    this.dataSourceInit();
  }

  onDeleteRate(id, name) {
    swal({
      title: this.msg_delete_rate_title,
      text: this.msg_delete_rate_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        this.http.delete("delete/rate/" + id).subscribe(httpResponse => {
          if (httpResponse.status === 204) {
            swal({
              title: this.msg_delete_rate_success_title,
              text: this.msg_delete_rate_success_text,
              type: "success",
              confirmButtonClass: "btn btn-success",
              buttonsStyling: false
            });
            this.dataSourceInit();
          }
          error => {
            this.alert.showNotification(
              this.msg_delete_unsuccess,
              "danger",
              "error"
            );
          };
        });
      },
      dismiss => {}
    );
  }

  dataSourceInit() {
    this.selectStatus = 'all';
    this.dataSource = new RateDataSource(this.http);
    this.dataSource.loadRates(
      this.selectStatus,
      this.paginator.pageIndex + 1,
      this.selectedType,
      this.sectionId
    );
    this.paginator.firstPage();
    this.subscription = this.http
      .get(this.selectedType + "/comments/" + this.sectionId + "/admin", 1)
      .subscribe(httpResponse => {
        if (httpResponse.status === 200) {
          this.total = httpResponse.json().total;
          this.pageSize = httpResponse.json().per_page;
        }
      }, err => {
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      });
  }
  getColor(type) {
    if (type !== null) {
      if (type.toLowerCase() === "facebook") {
        return "#3B5998";
      }
      if (type.toLowerCase() === "instagram") {
        return "#262626";
      }
      if (type.toLowerCase() === "google") {
        return "#DB4437";
      }
    }
  }

  getStars(rating) {
    // Get the value
    var val = parseFloat(rating);
    // Turn value into number/100
    var size = (val / 5) * 100;
    return size + "%";
  }
  disapprovedComment() {
    this.http
      .get("rate/deapprove/" + this.commentId, 1)
      .subscribe(HttpResponse => {
        if (HttpResponse.status === 204) {
          this.dataSourceInit();
          swal({
            title: this.msg_deapproved_title,
            text: this.msg_deapproved,
            type: "error",
            confirmButtonClass: "btn btn-info",
            buttonsStyling: false
          });
          $(".modal").modal("toggle");
        }
      });
  }
  approvedComment() {
    this.http
      .get("rate/approve/" + this.commentId, 1)
      .subscribe(HttpResponse => {
        if (HttpResponse.status === 204) {
          this.dataSourceInit();
          swal({
            title: this.msg_approved_title,
            text: this.msg_approved,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          $(".modal").modal("toggle");
        }
      });
  }
  openSelectedRate(event, row) {
    // disable to click on button for delete and open modal
    if (event.target.nodeName == "I") return false;
    if (event.target.nodeName == "BUTTON") return false;
    // open modal
    this.commentId = row.id;
    $(".modal").modal({
      show: "true"
    });
    // set value for intefrace commentRow
    this.commentRow = {
      full_name: row.user.full_name,
      social_type: row.user.social_type,
      comment: row.comment,
      cover_image: row.cover_image,
      created_at: row.created_at,
      rate: row.rate,
      status: row.status
    };
  }
  FilterByStatus(filter) {
    this.filterStatus = filter;
    this.selectStatus = 'status';
    this.http
    .get(this.selectedType + "/" + this.sectionId + "/rate/" + this.filterStatus, 1)
    .subscribe(httpResponse => {
      if (httpResponse.status === 200) {
        this.total = httpResponse.json().total;
        this.pageSize = httpResponse.json().per_page;
      }
    });
    this.dataSource.loadRates(
      this.selectStatus,
      this.paginator.pageIndex + 1,
      this.selectedType,
      this.sectionId,
      this.filterStatus
    );
    this.paginator.firstPage();
   
  }
}
