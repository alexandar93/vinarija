import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatTableModule, MatSortModule, MatPaginatorModule, MatIconModule, MatTooltipModule, MatProgressSpinnerModule, MatInputModule, MatSelectModule } from '@angular/material';
import { RateTable } from './rate.routing';
import { RouterModule } from '@angular/router';
import { RateComponent } from './rate.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RateTable),
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    TranslateModule
  ],
  declarations: [
    RateComponent
  ],
  exports: [
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressSpinnerModule
  ]
})
export class RateModule { }
