import { Routes } from '@angular/router';
import { RateComponent } from './rate.component';



export const RateTable: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: RateComponent
        },
        {
            path: ':type/:id',
            component: RateComponent
        }]
    }
];

