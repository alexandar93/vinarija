import { Injectable } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { TranslateLoader } from '@ngx-translate/core';
import { Constants } from "./endpoint.service"
import { Observable } from 'rxjs/Observable'; 
import {LocalStorageService} from './../services/localstorage.service';


@Injectable()
export class CustomTranslateLoader implements TranslateLoader  {
    
    constructor(private http: Http, private ls: LocalStorageService) {
        // console.log(globals.language_id.valueOf);
        // this.languageId = globals.language;
    }
    getTranslation(lang: string, id?: any): Observable<any>{
        let languageId = localStorage.getItem('language_id');
        if(languageId === null){
            languageId = '1';
        }
        let contentHeader = new Headers({"Content-Type": "application/json","Access-Control-Allow-Origin":"*","Accept-Language": languageId});
        // var apiAddress = 'http://81.4.110.135/vino/api/public/index.php/web/language';
        // var apiAddress = 'http://localhost:4200/assets/i18n/rs.json';
        var apiAddress = 'http://admin.vinovojo.com/api/public/web/language';
        
        return Observable.create(observer => {
          this.http.get(apiAddress, { headers: contentHeader }).subscribe((res: Response) => {
                    observer.next(res.json());
                    observer.complete();               
                },
            error => {
                //  failed to retrieve from api, switch to local
                console.log('err');
                this.http.get("/assets/i18n/rs.json").subscribe((res: Response) => {
                    observer.next(res);
                    observer.complete();               
                })
            }
            );
        }); 
    }
}