import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {Article} from "../model/article";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class ArticleDataSource implements DataSource<Article> {

    private articleSubject = new BehaviorSubject<Article[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadArticle(pageIndex, sort, lang) {

        this.loadingSubject.next(true);

        this.httpService.findArticle('get/article', sort, lang, pageIndex).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(articles => this.articleSubject.next(articles));

    }

    connect(collectionViewer: CollectionViewer): Observable<Article[]> {
        return this.articleSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.articleSubject.complete();
        this.loadingSubject.complete();
    }

}

