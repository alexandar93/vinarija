import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {Type} from "../model/type";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class TypeDataSource implements DataSource<Type> {

    private typeSubject = new BehaviorSubject<Type[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadTypes(pageIndex, sort, lang) {

        this.loadingSubject.next(true);
        // TODO putanja za type 
        this.httpService.findTypes('get/wineClass', sort, lang, pageIndex).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(types => this.typeSubject.next(types));

    }

    connect(collectionViewer: CollectionViewer): Observable<Type[]> {
        return this.typeSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.typeSubject.complete();
        this.loadingSubject.complete();
    }

}

