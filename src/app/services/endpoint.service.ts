import { Injectable } from '@angular/core';

@Injectable()
export class Constants {
    // public endpoint: string = 'http://oneview.misa.local/backend/public/api/';
    // public endpoint: string = 'http://81.4.110.135/vino/api/public/index.php/';
    public endpoint: string = 'http://admin.vinovojo.com/api/public/';
    // public endpoint: string = 'http://172.16.40.42:8080/';

    url(route) {
        return this.endpoint + route;
    }
}