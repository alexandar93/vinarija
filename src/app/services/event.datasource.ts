import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {Event} from "../model/event";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class EventsDataSource implements DataSource<Event> {

    private eventsSubject = new BehaviorSubject<Event[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadEvents(pageIndex, sort, lang) {

        this.loadingSubject.next(true);

        this.httpService.findEvents('get/happening', sort, lang, pageIndex).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(events => this.eventsSubject.next(events));

    }

    connect(collectionViewer: CollectionViewer): Observable<Event[]> {
        return this.eventsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.eventsSubject.complete();
        this.loadingSubject.complete();
    }

}

