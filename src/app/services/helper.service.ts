import { Injectable } from '@angular/core';
import {  HttpErrorResponse } from '@angular/common/http';
import { Response } from '@angular/http';
import { Constants } from './endpoint.service';
import { HttpService } from './http.service';

import {Observable} from "rxjs";
import 'rxjs/Rx';

declare var $:any;

@Injectable()
export class HelperService {

  constructor(private contants: Constants, private httpService: HttpService) { }

  placeTypes = [{
    id : 3,
    name : 'Vinarija'
  }, {
    id : 20,
    name : 'Cafe'
  },{
    id : 21,
    name : 'Restoran'
  }];

  searchPointsOfInterest(page : number = 1, string : string, lang) {
      const url = 'search/pointOfInterest?page=' + page;
      let data : FormData = new FormData();
      data.append( 'search', string );
      return this.httpService.postFormData( url, data, true, lang)
        .map((res: Response) => res.json())
        .catch((err:Response) => {
            let details = err.json();
            return Observable.throw(details || 'Server error');
        });
  }

  getWinePaths(page : number = 1, searchquery){

    //console.log(searchquery)
    const url = 'get/winePath?page=' + page + '&searched=' + searchquery;
    return this.httpService.get( url, 1 )
      .map((res: Response) => res.json())
      .catch((err:Response) => {
          let details = err.json();
          return Observable.throw(details || 'Server error');
      });
  }
  getWineInCategories(page : number = 1, wineryId, categoryId, searchquery){
    //console.log(searchquery)
    const url = 'wine/winery/' + wineryId + '/' + categoryId + '?page=' + page + '&searched=' + searchquery;
    return this.httpService.get( url, 1 )
      .map((res: Response) => res.json())
      .catch((err:Response) => {
          let details = err.json();
          return Observable.throw(details || 'Server error');
      });
  }

  searchWinePaths(page : number = 1, string : string, lang ){
    const url = 'search/winePath?page=' + page;
    let data : FormData = new FormData();
    data.append( 'search', string );
    return this.httpService.postFormData( url, data, true , lang )
      .map((res: Response) => res.json())
      .catch((err:Response) => {
          let details = err.json();
          return Observable.throw(details || 'Server error');
      });
  }

  getWinePath(id) {
    const url = 'patch/initialize/winePath/' + id;

    return this.httpService.get( url, 1 )
      .map((res: Response) => res.json())
      .catch((err:Response) => {
          let details = err.json();
          return Observable.throw(details || 'Server error');
      });
  }

  getPointsOfInterests() {
    const url ='get/pointOfInterest';
    return this.httpService.get( url, 1 )
      .map((res: Response) => res.json())
      .catch((err:Response) => {
          let details = err.json();
          return Observable.throw(details) || 'Server error';
      });
  }

  deleteWinePath(id: number){
    const url = 'delete/winePath/' + id;

    return this.httpService.delete( url )
      .map((res) => res)
      .catch((err: Response) => {
        console.log(err)
          let details = err;
          return Observable.throw(details || 'Server error');
      });
  }

  deletePoi(id: number){
    const url = 'delete/pointOfInterest/' + id;

    return this.httpService.delete( url )
      .map((res) => res)
      .catch((err: Response) => {
        console.log(err)
          let details = err;
          return Observable.throw(details || 'Server error');
      });
  }

  getLanguages(){
    const url = 'dropdown/language';

    return this.httpService.get( url, 1 );
  }

  postWinepath(data){
    const url = 'create/winePath';
    return this.httpService.postFormData( url, data );
  }

  updateWinepath(data, id){
    const url = 'patch/winePath/' + id;
    return this.httpService.postFormData( url, data );
  }

  postPoi(data) {
    const url = 'create/pointOfInterest';
    return this.httpService.post( url, data );
  }

  updatePoi(data, id){
    const url = 'patch/pointOfInterest/' + id;
    return this.httpService.post( url, data );
  }

  getPoi(id) {
    const url = 'patch/initialize/pointOfInterest/' + id;

    return this.httpService.get( url, 1 )
      .map((res: Response) => res.json())
      .catch((err:Response) => {
          let details = err.json();
          return Observable.throw(details || 'Server error');
      });
  }

   public handleResponse(data: any): any {
      //console.log(data)
      // console.log('handleResponse: ');

      if(!data) return;

      if (!data.errors) {
        //console.log('errors is false!');

        if (data.messages) {
          // console.log('messages found!');
          data.messages.forEach((value, index) => {
            // console.log('Message index: ' + index);
            // console.log('Message value: ' + value);
            this.showNotification({status : 'success', message: value});
          });
        }

        if (data.message) {
          // console.log('message found!');
          this.showNotification({status : 'success', message: data.message});
        }

      } else if (data.errors) {
         //console.log('errors is true!');

        if (data.messages) {
          // console.log('messages found!');
          data.messages.forEach((value, index) => {
            // console.log('Message index: ' + index);
            // console.log('Message value: ' + value);
            this.showNotification({status : 'error', message: value});
          });
        }

        if (data.message) {
          // console.log('message found!');
          this.showNotification({status : 'error', message: data.message});
        }

      } else {
        console.log('Failed to check for errors.');
      }

    }

  showNotification({status, message}){
    let color;

    if(status === 'success') {
      color = 'success';
    } else if (status === 'error'){
      color = 'danger';
    }

    $.notify({
        icon: "notifications",
        message: message

      },{
          type: color,
          timer: 3000,
          placement: {
              from: 'top',
              align: 'right'
          }
      });
      
	}
  /** Check if winery admin is logged */
  isWineryAdmin(): boolean {
    const userData = JSON.parse(localStorage.getItem('user_data'));

    if(userData.type === 'winery_admin') {
      return true;
    } else {
      return false;
    }
  }

}
