import {Injectable} from '@angular/core';
import {OnInit} from '@angular/core';
import {Constants} from './endpoint.service';
import {Observable} from 'rxjs/Observable';

import {Http, Response, RequestOptions, Headers, URLSearchParams} from '@angular/http';
import {LocalStorageService} from './localstorage.service';
import {HttpClient, HttpParams, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Winery} from '../model/Winery';
import {Wine} from '../model/Wine';
import {Event} from '../model/event';
import {User} from '../model/user';
import {Article} from '../model/article';
import {Rate} from '../model/rate';
import {Type} from '../model/type';
import {Region} from '../model/region';
import {Winesort} from '../model/winesort';
import {map} from "rxjs/operators";
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HttpService implements OnInit {

    constructor(private endpoint: Constants,
                private http: Http,
                private ls: LocalStorageService,
                private httpc: HttpClient) {
    }

    ngOnInit() {
    }

    public findMobileLanguage(route): Observable<any> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            // TODO
        }).pipe(
            map(res => res)
        );
    }

    public findWinesort(route, sort: string, lang, pageNumber): Observable<Winesort[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString())
                .set('Sort', sort).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    public findRegions(route, lang, pageNumber): Observable<Region[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString()).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    public findReon(route, lang, pageNumber): Observable<any> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString()).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res)
        );
    }

    public findVinogorje(route, lang, pageNumber): Observable<any> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString()).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res)
        );
    }

    public findTypes(route, sort: string, lang, pageNumber): Observable<Type[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString())
                .set('Sorting', sort).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    public findRates(route, lang, pageNumber): Observable<Rate[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString()).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    public findArticle(route, sort, lang, pageNumber): Observable<Article[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString())
                .set('Sorting', sort).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    public findEvents(route, sort, lang, pageNumber): Observable<Event[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString())
                .set('Sorting', sort.toString()).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    public findUser(route, sort, pageNumber): Observable<User[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Sorting', sort.toString()).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    public findWineries(route, sort, lang, pageNumber): Observable<Winery[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString())
                .set('Sorting', sort.toString()).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    public findWines(route, sort, lang, pageNumber = 1): Observable<Wine[]> {
        let url = this.endpoint.url(route);
        return this.httpc.get(url, {
            params: new HttpParams()
                .set('page', pageNumber.toString()),
            headers: new HttpHeaders().set('Accept-Language', lang.toString())
                .set('Sorting', sort.toString()).set('Authorization', 'Bearer' + this.ls.get('token'))
        }).pipe(
            map(res => res['data'])
        );
    }

    // public getPromise(route: string, lang, token=true): Promise<any> {
    //     let url = this.endpoint.url(route);
    //     let headers = new HttpHeaders();
    //     if (token) {
    //        headers = headers.set('Authorization', 'Bearer ' + this.ls.get('token'));
    //     }
    //     headers = headers.set('Accept-Language' , lang).set('Accept', 'application/json');
    //     console.log(headers)
    //     return this.httpc
    //         .get(url, {observe: 'response', headers: headers})
    //         .toPromise()
    //         .then(this.extractData)
    //         .catch(this.handleError);
    // }
    // // function for data getPromise
    // private extractData(res: HttpResponse<any>) {
    //     console.log('test', res);
    //     let body = res;
    //     return body || {};
    // }
    // // handling errors for getPromise
    // private handleError(error: any): Promise<any> {
    //     console.error('An error occurred', error);
    //     return Promise.reject(error.message || error);
    // }
    public uploadGallery(url, files: FileList, fileName, lang, token = true) {
        url = this.endpoint.url(url);

        let form = new FormData();
        fileName += '[]';
        for (let i = 0; i < files.length; i++)
            form.append(fileName, files[i]);

        let headers = new Headers();
        if (lang) {
            headers.append('Accept-Language', lang.toString());
        }

        if (token) {
            headers.append('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        headers.append('Accept', 'application/json');

        let options = new RequestOptions({headers: headers});
        // url += "?token=" + this.ls.get('token');
        return this.http.post(url, form, options); // salje prazan payload
    }

    public postFormData(route, data, token = true, lang?: number) {

        let url = this.endpoint.url(route);
        let headers = new Headers();
        if (token) {
            headers.append('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        if (lang) {
            headers.append('Accept-Language', lang.toString());
        }
        let options = new RequestOptions({headers: headers});
        return this.http.post(url, data, options);
    }


    public get(route, lang, token = true) {
        let url = this.endpoint.url(route);
        let headers = new Headers();
        if (token) {
            headers.append('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        headers.append('Accept-Language', lang);
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({headers: headers});
        let o = this.http.get(url, options);
        // o.connect();
        return o;
    }

    public postLanguage(route, data, token = true) {
        let url = this.endpoint.url(route);
        let headers = new Headers();
        if (token) {
            headers.append('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        // headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        // data = JSON.stringify(data);
        let options = new RequestOptions({headers: headers});
        return this.http.post(url, data, options);
    }

    public post(route, data, token = true) {
        let url = this.endpoint.url(route);
        let headers = new Headers();
        if (token) {
            headers.append('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        // headers.append('Accept', 'application/json');
        // headers.append('Content-Type', 'application/json');
        // data = JSON.stringify(data);
        let options = new RequestOptions({headers: headers});
        return this.http.post(url, data, options);
    }

    public patch(route, data, token = true) {
        let url = this.endpoint.url(route);
        let headers = new Headers();
        if (token) {
            headers.append('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        data = JSON.stringify(data);
        let options = new RequestOptions({headers: headers});
        return this.http.patch(url, data, options);
    }

    public patchFile(route, data, token = true) {
        let url = this.endpoint.url(route);
        let headers = new Headers();
        if (token) {
            headers.append('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        let options = new RequestOptions({headers: headers});
        return this.http.patch(url, data, options);
    }

    public download(url, token = true) {

    }

    public delete(route, token = true) {
        let url = this.endpoint.url(route);
        let headers = new HttpHeaders();
        if (token) {
            headers = headers.set('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        return this.httpc.delete(url, {observe: 'response', headers: headers});
    }

    public upload(url, file, fileName, postData = null, token = true) {
        url = this.endpoint.url(url);
        let form = new FormData();
        form.append(fileName, file);
        console.log(typeof postData);
        if (typeof postData == 'object')
            for (var key in postData)
                form.append(key, postData[key]);
        let headers = new Headers();
        if (token) {
            headers.append('Authorization', 'Bearer ' + this.ls.get('token'));
        }
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({headers: headers});
        return this.http.post(url, form, options); // salje prazan payload
    }
}
