import { Injectable } from '@angular/core';

@Injectable()
export class ImageVideoValidatorsService {

  constructor() { }
  // validation for image
  validateImage(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'png') {
      return true;
    } 
    if (ext.toLowerCase() == 'jpg') {
      return true;
    } 
    if (ext.toLowerCase() == 'jpeg') {
      return true;
    } 
      return false;
  }
  // validation for video mp4 format
  validateVideo(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'mp4') {
        return true;
    }  
    return false;
  }
}
