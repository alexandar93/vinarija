import { Injectable } from '@angular/core';
import { OnInit } from '@angular/core';

@Injectable()
export class LocalStorageService implements OnInit
{


    constructor()
    {
    }



    ngOnInit() {
    }

    public has(name) {
        return this.get(name) != null;
    }

    public get(name) {
        let string = localStorage.getItem(name);
        if (string == null)
            return null;

        return JSON.parse(string);
    }

    public remove(name) {
        localStorage.removeItem(name);
    }

    public set(name, value) {
        value = JSON.stringify(value);
        localStorage.setItem(name, value);
    }

}
