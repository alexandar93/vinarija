import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {PoiModel} from "../poi/poi.model";
import {HelperService} from "./helper.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class PoiDataSource implements DataSource<any> {

    private poiSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private HelperService: HelperService) {

    }

    loadPois(pageIndex:number, searchText: string, lang: number) {

        this.loadingSubject.next(true);

        this.HelperService.searchPointsOfInterest(pageIndex, searchText, lang).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(poi => this.poiSubject.next(poi.data));

    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.poiSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.poiSubject.complete();
        this.loadingSubject.complete();
    }

}

