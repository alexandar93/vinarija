import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {Rate} from "../model/rate";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";

export class RateDataSource implements DataSource<Rate> {

    private rateSubject = new BehaviorSubject<Rate[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadRates(route: string, pageIndex, sectionType, sectionId, filter?) {
        let custom_route: string = '';
        this.loadingSubject.next(true);
        if(route === 'status') {
            custom_route = sectionType + '/' + sectionId +'/rate/'+  filter;
        }
        if(route == 'all') {
            custom_route = sectionType + '/comments/'+ sectionId + '/admin';
        }
            this.httpService.findRates(custom_route, 1, pageIndex).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(rates => this.rateSubject.next(rates));
    }

    connect(collectionViewer: CollectionViewer): Observable<Rate[]> {
        return this.rateSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.rateSubject.complete();
        this.loadingSubject.complete();
    }

}

