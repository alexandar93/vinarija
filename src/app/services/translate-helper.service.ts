import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class TranslateHelperService {

  constructor(private translate: TranslateService) {}

  getTranslate(key: string):Promise<string> {
  	return new Promise((resolve, reject) => {
  		this.translate.get(key).subscribe((res: string) => {
  		resolve(res);
  	}, err => {
  		console.log(err);
  	});
  	});
  	
  }
}
