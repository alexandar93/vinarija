import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import { User } from "../model/user";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class UserDataSource implements DataSource<User> {

    private userSubject = new BehaviorSubject<User[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadUser(status, sort, pageIndex, type?) {
        let custom_route: string = '';
        this.loadingSubject.next(true);
        if(status == 'status') {
            custom_route = 'user/' + type;
        }
        if(status == 'all') {
            custom_route = 'user/users';
        }
        this.httpService.findUser(custom_route, sort, pageIndex).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        )
        .subscribe(users => this.userSubject.next(users));
    }

    connect(collectionViewer: CollectionViewer): Observable<User[]> {
        return this.userSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.userSubject.complete();
        this.loadingSubject.complete();
    }

}

