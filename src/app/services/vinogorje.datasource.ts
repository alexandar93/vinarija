import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {Region} from "../model/region";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class VinogorjeDataSource implements DataSource<Region> {

    private regionSubject = new BehaviorSubject<Region[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadVinogorje(pageIndex, areaId, lang, reset?: boolean) {

        this.loadingSubject.next(true);
        // TODO putanja za type 
        // if(reset) {
        //     this.loadingSubject.next(false);
        //     this.regionSubject.next(null);
        // }
        if(!reset) {
            this.httpService.findVinogorje('area/children/' + areaId, lang, pageIndex).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe((regions: any) =>{
                this.regionSubject.next(regions.children.data)
           });
        }
        

    }

    connect(collectionViewer: CollectionViewer): Observable<Region[]> {
        return this.regionSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.regionSubject.complete();
        this.loadingSubject.complete();
    }

}

