import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {Wine} from "../model/wine";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class WineDataSource implements DataSource<Wine> {

    private wineSubject = new BehaviorSubject<Wine[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadWines(pageIndex, sort, lang, route) {

        this.loadingSubject.next(true);

        this.httpService.findWines(route, sort, lang, pageIndex).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(wines => this.wineSubject.next(wines));

    }

    connect(collectionViewer: CollectionViewer): Observable<Wine[]> {
        return this.wineSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.wineSubject.complete();
        this.loadingSubject.complete();
    }

}

