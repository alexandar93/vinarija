import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {Winery} from "../model/winery";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class WineryDataSource implements DataSource<Winery> {

    private winerySubject = new BehaviorSubject<Winery[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadWineries(pageIndex, sort, lang, route) {

        this.loadingSubject.next(true);

        this.httpService.findWineries(route, sort, lang, pageIndex).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(wineries => this.winerySubject.next(wineries));

    }

    connect(collectionViewer: CollectionViewer): Observable<Winery[]> {
        return this.winerySubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.winerySubject.complete();
        this.loadingSubject.complete();
    }

}

