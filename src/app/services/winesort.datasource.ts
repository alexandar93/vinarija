import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {Winesort} from "../model/winesort";
import {HttpService} from "./http.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";



export class WinesortDataSource implements DataSource<Winesort> {

    private regionSubject = new BehaviorSubject<Winesort[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private httpService: HttpService) {

    }

    loadWinesort(pageIndex, sort: string, lang) {

        this.loadingSubject.next(true);
        // TODO putanja za type 
        this.httpService.findWinesort('get/category/paginate', sort, lang, pageIndex).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe(winesorts => this.regionSubject.next(winesorts));

    }

    connect(collectionViewer: CollectionViewer): Observable<Winesort[]> {
        return this.regionSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.regionSubject.complete();
        this.loadingSubject.complete();
    }

}

