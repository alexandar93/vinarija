import { Component, OnInit, Renderer, ViewChild, ElementRef, Directive } from '@angular/core';
import { ROUTES } from '../.././sidebar/sidebar-routes.config';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { LocalStorageService } from '../../services/localstorage.service';
import { TranslateService} from '@ngx-translate/core';
import { HttpService } from './../../services/http.service';
import { Globals } from './../../model/globals';

var misc:any ={
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0,
}
declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'navbar-cmp',
    templateUrl: 'navbar.component.html',
    styleUrls: ["./navbar.component.css"]
})

export class NavbarComponent implements OnInit{
    private listTitles: any[];
    location: Location;
    private nativeElement: Node;
    private toggleButton;
    private sidebarVisible: boolean;
    listOfLanguages: any;
    flag_name:string;

    @ViewChild("navbar-cmp") button;

    constructor(location:Location, 
        private renderer : Renderer,
         private element : ElementRef,
          private router: Router,
           private ls: LocalStorageService,
           private http: HttpService,
           private translate: TranslateService,
           private globals: Globals
           ) {

        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('rs');

         // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use('rs');

        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    ngOnInit(){
        if(localStorage.getItem('language_code') === null) {
            this.ls.set('language_code', 'RS');
            this.ls.set('language_id', 1);
            this.flag_name = 'RS';
        } else {
            this.flag_name = localStorage.getItem('language_code').slice(1, -1);
        }

        
        this.http.get('dropdown/language', 1).subscribe(res => {
            this.listOfLanguages = res.json();
        });   

        this.listTitles = ROUTES.filter(listTitle => listTitle);

        var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        if($('body').hasClass('sidebar-mini')){
            misc.sidebar_mini_active = true;
        }
        $('#minimizeSidebar').click(function(){
            var $btn = $(this);

            if(misc.sidebar_mini_active == true){
                $('body').removeClass('sidebar-mini');
                misc.sidebar_mini_active = false;

            }else{
                setTimeout(function(){
                    $('body').addClass('sidebar-mini');

                    misc.sidebar_mini_active = true;
                },300);
            }

            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function(){
                window.dispatchEvent(new Event('resize'));
            },180);

            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function(){
                clearInterval(simulateWindowResize);
            },1000);
        });
    }
    onChangeLanguage(id: any, code: string = 'rs', name: string) {
         // the lang to use, if the lang isn't available, it will use the current loader to get them
        let language_code = code.toLowerCase();
        this.ls.set('language_code', code.toUpperCase());
        this.ls.set('language_id', id);
        this.translate.use(language_code);
        location.reload();
    }
    home() {
        this.router.navigate(['/dashboard']);
    }
    onLogout() {
        this.ls.remove('token');
        this.ls.remove('user');
        this.router.navigate(['login']);
    }
    onEditLoggedUser() {
        let id = this.ls.get('user_id');
        this.router.navigate(['users/edit/'+ id]);
    }
    isMobileMenu(){
        if($(window).width() < 991){
            return false;
        }
        return true;
    }
    sidebarToggle(){
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];

        if(this.sidebarVisible == false){
            setTimeout(function(){
                toggleButton.classList.add('toggled');
            },500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }

    getTitle(){
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if(titlee.charAt(0) === '#'){
            titlee = titlee.slice( 2 );
        }
        for(var item = 0; item < this.listTitles.length; item++){
            if(this.listTitles[item].path === titlee){
                return this.listTitles[item].title;
            }
        }
        return '';
    }
    getPath(){
        // console.log(this.location);
        return this.location.prepareExternalUrl(this.location.path());
    }
}
