import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Kontrolna tabla', icon: 'material-icons' },
    { path: '/users', title: 'Korisnici', icon: 'material-icons' },
    { path: '/winery', title: 'Vinarije', icon: 'material-icons' },
    { path: '/wines', title: 'Vina', icon: 'material-icons' },
    { path: '/events', title: 'Dešavanja', icon: 'material-icons' },
    { path: '/wine-paths', title: 'Vinski putevi', icon: 'material-icons' },
    { path: '/poi', title: 'Tačke od interesa', icon: 'material-icons' },
    { path: '/article', title: 'Vesti', icon: 'material-icons' },
    { path: '/rate', title: 'Komentari', icon: 'material-icons' },
    { path: '/push-notification', title: 'Push notifications', icon: 'material-icons' },
    { path: '/create-region', title: 'Regije', icon: 'material-icons' },
    { path: '/create-winesort', title: 'Vrsta vina', icon: 'material-icons' },
    { path: '/create-type', title: 'Sorta vina', icon: 'material-icons' },
    { path: '/create-language', title: 'Jezici', icon: 'material-icons' },
    // { path: '/classification', title: 'Klasifikacija vina', icon: 'material-icons' }
];
