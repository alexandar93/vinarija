import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  RequiredValidator,
  AbstractControl,
  FormControl
} from "@angular/forms";
import { HttpService } from "../../services/http.service";
import { NotificationsService } from "../../notifications/notifications.service";
import { HttpResponse } from '@angular/common/http';
import { CustomValidators } from 'ng4-validators';
import { Router } from '@angular/router';
import { TranslateHelperService } from './../../services/translate-helper.service';
import { HelperService } from './../../services/helper.service';

declare var $;
declare var swal;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  hide = true;
  userForm: FormGroup;
  errorPassword: any;
  errorPasswordLength: string;
  errorEmail: any;
  errorMessage = false;
  emailPattern: any = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
  patternforpass: any = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
  /** list of wineries */
  listOfWineries: any;
  /** check is winery admin logged */
  isWineryAdmin: boolean = this.helper.isWineryAdmin();
  /** is admin logged */
  isAdminLogged: boolean = true;

  msg_loading_swal: string;
  msg_submit_succes: string;
  msg_password_length_error: string;
  msg_email_already_in_use_error: string;
  msg_password_pattern_error: string;
  msg_alert_email_exist: string;

  listOfTypes: any[] = [{
    name: 'admin'
  }, {
    name: 'winery_admin'
  }]

  @ViewChild('f') myNgForm;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private router: Router,
    private translate: TranslateHelperService,
    private helper: HelperService
  ) {}

  ngOnInit() {
    this.translate.getTranslate('USERS_ALERT_MSG_SUCCESS_CREATE').then((val: string) => {
      this.msg_submit_succes = val;
    });
    this.translate.getTranslate('ADDONS_USERS_FIELD_PASSWORD_LENGTH').then((val: string) => {
      this.msg_password_length_error = val;
    });
    this.translate.getTranslate('ADDONS_USERS_FIELD_EMAIL_INVALID').then((val: string) => {
      this.msg_email_already_in_use_error = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_ERROR_PASSWORD_LENGTH').then((val: string) => {
      this.msg_password_pattern_error = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_ERROR_EMAIL').then((val: string) => {
      this.msg_alert_email_exist = val;
    }); 
    
    let password = new FormControl('', [Validators.required, Validators.pattern(this.patternforpass)]);
    let confirmPassword = new FormControl('',[Validators.required, CustomValidators.equalTo(password)]);
    // init userForm
    this.userForm = this.fb.group({
      full_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(this.emailPattern), Validators.email]],
      type: ['', Validators.required],
      wineries: ['', Validators.required],
      password: password,
      confirmPassword: confirmPassword
    });

    if(this.isWineryAdmin) {
      this.userForm.controls.wineries.disable();
    }
    this.getListOfWineries();
  }

  onSelectAdminType(typeId, typeName) {
    if(typeName === 'admin') {
      this.isAdminLogged = true;
      this.userForm.controls.wineries.disable();
    } else {
      this.userForm.controls.wineries.enable();
      this.isAdminLogged = false;
    }
  }
  getListOfWineries() {
    this.http.get('dropdown/winery', 1).subscribe(res => {
      if(res.status === 200) {
        this.listOfWineries = res.json();
      }
    });
  }

  goBack() {
    this.router.navigate(['/users']);
  }

  resetForm() {
    this.myNgForm.resetForm();
  }

  onSubmit() {
    const fd: FormData = new FormData();
    let formInput = this.userForm.value;
    delete formInput.confirmPassword;
    
    fd.append("json", JSON.stringify(formInput));
    swal({title: 'Molimo sačekajte', allowOutsideClick: false});
    swal.showLoading();
    this.http.postFormData('create/user', fd).subscribe(httpResponse => {
      if(httpResponse.status === 201) {
        swal.close();
        this.alert.showNotification(this.msg_submit_succes, 'success', '');
        // this.userForm.reset();
        this.resetForm();

        // this.userForm.markAsPristine();
        // this.userForm.markAsUntouched();
        // this.userForm.updateValueAndValidity();
      }
    },
    error => {
      swal.close();
      if(error.status == 422){
        if(error.json().password != null) {
          // set password as invalid and trigger error
          this.userForm.controls.password.setErrors({'incorrect': true});
          this.errorPasswordLength = this.msg_password_length_error;
        } else {
          this.errorPasswordLength = '';
        }
        if(error.json().email != null) {
            // set password as invalid and trigger error
            this.userForm.controls.email.setErrors({'incorrect': true});
            this.errorEmail = this.msg_email_already_in_use_error;
          }
        else {
          this.errorEmail = '';
        }
        if(this.errorPasswordLength != ''){
          this.alert.showNotification(this.msg_password_pattern_error, 'danger', 'notifications');
        }
        if(this.errorEmail != '') {
          this.alert.showNotification( this.userForm.controls.email.value + ', ' + this.msg_alert_email_exist, 'info', 'notifications');
        }
      }
    })
  }
}
