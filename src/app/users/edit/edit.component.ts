import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpService } from '../../services/http.service';
import { Subscription } from 'rxjs/Subscription';
import { CustomValidators } from 'ng4-validators';
import { NotificationsService } from '../../notifications/notifications.service';
import { ValidationFormsComponent } from '../../forms/validationforms/validationforms.component';
import { TranslateHelperService } from './../../services/translate-helper.service';
import { HelperService } from './../../services/helper.service';


declare var swal;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, OnDestroy {
  /**variables */
  hide = true;
  hide2 = true;
  userForm: FormGroup;
  subscriptionParams: Subscription;
  id: number;
  errorMessage;
  errorOldPassword = '';
  errorPasswordLength = '';
  /** email error handling messages */
  errorEmail = '';
  /** Email */
  currentEmail = '';
  /** types: admin, user, trusted */
  type: any;
  /** email pattern */
  emailPattern: any = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
  /** password pattern */
  patternforpass: any = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
  /** list of admin types hard - coded*/
  listOfTypes: any[] = [{
    name: 'admin'
  }, {
    name: 'winery_admin'
  }]
  /** list of wineries */
  listOfWineries: any;
  /** check is winery admin logged */
  isWineryAdmin: boolean = this.helper.isWineryAdmin();

  msg_success_patch: string;
  msg_server_error: string;
  msg_password_length_error: string;
  msg_old_password_not_correct: string;
  msg_email_already_exist: string;
  msg_alert_password_pattern: string;
  msg_alert_wrong_password_entered: string;
  msg_alert_email_pattern_error: string;
  


  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateHelperService,
    private helper: HelperService,
    private loc: Location
  ) { 
    if(this.helper.isWineryAdmin()) {
      const userData = JSON.parse(localStorage.getItem('user_data'));
      loc.subscribe((val) => {
         if(val.url !== '/users/edit/' + userData.id) {
           return loc.replaceState('/users/edit/' + userData.id);
         }
       });
    }
  }

  ngOnInit() {

    this.translate.getTranslate('USERS_ALERT_MSG_SUCCESS_PATCH').then((val: string) => {
      this.msg_success_patch = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_SERVER_ERRROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('ADDONS_USERS_FIELD_PASSWORD_LENGTH').then((val: string) => {
      this.msg_password_length_error = val;
    });
    this.translate.getTranslate('ADDONS_USERS_FIELD_OLD_PASSWORD_NOT_CORRECT').then((val: string) => {
      this.msg_old_password_not_correct = val;
    });
    this.translate.getTranslate('ADDONS_USERS_FIELD_EMAIL_INVALID').then((val: string) => {
      this.msg_email_already_exist = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_ERROR_PASSWORD_LENGTH').then((val: string) => {
      this.msg_alert_password_pattern = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_WRONG_PASSWORD').then((val: string) => {
      this.msg_alert_wrong_password_entered = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_ERROR_EMAIL').then((val: string) => {
      this.msg_alert_email_pattern_error = val;
    });
    // prevent winery admin to route to another id's
     // get url params for user ID
     this.subscriptionParams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    let password = new FormControl('', Validators.pattern(this.patternforpass)); //  creating new controler for password input field for matching passwords 
    let confirmPassword = new FormControl('', CustomValidators.equalTo(password)); // creating new controler for confirmPass.

    // init FromGroup  userForm
    this.userForm = this.fb.group({
      full_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      type: ['', Validators.required],
      wineries: ['', Validators.required],
      old_password: [''],
      password: password,
      confirmPassword: confirmPassword
    });
    if(this.isWineryAdmin) {
      this.userForm.controls.wineries.disable();
    } else {
      this.userForm.controls.wineries.enable();
    }
    this.loadingData(); // calling func for loading data in form
  }
  onSelectAdminType(typeId, typeName) {
    if(typeName === 'admin') {
      this.userForm.controls.wineries.disable();
    } else {
      this.userForm.controls.wineries.enable();
    }
  }

  goBack() {
    this.router.navigate(['/users']);
  }
  // init for loading and fills fields with values from server
  loadingData() {
    this.http.get('dropdown/winery', 1).subscribe(res => {
      if(res.status === 200) {
        this.listOfWineries = res.json();
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });

    this.http.get('patch/initialize/user/' + this.id, 1).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        let wineryData: any[] = [];
        let serverData = httpResponse.json();
        this.userForm.controls.full_name.setValue(serverData.full_name);
        this.userForm.controls.email.setValue(serverData.email);
        this.currentEmail = serverData.email;
        this.userForm.controls.type.setValue(serverData.type);

        serverData.winery.forEach(winery => {
          wineryData.push(winery.id);
        });
        this.userForm.controls.wineries.setValue(wineryData);

        if(this.userForm.controls.type.value === 'admin') {
          this.userForm.controls.wineries.disable();
        }
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })  
  }
  // destroying all subscripions
  ngOnDestroy() {
    this.subscriptionParams.unsubscribe();
  }
  onSubmit() {
    const fd: FormData = new FormData(); // init new FormData 
    let formInput = this.userForm.value;
    delete formInput.confirmPassword; // deleting FormControl 

    // checking for password if input is empty dont sent password
    if(formInput.old_password == "") {
      delete formInput.old_password;
      delete formInput.password;
    }
    if(this.currentEmail === formInput.email) {
      delete formInput.email;
    }
    // append values from input fields to FormData
    fd.append('json', JSON.stringify(formInput));
    // show loading spinner
    swal({title: 'Molimo sačekajte', allowOutsideClick: false});
    swal.showLoading();
    // patching values from edit form
    this.http.postFormData('patch/user/' + this.id, fd).subscribe(httpResponse => {
      if(httpResponse.status === 204) {
        swal.close();
        // handling success response
        this.alert.showNotification(this.msg_success_patch, 'success', 'notifications');
      } else {
        this.alert.showNotification(this.msg_server_error, 'danger', 'notifications');
      }
    },
    error => {
      swal.close();
      if(error.status === 422){
        this.errorMessage = true;
        // handling common error for wrong inputs
        if(error.json().password != null) {
          // set password as invalid and trigger error
          this.userForm.controls.password.setErrors({'incorrect': true});
          this.errorPasswordLength = this.msg_password_length_error;
        } else {
          this.errorPasswordLength = '';
        }
        if(error.json().old_password != null) {
          // set password as invalid and trigger error
          this.userForm.controls.old_password.setErrors({'incorrect': true});
          this.errorOldPassword = this.msg_old_password_not_correct;
        } else {
          this.errorOldPassword = '';
        }
        if(error.json().email != null) {
          if(this.currentEmail != this.userForm.controls.email.value) {
            // set password as invalid and trigger error
            this.userForm.controls.email.setErrors({'incorrect': true});
            this.errorEmail = this.msg_email_already_exist;
          } else {
            this.userForm.controls.email.setErrors({'incorrrect': false});
          }
        } else {
          this.errorEmail = '';
        }
        if(this.errorPasswordLength != ''){
          this.alert.showNotification(this.msg_alert_password_pattern, 'danger', 'notifications');
        }
        if(this.errorOldPassword != '') {
          this.alert.showNotification(this.msg_alert_wrong_password_entered, 'danger', 'notifications');
        }
        if(this.errorEmail != '') {
          this.alert.showNotification(this.userForm.controls.email.value + this.msg_alert_email_pattern_error, 'info', 'notifications');
        }
      }
    })
  }
}
