import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { SelectionModel, DataSource } from "@angular/cdk/collections";
import { MatPaginator, MatTableDataSource } from "@angular/material";
import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  tap,
  delay
} from "rxjs/operators";

import { UserDataSource } from "../services/user.datasource";
import { HttpService } from "../services/http.service";
import { NotificationsService } from "../notifications/notifications.service";
import { TranslateHelperService } from './../services/translate-helper.service';

declare var swal: any;
declare var $: any;

@Component({
  selector: "app-users",
  templateUrl: "users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  /** variables */
  /** total number of pages */
  total;
  /** number element per page */
  pageSize;
  /** status for filtering */
  status: string;
  /** selected type */
  selectedType: string;
  /** user type selected */
  userTypeSelect: string = 'admin';
  /** types of users*/
  typesList: any[] = [{
    name: 'admin',
  }, {
    name: 'user',
  }, {
    name: 'trusted'
  }, {
    name: 'winery_admin'
  }]
  /** sort */
  sort: string = 'asc';
  dataSource: UserDataSource;
  displayedColumns = ["id", "name", "options", "actions"];
  msg_type_success: string;
  msg_server_error: string;
  msg_swal_title: string;
  msg_swal_text: string;
  swal_button_yes: string;
  swal_button_no: string;
  msg_swal_delete_success_text: string;
  msg_swal_delete_success_title: string;
  msg_swal_delete_unsuccess: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private router: Router,
    private http: HttpService,
    private alert: NotificationsService,
    private translate: TranslateHelperService,
  ) { }

  ngOnInit() {
    this.status = 'all';
    // translate for pagination
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_CHANGE_TYPE_SUCCESS').then((val: string) => {
      this.msg_type_success = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_swal_title = val;
    });
     this.translate.getTranslate('USERS_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_swal_text = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.swal_button_no = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_swal_delete_success_title = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_swal_delete_success_text = val;
    });
    this.translate.getTranslate('USERS_ALERT_MSG_DELETE_UNSUCCESS').then((val: string) => {
      this.msg_swal_delete_unsuccess = val;
    });

    // init table data
    this.dataSource = new UserDataSource(this.http);
    this.dataSource.loadUser(this.status, this.sort, 1);

    this.http.get('user/users', 1).subscribe(httpResponse => {
      if (httpResponse.status === 200) {
        this.total = httpResponse.json().total;
        this.pageSize = httpResponse.json().per_page;
      }
    }, error => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    })
  }
  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.loadUserPage())).subscribe();
  }
  loadUserPage() {
    this.dataSource.loadUser(this.status, this.sort, this.paginator.pageIndex + 1);
    // console.log("loadUserPage triggered!: ", this.paginator.pageIndex);
  }
  onSelectUserType(element, type: string, i: number) {
    // console.log(element, type, 'index: ', i);
    const fd = new FormData();

    let data = {
      type: type
    }
    fd.append('json', JSON.stringify(data));
    this.http.postFormData('patch/user/' + element.id, fd).subscribe(httpResponse => {
      if (httpResponse.status === 204) {
        this.alert.showNotification(this.msg_type_success, 'success', '');
        this.loadDataSource();
      }
    })
  }

  sortData(event) {
    if (event.direction == 'asc') {
      this.sort = event.direction;
      this.loadDataSource();
    }
    if (event.direction == 'desc') {
      this.sort = event.direction;
      this.loadDataSource();
    }
  }
  loadDataSource() {
    this.dataSource.loadUser(this.status, this.sort, this.paginator.pageIndex + 1, this.selectedType);
    this.paginator.firstPage();
  }

  OnEditUser(id) {
    this.router.navigate(["users/edit/", id]);
  }

  OnAddUser() {
    this.router.navigate(["users/add"]);
  }
  loadAllUsers() {
    this.status = 'all'
    this.http.get('user/users', 1).subscribe(httpResponse => {
      if (httpResponse.status === 200) {
        this.total = httpResponse.json().total;
        this.pageSize = httpResponse.json().per_page;
      }
    }, error => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });
    this.loadDataSource();
  }

  FilterByType(type) {
    this.selectedType = type;
    this.status = 'status'
    this.http.get('user/' + this.selectedType, 1).subscribe(httpResponse => {
      if (httpResponse.status === 200) {
        this.total = httpResponse.json().total;
        this.pageSize = httpResponse.json().per_page;
      }
    });
    this.dataSource.loadUser(
      this.status,
      this.sort,
      this.paginator.pageIndex + 1,
      this.selectedType
    );
    this.paginator.firstPage();
  }
  // onDeleteLang(id, name) {
  //   console.log(id, name);
  // }

  OnDeleteUser(id, name) {
    // console.log(name);
    swal({
      title: this.msg_swal_title + ` ${name} ?`,
      text: this.msg_swal_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.swal_button_yes,
      buttonsStyling: false
    }).then(() => {
      this.http.delete("delete/user/" + id).subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          swal({
            title: this.msg_swal_delete_success_title,
            text: name + ' ' + this.msg_swal_delete_success_text,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          this.http.get("get/user", 1).subscribe(data => {
            this.paginator.length = data.json().total;
          });
          this.dataSource = new UserDataSource(this.http);
          this.dataSource.loadUser(this.status, this.sort, this.paginator.pageIndex);
        }
        error => {
          this.alert.showNotification(
            this.msg_swal_delete_unsuccess,
            "danger",
            "error"
          );
        };
      });
    }, (dismiss) => {

    });
  }
}
