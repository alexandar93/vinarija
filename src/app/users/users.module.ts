import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';


import { UsersTable } from './users.routing';

import { UsersComponent } from './users.component';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';
import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTableModule, MatSortModule, MatPaginatorModule, MatProgressSpinnerModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { CustomFormsModule } from 'ng4-validators';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UsersTable),
        FormsModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatTooltipModule,
        CustomFormsModule,
        TranslateModule
    ],
    declarations: [
        UsersComponent,
        EditComponent,
        AddComponent

    ], exports: [
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatTooltipModule,
        MatProgressSpinnerModule
    ]
})

export class UsersModule {}
