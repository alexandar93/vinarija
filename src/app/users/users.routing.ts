import { Routes } from '@angular/router';

import { UsersComponent } from './users.component';
import { EditComponent } from './edit/edit.component';
import {AddComponent} from './add/add.component';
import { AuthService as AuthGuard } from './../auth/auth.service';
import { RoleGuardService as RoleGuard } from './../auth/role-guard.service';

export const UsersTable: Routes = [
    {
        path: '',
        children: [ {
            path: '',
            component: UsersComponent,
        }],
        canActivate: [RoleGuard],
        data: {
            expectedRole: 'admin'
        }
    }, {
        path: '',
        children: [ {
            path: 'edit/:id',
            component: EditComponent,
            pathMatch: 'full'
        }]
    }, {
        path: '',
        children: [ {
            path: 'add',
            component: AddComponent,
            pathMatch: 'full'
        }],
        canActivate: [RoleGuard],
        data: {
            expectedRole: 'admin'
        }
    }
];