import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddWinePathComponent } from './add-wine-path.component';

describe('AddWinePathComponent', () => {
  let component: AddWinePathComponent;
  let fixture: ComponentFixture<AddWinePathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddWinePathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWinePathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
