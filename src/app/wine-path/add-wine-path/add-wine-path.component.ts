import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormArray, FormBuilder, FormGroup, Validators }   from '@angular/forms';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { Response } from '@angular/http';
import { } from '@types/googlemaps';

import { WinePathModel } from '../wine-path-model';
import { HelperService } from '../../services/helper.service';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var google: any;
declare var $: any;

@Component({
  selector: 'app-add-wine-path',
  templateUrl: './add-wine-path.component.html',
  styleUrls: ['./add-wine-path.component.css']
})
export class AddWinePathComponent implements OnInit {

  @ViewChild('pathImg') pathImg: any;

  map: google.maps.Map;
  pointsOfInterest;
  data : WinePathModel;
  placeTypes : Array<Object> = [];

  // initial center position for the map
  lat: number = 44.016521;
  lng: number = 20.865859;

  poly = new google.maps.Polyline({
    strokeColor: '#920000',
    strokeOpacity: 1.0,
    strokeWeight: 3
  });
  path = new google.maps.MVCArray();
  service = new google.maps.DirectionsService();
  geocoder = new google.maps.Geocoder();
  infoWindow = new google.maps.InfoWindow();
  lat_lng = new Array();
  markersArray = new Array();
  pointsArray = new Array();
  oldPointsArray = new Array();
  addWinePathForm: FormGroup;
  languagesArray = [];
  usedLanguages = [];
  searchText: any;

  /** translate strings */
  msg_server_error: string;
  msg_wine_path_minimum_markers: string;
  msg_success_created: string;
  msg_file_is_not_image: string;
  msg_cannot_add_marker: string;
  msg_max_allowed_waypoints_reached: string;


  constructor(private dragulaService: DragulaService, private http: HelperService, private fb : FormBuilder, private translate: TranslateHelperService) {
    
    this.placeTypes = http.placeTypes;

    dragulaService.setOptions('points-bag', {
      moves: function (el, container, handle) {
        return handle.className === 'material-icons drag_indicator';
      }
    });
    dragulaService.dragend.subscribe((value) => { 
      this.drawRoute(this.pointsArray);
    });
  }

  ngOnInit() {
    //todo add/ edit
    /** translate */
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_MIN_MARKERS_ERR').then((val: string) => {
      this.msg_wine_path_minimum_markers = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_SUCCESS_CREATED').then((val: string) => {
      this.msg_success_created = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_WINDOWS_ALERT_MAX_NO_POINTS').then((val: string) => {
      this.msg_max_allowed_waypoints_reached = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_is_not_image = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_CANNOT_ADD_MARKER').then((val: string) => {
      this.msg_cannot_add_marker = val;
    });


    this.http.getPointsOfInterests().subscribe(res=>{
      this.pointsOfInterest = res;
    }, err=> {
      console.log(err);
      this.http.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
    });


    this.http.getLanguages().subscribe(res=>{
      this.languagesArray = res.json();
    }, (err : Response) => {
      console.log(err);
      this.http.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
    })

    this.initMap();

    this.addWinePathForm = this.createForm();
  }

  createForm(){
    return this.fb.group({
      languages : this.fb.array([
        this.initLanguage(1),
      ])
    })
  }

  onSubmit(){
    if(this.pointsArray.length < 2) {
      return this.http.showNotification({
        status: 'error',
        message: this.msg_wine_path_minimum_markers
      })
    }

    let winePathFormated = {
      start : this.pointsArray[0],
      end : this.pointsArray[this.pointsArray.length - 1],
      waypoints : this.pointsArray.filter((p, i)=>{
        return i !== 0 && i !== this.pointsArray.length -1 ;
      })
    }

    const fd: FormData = new FormData();
    fd.append("json", JSON.stringify({ ...this.addWinePathForm.value, ...winePathFormated }));
    fd.append("cover", this.wineImage);
    
    this.http.postWinepath(fd).subscribe(res=>{
      this.http.showNotification({
        status: 'success',
        message: this.msg_success_created
      });
      
      this.pathImg.nativeElement.click();
      this.wineImage = null;

      this.addWinePathForm = this.createForm();

      this.pointsArray = [];
      this.clearMap();

    }, err=>{
      this.http.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
      console.log(err)
    });
  }

  initLanguage(lang_id?){
    //console.log(lang_id)
    this.usedLanguages[lang_id] = lang_id;

    return this.fb.group({
        language_id : [lang_id, Validators.required],
        name : ["name"],
        value: ["", Validators.required]
    });

  }

  addLanguage(lang_id : number) {
    const control = <FormArray>this.addWinePathForm.controls['languages'];
    control.push(this.initLanguage(lang_id))
  }

  removeLanguage(event, i, lang_id: number) {
    event.stopPropagation();

    let indexInUsedLanguages = this.usedLanguages.indexOf(lang_id);
    this.usedLanguages[indexInUsedLanguages] = null;

    const control = <FormArray>this.addWinePathForm.controls['languages'];
    control.removeAt(i);
  }

  langAlreadyChoosen(lang_id){
    //console.log(lang_id)
    for(let i=0; i< this.langAlreadyChoosen.length; i++) {
      //console.log(lang_id + ' - ' + this.usedLanguages[i])
      return this.usedLanguages[i] == lang_id;
    }
  }


  wineImage= null;
  onFileChange(event) {
    let wine_image = <any>event.target.files[0];
    if(wine_image.type.indexOf('image') == -1) {
      this.http.showNotification({
        status: 'error',
        message: this.msg_file_is_not_image
      })
    }
    if(wine_image.type.indexOf('image') !== -1) {
      this.wineImage = <File>event.target.files[0];
    }
  }

  initMap() {
    this.map = new google.maps.Map(document.getElementById('map_demo'), {
      zoom: 7,
      center: {lat: 44.016521, lng: 20.865859},
      streetViewControl: false,
      scrollwheel: false
    });

    this.poly.setMap(this.map);

    // Add a listener for the click event
    this.map.addListener('click', (event)=>{ return this.addLatLng(event) });

    this.drawRoute(this.pointsArray);
  }

  // Clearing all waypoints and markers from map
  clearMap(){
    this.path.clear();
    this.poly.setMap(null);
    for(var i=0; i < this.markersArray.length; i++){
        this.markersArray[i].setMap(null);
    }
    this.markersArray = [];
    this.poly.setMap(this.map);
  };

  // Handles click events on a map, and adds a new point map
  addLatLng(event) {
    this.geocoder.geocode({
    'latLng': event.latLng
    }, (results, status)=>{
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          //alert(results[0].formatted_address);
          this.pointsArray.push({
            name: "",
            address: results[0].formatted_address,
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
          });
        }

        this.drawRoute(this.pointsArray);
      } else {
        window.alert(this.msg_cannot_add_marker);
      }
    });
  }

  drawRoute(data) {
    this.clearMap();

    if(!data.length) return;

    let markers = data;

    for (let i = 0; i < markers.length; i++) {
        let data = markers[i];
        let myLatlng = new google.maps.LatLng(data.lat, data.lng);

        let letter = String.fromCharCode("A".charCodeAt(0) + i);
        markers[i].letter = letter;

        let marker = new google.maps.Marker({
            position: myLatlng,
            map: this.map,
            title: data.name,
            icon: "http://maps.google.com/mapfiles/marker" + letter + ".png",
            draggable: true,
            id : i
        });

        this.markersArray.push(marker);

        ((marker, data)=>{
            // Click on marker handler
            google.maps.event.addListener(marker, "click", (event)=>{

              let pointType = data.type ? (' | <i> ' + data.type + ' </i>') : '';

              if(data.address.length) {
                this.infoWindow.setContent('<p><strong>' + data.name + '</strong>' + pointType + '</p><span>' + data.address + '</span>');
              } else {
                this.infoWindow.setContent(letter);
              }

              this.infoWindow.open(this.map, marker);
            });

            // Marker drop handler
            google.maps.event.addListener(marker, "dragend", (event)=>{
              this.clearMap();

              let lat = event.latLng.lat(); 
              let lng = event.latLng.lng();

              this.pointsArray[marker['id']].lat = lat;
              this.pointsArray[marker['id']].lng = lng;

              this.geocoder.geocode({
                'latLng': event.latLng
                }, (results, status)=>{
                  if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                      //alert(results[0].formatted_address);
                      this.pointsArray[marker['id']].address = results[0].formatted_address;

                      this.drawRoute(this.pointsArray);
                    }
                  } else {

                    this.drawRoute(this.oldPointsArray);
                    window.alert(this.msg_cannot_add_marker);
                  }
               });
            });
        })(marker, data);
    }

    let src =  data[0];
    let des =  data[data.length-1];

    this.poly.setPath(this.path); //add the ways to the polyine 
    this.service.route({
      origin: src,
      destination: des,
      waypoints : data.filter((point, i)=>{
        if (i != 0 && i != data.length-1) {
          return true;
        } else {
          return false;
        }
      }).map((point)=>{
        return {
          location: {
            lat: point.lat,
            lng: point.lng
          },
          stopover: true
        };
      }),
      travelMode: google.maps.DirectionsTravelMode.DRIVING,
      provideRouteAlternatives: false,
    }, ((s,d)=>{return (result, status)=>{
      //console.log(status)
      if (status == google.maps.DirectionsStatus.OK) {
        let len = result.routes[0].overview_path.length;
        for (let j = 0; j < len; j++) {
            this.path.push(result.routes[0].overview_path[j]);
        }
        this.oldPointsArray = JSON.parse(JSON.stringify(this.pointsArray));
      } else {
        if(status === 'ZERO_RESULTS') {
          this.pointsArray = JSON.parse(JSON.stringify(this.oldPointsArray));
          this.clearMap();
          this.drawRoute(this.pointsArray);
          window.alert(this.msg_cannot_add_marker);

        } else if(status === "MAX_WAYPOINTS_EXCEEDED") {
            this.pointsArray = JSON.parse(JSON.stringify(this.oldPointsArray));
            this.clearMap();
            this.drawRoute(this.pointsArray);
            window.alert(this.msg_max_allowed_waypoints_reached);
        } else {
            window.alert(this.msg_server_error);
        }
      }
    }})(src,des));
  }

  // Handle remove button click in dragable list
  removePoint(elemIndex, array) {
    array.splice(elemIndex, 1);
    this.drawRoute(this.pointsArray);
  }

  // Add custom function to marker, after used for bouncing on hover list items
  bounceMarker(id) {
    if(this.markersArray[id]) {
      this.markersArray[id].setAnimation(google.maps.Animation.BOUNCE);
      setTimeout(()=>{ this.markersArray[id].setAnimation(null); }, 50);
    }
  }

  addPathFromSearch(e) {
    this.pointsArray.push(e);

    this.drawRoute(this.pointsArray);
  }

  ngOnDestroy() {
    this.dragulaService.destroy('points-bag');
  }

}
