import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditWinePathComponent } from './edit-wine-path.component';

describe('EditWinePathComponent', () => {
  let component: EditWinePathComponent;
  let fixture: ComponentFixture<EditWinePathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditWinePathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditWinePathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
