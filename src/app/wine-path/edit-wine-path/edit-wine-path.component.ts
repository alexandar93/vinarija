import { Component, OnInit, OnDestroy, ViewChild, HostListener  } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormArray, FormBuilder, FormGroup, Validators }   from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
// import { } from '@types/googlemaps';

import { WinePathModel } from '../wine-path-model';
import { HelperService } from '../../services/helper.service';

import { AgmCoreModule, MapsAPILoader, GoogleMapsAPIWrapper } from "@agm/core";
import { } from 'googlemaps';
import { TranslateHelperService } from './../../services/translate-helper.service';

declare var google: any;
declare var $: any;

@Component({
  selector: 'app-wine-path',
  templateUrl: './edit-wine-path.component.html',
  styleUrls: ['./edit-wine-path.component.css']
})

export class EditWinePathComponent implements OnInit, OnDestroy {
  @ViewChild('pathImg') pathImg: any;

  map: google.maps.Map;

  // initial center position for the map
  lat: number = 44.016521;
  lng: number = 20.865859;

  poly = new google.maps.Polyline({
    strokeColor: '#920000',
    strokeOpacity: 1.0,
    strokeWeight: 3
  });

  //Initialize the Path Array
  path = new google.maps.MVCArray();

  //Initialize the Direction Service
  service = new google.maps.DirectionsService();
  geocoder = new google.maps.Geocoder();
  infoWindow = new google.maps.InfoWindow();
  lat_lng = new Array();
  markersArray = new Array();
  pointsArray = new Array();
  oldPointsArray = new Array();
  data : WinePathModel;
  placeTypes : Array<Object> = [];
  editWinePathForm: FormGroup;
  languagesArray = [];
  usedLanguages = [];
  pointsOfInterest;
  winePathId;
  searchText: any;

  /** translate strings */
  msg_server_error: string;
  msg_wine_path_minimum_markers: string;
  msg_success_edited: string;
  msg_file_is_not_image: string;
  msg_cannot_add_marker: string;
  msg_max_allowed_waypoints_reached: string;

  constructor(
    private dragulaService: DragulaService, 
    private http: HelperService, 
    private activatedRoute: ActivatedRoute, 
    private fb : FormBuilder,
    private translate: TranslateHelperService
    ) {
    
    this.placeTypes = http.placeTypes;

    dragulaService.setOptions('points-bag', {
      moves: function (el, container, handle) {
        return handle.className === 'material-icons drag_indicator';
      }
    });
    dragulaService.dragend.subscribe((value) => { 
      this.drawRoute(this.pointsArray);
    });
  }

  ngOnInit() {

    this.translate.getTranslate('WINE_PATH_ALERT_MSG_MIN_MARKERS_ERR').then((val: string) => {
      this.msg_wine_path_minimum_markers = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
      this.msg_success_edited = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_WINDOWS_ALERT_MAX_NO_POINTS').then((val: string) => {
      this.msg_max_allowed_waypoints_reached = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_is_not_image = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_CANNOT_ADD_MARKER').then((val: string) => {
      this.msg_cannot_add_marker = val;
    });

    this.winePathId = this.activatedRoute.snapshot.paramMap.get('id');

    this.getWinePath(parseInt(this.winePathId));

    this.http.getPointsOfInterests().subscribe(res=>{
      this.pointsOfInterest = res;
    }, err=> {
      console.log(err);
    });

    this.http.getLanguages().subscribe(res=>{
      this.languagesArray = res.json();

      //console.log( res.json())
    }, (err : Response) => {
      console.log(err);
      this.http.showNotification({
        status: 'error',
        message: 'Something went wrong.'
      })
    })

    this.initMap();

    this.editWinePathForm = this.createForm();
  }

  createForm(){
    return this.fb.group({
      languages : this.fb.array([
        //this.existingLangs()
      ])
    })
  }

  onSubmit(){
    //console.log(this.pointsArray)
    if(this.pointsArray.length < 2) {
      return this.http.showNotification({
        status: 'error',
        message: this.msg_wine_path_minimum_markers
      })
    }

    let winePathFormated = {
      start : this.pointsArray[0],
      end : this.pointsArray[this.pointsArray.length - 1],
      waypoints : this.pointsArray.filter((p, i)=>{
        return i !== 0 && i !== this.pointsArray.length -1 ;
      })
    }

    const fd: FormData = new FormData();
    fd.append("json", JSON.stringify({ ...this.editWinePathForm.value, ...winePathFormated }));
    fd.append("cover", this.wineImage);


    //console.log(this.editWinePathForm)
    
    this.http.updateWinepath(fd, this.winePathId ).subscribe(res=>{
      this.http.showNotification({
        status: 'success',
        message: this.msg_success_edited
      })
    }, err=>{
      this.http.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
      console.log(err)
    });
  }

  showData(data, field) {
    const control = <FormArray>this.editWinePathForm.controls[field];
    return control.push(data);
  }

  langList = [];
  existingLangs() {
    if(this.editWinePathForm) {
      const control = <FormArray>this.editWinePathForm.controls['languages'];
      control.controls = [];
    }

    //console.log(this.data.languages)
    if(this.data.languages) {
      for (let lang_obj of this.data.languages) {
        this.langList.push(lang_obj);
      };
      
      if (this.langList.length>0) {
        for(let lang of this.langList){

          this.usedLanguages[lang.language_id] = lang.language_id;

          this.showData(this.fb.group({
            language_id : [lang.language_id, Validators.required],
            name : ["name"],
            value: [lang.fields[0].value, Validators.required],
            id : [lang.fields[0].id, Validators.required]
          }), 'languages');
        }
      }
    } else {
      return false;
    }
  }

  removeLanguage(event, i, lang_id: number) {
    event.stopPropagation();

    let indexInUsedLanguages = this.usedLanguages.indexOf(lang_id);
    this.usedLanguages[indexInUsedLanguages] = null;

    const control = <FormArray>this.editWinePathForm.controls['languages'];
    control.removeAt(i);
  }

  langAlreadyChoosen(lang_id){
    //console.log(lang_id)
    for(let i=0; i< this.langAlreadyChoosen.length; i++) {
      //console.log(lang_id + ' - ' + this.usedLanguages[i])
      return this.usedLanguages[i] == lang_id;
    }
  }


  wineImage= null;
  onFileChange(event) {
    let wine_image = <any>event.target.files[0];
    if(wine_image.type.indexOf('image') == -1) {
      this.http.showNotification({
        status: 'error',
        message: this.msg_file_is_not_image
      })
    }
    if(wine_image.type.indexOf('image') !== -1) {
      this.wineImage = <File>event.target.files[0];
    }
  }

  removeImage() {
    this.wineImage = null;
  }

  getWinePath(id : number){
    this.http.getWinePath(id).subscribe(res=>{
      this.http.handleResponse(res);
      this.data = res;
      this.wineImage = res.cover_image || null;

      //console.log(res)

      this.existingLangs();
      this.pointsArray = [this.data.start, ...this.data.waypoints, this.data.end];
      this.drawRoute(this.pointsArray);
    }, err=>{
      console.log(err)
    })
  }

  initLanguage(lang_id?){
    //console.log(lang_id)
    this.usedLanguages[lang_id] = lang_id;

    let langFieldId = null;
    for(let i=0; i<this.data.languages.length; i++) {
      if(this.data.languages[i].language_id === lang_id) {
        langFieldId = this.data.languages[i].fields[0].id;
      }
    }

    return this.fb.group({
        language_id : [lang_id, Validators.required],
        name : ["name"],
        value: ["", Validators.required],
        id : [langFieldId],
    });
  }

  addLanguage(lang_id : number) {
    const control = <FormArray>this.editWinePathForm.controls['languages'];
    control.push(this.initLanguage(lang_id))
  }

  initMap() {
    this.map = new google.maps.Map(document.getElementById('map_demo'), {
      zoom: 7,
      center: {lat: 44.016521, lng: 20.865859},
      streetViewControl: false,
      scrollwheel: false
    });

    this.poly.setMap(this.map);

    // Add a listener for the click event
    this.map.addListener('click', (event)=>{ return this.addLatLng(event) });

    this.drawRoute(this.pointsArray);
  }

  // Clearing all waypoints and markers from map
  clearMap(){
    this.path.clear();
    this.poly.setMap(null);
    for(var i=0; i < this.markersArray.length; i++){
        this.markersArray[i].setMap(null);
    }
    this.markersArray = [];
    this.poly.setMap(this.map);
  };

  // Handles click events on a map, and adds a new point map
  addLatLng(event) {
    //console.log(event)
    this.geocoder.geocode({
    'latLng': event.latLng
    }, (results, status)=>{
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          //alert(results[0].formatted_address);
          this.pointsArray.push({
            name: "",
            address: results[0].formatted_address,
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
          });
        }

        this.drawRoute(this.pointsArray);
      } else {
        window.alert(this.msg_cannot_add_marker);
      }
    });
  }

  drawRoute(data) {
    this.clearMap();

    if(!data.length) return;

    let markers = data;

    for (let i = 0; i < markers.length; i++) {
        let data = markers[i];
        
        let myLatlng = new google.maps.LatLng(data.lat, data.lng);

        let letter = String.fromCharCode("A".charCodeAt(0) + i);

        markers[i].letter = letter;

        let marker = new google.maps.Marker({
            position: myLatlng,
            map: this.map,
            title: data.name,
            icon: "http://maps.google.com/mapfiles/marker" + letter + ".png",
            draggable: true,
            id : i
        });

        this.markersArray.push(marker);

        ((marker, data)=>{
            // Click on marker handler
            google.maps.event.addListener(marker, "click", (event)=>{

              let pointTypeArr = this.placeTypes.filter((type)=>{
                return data.type == type['id'];
              })

              let pointType = data.type ? (' | <i> ' + pointTypeArr[0]['name'] + ' </i>') : '';

              //console.log(data)
              if(data.address !== null) {
                this.infoWindow.setContent('<p><strong>' + data.name + '</strong>' + pointType + '</p><span>' + data.address + '</span>');
              } else {
                this.infoWindow.setContent(letter);
              }

              this.infoWindow.open(this.map, marker);
            });

            // Marker drop handler
            google.maps.event.addListener(marker, "dragend", (event)=>{
              this.clearMap();

              let lat = event.latLng.lat(); 
              let lng = event.latLng.lng();

              this.pointsArray[marker['id']].lat = lat;
              this.pointsArray[marker['id']].lng = lng;

              this.geocoder.geocode({
                'latLng': event.latLng
                }, (results, status)=>{
                  if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                      //alert(results[0].formatted_address);
                      this.pointsArray[marker['id']].address = results[0].formatted_address;

                      this.drawRoute(this.pointsArray);
                    }
                  } else {

                    this.drawRoute(this.oldPointsArray);
                    window.alert(this.msg_cannot_add_marker);
                  }
               });
            });
        })(marker, data);
    }
    

    //Loop and Draw Path Route between the Points on MAP
    //for (var i = 0; i < this.lat_lng.length; i++) {
      //if ((i+1) <  this.lat_lng.length) {
        let src =  data[0];
        let des =  data[data.length-1];

        //console.log(src, des)

        this.poly.setPath(this.path); //add the ways to the polyine 
        this.service.route({
          origin: src,
          destination: des,
          waypoints : data.filter((point, i)=>{
            if (i != 0 && i != data.length-1) {
              return true;
            } else {
              return false;
            }
          }).map((point)=>{
            return {
              location: {
                lat: point.lat,
                lng: point.lng
              },
              stopover: true
            };
          }),
          travelMode: google.maps.DirectionsTravelMode.DRIVING,
          provideRouteAlternatives: false,
        }, ((s,d)=>{return (result, status)=>{
          //console.log(status)
          if (status == google.maps.DirectionsStatus.OK) {
            let len = result.routes[0].overview_path.length;
            for (let j = 0; j < len; j++) {
                this.path.push(result.routes[0].overview_path[j]);
            }
            this.oldPointsArray = JSON.parse(JSON.stringify(this.pointsArray));
          } else {
            if(status === 'ZERO_RESULTS') {
              this.pointsArray = JSON.parse(JSON.stringify(this.oldPointsArray));
              this.clearMap();
              this.drawRoute(this.pointsArray);
              window.alert(this.msg_cannot_add_marker);

            } else if(status === "MAX_WAYPOINTS_EXCEEDED") {
                this.pointsArray = JSON.parse(JSON.stringify(this.oldPointsArray));
                this.clearMap();
                this.drawRoute(this.pointsArray);
                window.alert(this.msg_max_allowed_waypoints_reached);
            } else {
                window.alert(this.msg_server_error);
            }
          }
          
          // else { 
          //   this.path.push(s); //add points to the plyline
          // }	
        }})(src,des));
      //}
    //}
  }

  // Handle remove button click in dragable list
  removePoint(elemIndex, array) {
    array.splice(elemIndex, 1);
    this.drawRoute(this.pointsArray);
  }

  // Add custom function to marker, after used for bouncing on hover list items
  bounceMarker(id) {
    if(this.markersArray[id]) {
      this.markersArray[id].setAnimation(google.maps.Animation.BOUNCE);
      setTimeout(()=>{ this.markersArray[id].setAnimation(null); }, 50);
    }
  }

  getPointsOfInterest() {

  }

  addPathFromSearch(e) {
    this.pointsArray.push(e);

    this.drawRoute(this.pointsArray);
  }

  ngOnDestroy() {
    this.dragulaService.destroy('points-bag');
  }

}