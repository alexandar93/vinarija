import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})

export class FilterPipe implements PipeTransform {

  transform(items: any[], searchText: string, type? : string): any[] {
    //console.log(items)
    if(!items) return [];

    if(type === 'winePath') {
      if(items && (!searchText || searchText.length < 1)) return items;
    }

    if(!searchText || searchText.length < 1) return;
    searchText = searchText.toLowerCase();
    
    return items.filter( it => {
        //console.log(it.name)
        return it.name.toLowerCase().includes(searchText);
    });
   }
}