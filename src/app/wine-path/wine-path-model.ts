export interface WinePathModel {
    id: number,
    name: string,
    cover_image: string,
    languages?: Array<any>,
    start: {
        lat: number,
        lng: number,
        name: string,
        address: string,
        type : number
    },
    end: {
        lat: number,
        lng: number,
        name: string,
        address: string,
        type : number
    },
    waypoints: [
        {
            id?: number,
            name: string,
            address: string,
            lat: number,
            lng: number,
            type: number
        }
    ]
}