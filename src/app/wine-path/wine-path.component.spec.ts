import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WinePathComponent } from './wine-path.component';

describe('WinePathComponent', () => {
  let component: WinePathComponent;
  let fixture: ComponentFixture<WinePathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinePathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinePathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
