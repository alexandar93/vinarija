import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Response } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';

import { HelperService } from '../services/helper.service';
import { WinePathModel } from './wine-path-model';
import { PageEvent, MatPaginator } from '@angular/material';
import { TranslateHelperService } from './../services/translate-helper.service';

declare var swal:any;

@Component({
  selector: 'app-wine-path',
  templateUrl: './wine-path.component.html',
  styleUrls: ['./wine-path.component.css']
})

export class WinePathComponent implements OnInit {
  
  winePaths : Array<WinePathModel> = [];
  currentPage : number = 1;
  totalPages : number = null;
  loading : boolean = true;
  model: string = '';
  modelChanged: Subject<string> = new Subject<string>();
  languagesArray = [];
  choosenLang;
  // MatPaginator Output
  pageEvent: PageEvent;
  total: number = null;
  searchText: any;

  /** translate strings */
  msg_server_error: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_delete_path_title: string;
  msg_delete_path_text: string;
  msg_delete_path_success_title: string;
  msg_delete_path_success_text: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private WinePathsHttp: HelperService, private router : Router, private translate: TranslateHelperService) {
    this.modelChanged
        .debounceTime(400)
        .distinctUntilChanged()
        .subscribe(model => {
            this.model = model
            this.currentPage = 1;
            this.searchWinePaths(this.currentPage, model)
        });
  }

  ngOnInit() {
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });

    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_delete_path_title = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_delete_path_text = val; 
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_path_success_title = val;
    });
    this.translate.getTranslate('WINE_PATH_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_path_success_text = val;
    });

    this.searchWinePaths(this.currentPage, '');

    this.WinePathsHttp.getLanguages().subscribe(res=>{
      //console.log(res.json())
      this.languagesArray = res.json();
    }, (err : Response) => {
      // console.log(err);
      this.WinePathsHttp.showNotification({
        status: 'error',
        message: this.msg_server_error
      })
    })

  }

  changed(text: string) {
    //console.log(text)
    this.modelChanged.next(text);
  }
  nextPage(value) {
    console.log(value)
    // value.pageIndex
    this.searchWinePaths(value.pageIndex + 1, this.model);
    //scroll page to top
    const container = document.querySelector('.main-panel');
            if(container !== null) {
                container.scrollTop = 0;
            }
  }

  langChanged(event) {
    //console.log(event.value)
    this.searchWinePaths(1, this.model, event.value);
  }

  onEditWinePath(id){
    this.router.navigate(['/wine-paths/edit-wine-path/',  id]);
  }

//   getWinePaths(id, searchQuery : string = '') {
      
//     this.WinePathsHttp.getWinePaths(id, searchQuery).subscribe(res =>{
//         //console.log(res.data)
//         this.WinePathsHttp.handleResponse(res);
//         this.winePaths = res.data;
//         this.currentPage = res.current_page;
//         this.totalPages = Math.ceil(res.total / res.per_page);
//         //console.log(this.totalPages)
//     }, err=>{
//         console.error(err);
//     });
//   }

  searchWinePaths(id, string : string, lang = 1) {

    this.WinePathsHttp.searchWinePaths(id, string, lang).subscribe(res =>{
        //console.log(res.data)
        this.WinePathsHttp.handleResponse(res);
        this.winePaths = res.data;
        this.currentPage = res.current_page;
        this.total = res.total;
        this.totalPages = Math.ceil(res.total / res.per_page);
        //console.log(this.totalPages)
    }, err=>{
        // console.error(err);
    });
  }

  onDeleteWinePath(id, name){
    swal({
        title: this.msg_delete_path_title + ` ${name} ?`,
        text: 'Podatke nije moguće povratiti nakon brisanja!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: this.msg_swal_button_yes,
        cancelButtonText: this.msg_swal_button_no,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger",
        buttonsStyling: false

    }).then(()=>{

        this.WinePathsHttp.deleteWinePath(id).subscribe(res=>{
            //console.log(id)
            //let response = res.json();
            this.WinePathsHttp.handleResponse(res);
            this.searchWinePaths(this.currentPage, this.model);
            swal({
                title: this.msg_delete_path_success_title,
                text: this.msg_delete_path_success_text,
                type: 'success',
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
            })
    
        }, err=>{
            // console.log(err);
        })
        
    }, (dismiss)=>{
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        // if (dismiss === 'cancel') {
        // swal({
        //     title: 'Cancelled',
        //     text: 'Your imaginary file is safe :)',
        //     type: 'error',
        //     confirmButtonClass: "btn btn-info",
        //     buttonsStyling: false
        // })
        // }
    })
    
  }




  

}