import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { DragulaModule } from 'ng2-dragula';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http'

//import { HelperService } from '../services/helper.service';

import { WinePathComponent } from './wine-path.component';
import { WinePathRoutes } from './wine-path.routing';
import { AddWinePathComponent } from './add-wine-path/add-wine-path.component';
import { EditWinePathComponent } from './edit-wine-path/edit-wine-path.component';
import { FilterPipe} from './filter.pipe';
import { AgmCoreModule } from '@agm/core';
import { MatSelectModule, MatTooltipModule, MatPaginatorModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
//import { DirectionsMapDirective } from './agm-directions.directive'


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(WinePathRoutes),
        FormsModule,
        ReactiveFormsModule,
        DragulaModule,
        HttpClientModule,
        HttpModule,
        MatSelectModule,
        MatTooltipModule,
        MatPaginatorModule,
        TranslateModule
        // AgmCoreModule.forRoot({
        //     apiKey: 'AIzaSyDrMV9T0XSpS7ugGaRGf94xCqmnLkIs8w8'
        // })
    ],
    declarations: [
        WinePathComponent,
        AddWinePathComponent,
        EditWinePathComponent,
        FilterPipe
        //DirectionsMapDirective
    ],
    exports :[MatSelectModule, MatTooltipModule, MatPaginatorModule],
    providers : [
        //GoogleMapsAPIWrapper
        //HelperService
    ]
})

export class WinePathModule {}
