import { Routes } from '@angular/router';

import { WinePathComponent } from './wine-path.component';
import { EditWinePathComponent } from './edit-wine-path/edit-wine-path.component';
import { AddWinePathComponent } from './add-wine-path/add-wine-path.component';

export const WinePathRoutes: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: WinePathComponent
        },
        {
            path: 'edit-wine-path/:id',
            component: EditWinePathComponent
        },
        {
            path: 'add-wine-path',
            component: AddWinePathComponent,
            pathMatch: 'full'
        }]
    }
];