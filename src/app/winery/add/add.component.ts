import {
  Component,
  OnDestroy,
  OnInit,
  NgZone,
  ElementRef,
  ViewChild
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  RequiredValidator
} from "@angular/forms";
import { HttpService } from "../../services/http.service";
import { NotificationsService } from "../../notifications/notifications.service";
import { Subscription } from "rxjs/Subscription";

import { AgmCoreModule, MapsAPILoader, GoogleMapsAPIWrapper } from "@agm/core";
import {} from "googlemaps";
import { ImageVideoValidatorsService } from "../../services/image-video-validators.service";
import { Globals } from "../../model/globals";
import { DragulaService } from "ng2-dragula";
import { galleryFiles } from "../winery.model";
import {
  UploadOutput,
  UploadInput,
  UploadFile,
  humanizeBytes,
  UploaderOptions
} from "ngx-uploader";
import { TranslateHelperService } from './../../services/translate-helper.service'; 
import { HelperService } from './../../services/helper.service';

declare var $: any;
declare var google: any;
declare var swal: any;

const datepicker = {
  // datepickerArray:[],
  monFri: [],
  saturday: [],
  sunday: []
};

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.css"]
})
export class AddComponent implements OnInit, OnDestroy {
  /** variables */
  subscriptionarea: Subscription;
  subscriptionlang: Subscription;
  defaultLanguage: number = 1;
  wineryForm: FormGroup;
  items: FormArray;
  langs: any[] = [];
  areas: any[] = [];
  languages: any[] = [];
  lat: number;
  lng: number;
  logoFile: any;
  coverFile: any;
  videoFile: any;
  contactNo = "^[0-9]+"; // pattern for contact number
  websiteUrl = "https?://.+"; // pattern for web site url
  fileValidator: any[] = [];
  defaultLangs: any = {
    id: "",
    name: ""
  };
  modules: any;
  images: Array<{position:number, file:any, type:string}> = new Array();
  videos: Array<{position:number, file:any, type:string}> = new Array();
  file: galleryFiles[] = [];
  files: Array<{position:number, file:File, type:string, src: string}> = new Array();
  /** list of regions */
  regionsList: any[] = [];
  /** list of reons */
  reonsList: any[] = [];
  /** list of vinogorja */
  vinogorjaList: any[] = [];
  /** id for region */
  region_id: number;
  /** id for reon */
  reon_id: number;
  /** list of winery admins */
  listOfAdmins: any[] = [];
  /** is winery admin logged */
  isWineryAdmin: boolean;
  /** msg translate */
  msg_swal_delete_language_title: string;
  msg_swal_delete_language_text: string;
  msg_swal_delete_language_success_title: string;
  msg_swal_delete_language_success_text: string;
  msg_swal_delete_language_unsuccess: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_alert_file_is_not_video: string;
  msg_alert_format_is_not_video: string;
  msg_alert_file_is_not_image: string;
  msg_alert_format_is_not_image: string;
  msg_alert_file_error: string;
  msg_alert_date_mon_fri_error: string;
  msg_alert_date_sath_error: string;
  msg_alert_date_sunday_error: string;
  msg_alert_file_false_error: string;
  msg_loading_upload_title: string;
  msg_alert_success_created: string;
  msg_server_error: string;
  msg_alert_files_not_valid: string;

  public editor;
  // public editorContent = `<h3>I am Example content</h3>`;
  // public editorOptions = {
  //   placeholder: "insert content..."
  // };
  @ViewChild("inputImageGallery") inputImageGallery: any;
  @ViewChild("inputVideoGallery") inputVideoGallery: any;
  @ViewChild("logoPath") logoPath: any;
  @ViewChild("coverPath") coverPath: any;
  @ViewChild("videoPath") videoPath: any;
  @ViewChild("address") public searchElementRef: ElementRef;
  @ViewChild("f") myNgForm: any;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private validate: ImageVideoValidatorsService,
    private globals: Globals,
    private dragulaService: DragulaService,
    private translate: TranslateHelperService,
    private helper: HelperService
  ) {
    dragulaService.setOptions("gallery-bag", {
      moves: function(el, container, handle) {
        return handle.className === "drag_indicator preview";
      }
      // removeOnSpill: true,
      // copy: false,
    });
    dragulaService.dropModel.subscribe(value => {
      this.onDropModel(value.slice(1));
    });
    dragulaService.removeModel.subscribe(value => {
      this.onRemoveModel(value.slice(1));
    });
    // console.log(globals.language);
    this.modules = globals.editor_settings;

    if(helper.isWineryAdmin()) {
      this.isWineryAdmin = true;
    }
  }

  ngOnInit() {

    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_swal_delete_language_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_swal_delete_language_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_swal_delete_language_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_swal_delete_language_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_swal_delete_language_unsuccess = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_VIDEO_FILE_IS_NOT_VIDEO').then((val: string) => {
      this.msg_alert_file_is_not_video = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_VIDEO_FILE_FORMAT').then((val: string) => {
      this.msg_alert_format_is_not_video = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_alert_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_alert_format_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_SERVER_ERRROR').then((val: string) => {
      this.msg_alert_file_error = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_MONDAY_FRIDAY_NOT_VALID').then((val: string) => {
      this.msg_alert_date_mon_fri_error = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_SATURDAY_NOT_VALID').then((val: string) => {
      this.msg_alert_date_sath_error = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_SUNDAY_NOT_VALID').then((val: string) => {
      this.msg_alert_date_sunday_error = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_FALSE_FILE').then((val: string) => {
      this.msg_alert_file_false_error = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_loading_upload_title = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_SUCCESS_CREATE').then((val: string) => {
      this.msg_alert_success_created = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_NOT_VALID_UPLOADED_FILES').then((val: string) => {
      this.msg_alert_files_not_valid = val;
    });




    this.lat = 43.3176108;
    this.lng = 21.9079216;

    this.subscriptionarea = this.http
      .get("area/dropdown/nested", 1)
      .subscribe(httpResponse => {
        this.regionsList = httpResponse.json();
      });

    this.http.get('user/winery_admin/dropdown', 1).subscribe(res => {
      if(res.status === 200) {
        this.listOfAdmins = res.json();
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });

    this.subscriptionlang = this.http
      .get("dropdown/language", 1)
      .subscribe(httpResponse => {
        this.langs = httpResponse.json();
        this.langs.forEach(item => {
          if (item.id == this.defaultLanguage) {
            this.defaultLangs.id = item.id;
            this.defaultLangs.name = item.name;
          }
        });
        this.addItem(this.defaultLangs);
        // this.langs.splice(0, 1);
      });

    this.wineryForm = this.fb.group({
      address: ["", Validators.required],
      // cardTitle: ["srb"],
      contact: ["", Validators.pattern(this.contactNo)],
      contact_person: [""],
      webpage: ["", Validators.pattern(this.websiteUrl)],
      recommended: [""],
      highlighted: [""],
      mondayFriday: [""],
      saturday: [""],
      sunday: [""],
      ponpet: [""],
      sub: [""],
      ned: [""],
      admins: ["", Validators.required],
      area_id: ["", Validators.required],
      items: this.fb.array([]),
      languages: [""],
      listOfRegions: ["", Validators.required],
      listOfReons: ["", Validators.required],
      listOfVinogorja: ["", Validators.required]
    });
    //if winery admin is logged set id
    if(this.isWineryAdmin) { 
      const adminData = JSON.parse(localStorage.getItem('user_data'));
      let admins: any[] = [];
      admins.push(adminData.id);
      this.wineryForm.controls.admins.setValue(admins);
    }
    //  Init Bootstrap Select Picker
    if ($(".selectpicker").length !== 0) {
      $(".selectpicker").selectpicker();
    }

    $(".timepicker").datetimepicker({
      format: "H:mm", // use this format if you want the 24hours timepicker
      // format: 'h:mm A',    //use this format if you want the 12hours timpiecker with AM/PM toggle
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: "fa fa-chevron-left",
        next: "fa fa-chevron-right",
        today: "fa fa-screenshot",
        clear: "fa fa-trash",
        close: "fa fa-remove",
        inline: true,
        sideBySide: true
      },
      stepping: 30
    });

    //set current position
    this.setCurrentPosition();
    //get lng and lat for map
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement,
        { types: ["address"], country: "srb" }
      );
      // Set initial restrict to the greater list of countries.
      autocomplete.setComponentRestrictions({ country: ["rs"] });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        });
      });
    });
  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      });
    }
  }
  private onDropModel(args) {
    let [el, target, source] = args;
    // do something else
  }

  private onRemoveModel(args) {
    let [el, source] = args;
    // do something else
  }

  onRemoveLangs(language_name, language_id, index) {
    let selected = this.wineryForm.get("items") as FormArray;

    swal({
      title: this.msg_swal_delete_language_title + ` ${language_name}`,
      text: this.msg_swal_delete_language_title,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        swal({
          title: this.msg_swal_delete_language_success_title,
          text: `${language_name} ` + this.msg_swal_delete_language_success_text,
          type: "success",
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        });
        selected.removeAt(index);
        let renewLang = {
          name: language_name,
          id: language_id
        };
        this.langs.push(renewLang);
      },
      dismiss => {}
    );
  }
  /** set value of dropdown for reons */
  dropdownValueRegion(id) {
    // console.log(id);
    // console.log(this.wineryForm.controls.listOfRegions.value);
    if(this.regionsList !== undefined) {
      this.regionsList.forEach(region => {
        if(region.id == id) {
          this.reonsList = region.children;
          this.region_id = id;
        }
      });
    }
  }
  /** set value of dropdown for vinogorje */
  dropdownValueReon(id) {
    // console.log(id);
    // console.log(this.wineryForm.controls.listOfReons.value);
    if(this.reonsList !== undefined) {
      this.reonsList.forEach(reon => {
        if(reon.id == id) {
          this.vinogorjaList = reon.children;
          this.reon_id = reon.id;
        }
      });
    }
  }
  /** set value for final selected region */
  dropdownValueVinogorje(id) {
    // console.log(this.wineryForm.controls.listOfVinogorja.value);
    this.wineryForm.controls.area_id.setValue(id);
  }

  onChoseLocation(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
  }

  createLanguage(languageId, languageName): FormGroup {
    return this.fb.group({
      wineryName: ["", Validators.required],
      wineryDesc: ["", Validators.required],
      language: languageId,
      language_name: [languageName]
    });
  }

  addItem(value): void {
    this.items = this.wineryForm.get("items") as FormArray;
    this.items.push(this.createLanguage(value.id, value.name));
    // let index = this.langs.indexOf(value, 0);
    let index = this.langs.findIndex(e => e.id==value.id);
    this.langs.splice(index, 1);
  }
  onUploadVideoFiles(event) {
    let videoValid: any = {};
    // console.log(event);
    let files = <Array<File>>event.target.files;
    for (let i = 0; i < files.length; i++) {
      if (event.target.files[i] !== undefined) {
        if (event.target.files[i].type.indexOf("video") == -1) {
          this.alert.showNotification(
            this.msg_alert_file_is_not_video,
            "danger",
            ""
          );
          videoValid.isValid = false;
          this.fileValidator[4] = videoValid;
          return false;
        }
        if (!this.validate.validateVideo(event.target.files[i].name)) {
          this.alert.showNotification(
            this.msg_alert_format_is_not_video,
            "danger",
            ""
          );
          videoValid.isValid = false;
          this.fileValidator[4] = videoValid;
          return false;
        }
        // this.files.push({position:i,file:event.target.files[i], type: 'video', src: ''});
        this.videos.push({position:i, file:event.target.files[i], type: 'video'});
      }
    }
  }
  onUploadImageFiles(event) {
    // this.http.uploadGallery('create/winery', event.target.files, 'gallery', 1).subscribe(res => {
    //   console.log(res);
    // });
    // let preview = document.querySelector('#preview');
    let logoValid: any = {};
    // console.log("image event:", <File>event.target.files[0]);
    // console.log(event)
    let files = event.target.files;
    for (let i = 0; i < files.length; i++) {
      if (event.target.files[i] !== undefined) {
        if (event.target.files[i].type.indexOf("image") == -1) {
          this.alert.showNotification(
            this.msg_alert_file_is_not_image,
            "danger",
            ""
          );
          logoValid.isValid = false;
          this.fileValidator[3] = logoValid;
          return false;
        }
        if (!this.validate.validateImage(event.target.files[i].name)) {
          this.alert.showNotification(
            this.msg_alert_format_is_not_image,
            "danger",
            ""
          );
          logoValid.isValid = false;
          this.fileValidator[3] = logoValid;
          return false;
        }
        let reader = new FileReader();
        reader.addEventListener(
          "load",
          (e: any) => {

            // console.log(e);
            this.file[i] = {
              // name: event.target.files[i].name,
              // size: event.target.files[i].size,
              // type: event.target.files[i].type,
              src: e.srcElement.result
            };
            // this.images.push({position:i,file:this.file[i], type: 'image'});
            this.files.push({position:i,file:event.target.files[i], type: 'image', src: this.file[i].src});
            // console.log("image event:", event);
          },
          false
        );
        reader.readAsDataURL(event.target.files[i]);
      }
      /** dodavanje slike u niz */
    }
    logoValid.isValid = true;
    this.fileValidator[0] = logoValid;
    // this.files = <File>event.target.files;
    // console.log('image gallery', this.files);
    // console.log(this.files[0]);
    // this.inputImageGallery.nativeElement.value = "";
  }
  /** remove video from gallery list */
  removeVideo(index, video) {
    this.inputVideoGallery.nativeElement.value = "";
    this.videos.splice(index, 1);
    console.log('video', video);
    console.log(this.files);
  }
  /** remove image from gallery list */
  removeImage(index, image) {
    this.inputImageGallery.nativeElement.value = "";
    this.files.splice(index, 1); 

    console.log(this.files);
  }
  /** on uploading logo */
  onUploadLogo(event) {
    let file = <File>event.target.files[0];
    // console.log(event);
    let logoValid: any = {};
    logoValid.name = "logo";
    if (file !== undefined) {
      if (file.type.indexOf("image") == -1) {
        this.alert.showNotification(
          this.msg_alert_file_is_not_image,
          "danger",
          ""
        );
        logoValid.isValid = false;
        this.fileValidator[0] = logoValid;
        return false;
      }
      if (!this.validate.validateImage(file.name)) {
        this.alert.showNotification(
          this.msg_alert_format_is_not_image,
          "danger",
          ""
        );
        logoValid.isValid = false;
        this.fileValidator[0] = logoValid;
        return false;
      } else {
        logoValid.isValid = true;
        this.fileValidator[0] = logoValid;
        this.logoFile = <File>event.target.files[0];
        // console.log(this.logoFile);
      }
    }
  }
  /** on uploading cover */
  onUploadCover(event) {
    let coverValid: any = {};
    coverValid.name = "cover";
    let file = <File>event.target.files[0];
    if (file !== undefined) {
      if (file.type.indexOf("image") == -1) {
        this.alert.showNotification(
          this.msg_alert_file_is_not_image,
          "danger",
          ""
        );
        coverValid.isValid = false;
        this.fileValidator[1] = coverValid;
        return false;
      }
      if (!this.validate.validateImage(file.name)) {
        this.alert.showNotification(
          this.msg_alert_format_is_not_image,
          "danger",
          ""
        );
        coverValid.isValid = false;
        this.fileValidator[1] = coverValid;
        return false;
      } else {
        coverValid.isValid = true;
        this.fileValidator[1] = coverValid;
        this.coverFile = <File>event.target.files[0];
      }
    }
  }
  /** on uploading video */
  onUploadVideo(event) {
    let videoValid: any = {};
    videoValid.name = "video";
    let file = <File>event.target.files[0];
    if (file !== undefined) {
      if (file.type.indexOf("video") == -1) {
        this.alert.showNotification(
          this.msg_alert_file_is_not_video,
          "danger",
          ""
        );
        videoValid.isValid = false;
        this.fileValidator[2] = videoValid;
        return false;
      }
      if (!this.validate.validateVideo(file.name)) {
        this.alert.showNotification(
          this.msg_alert_format_is_not_video,
          "danger",
          ""
        );
        videoValid.isValid = false;
        this.fileValidator[2] = videoValid;
        return false;
      } else {
        videoValid.isValid = true;
        this.fileValidator[2] = videoValid;
        this.videoFile = <File>event.target.files[0];
      }
    }
  }
  /** reset form */
  resetForm() {
    this.myNgForm.resetForm({ mondayFriday: "", saturday: "", sunday: "" });
  }
  /** on submit */
  onSubmit() {
    const fd: FormData = new FormData();

    let langForm = this.wineryForm.get("items") as FormArray; // vrednosti FormArray 'items'
    datepicker.monFri = [];
    datepicker.saturday = [];
    datepicker.sunday = [];

    

    $("input.timepicker").each(function(index, value) {
      let d = $(value).data("date");

      if ($(value).attr("name") === "mondayFridayTimeStart") {
        datepicker.monFri.push(d);
      }
      if ($(value).attr("name") === "mondayFridayTimeEnd") {
        datepicker.monFri.push(d);
      }
      /**
       *
       */

      if ($(value).attr("name") === "saturdayTimeStart") {
        datepicker.saturday.push(d);
      }
      if ($(value).attr("name") === "saturdayTimeEnd") {
        datepicker.saturday.push(d);
      }
      /**
       *
       */
      if ($(value).attr("name") === "sundayTimeStart") {
        datepicker.sunday.push(d);
      }
      if ($(value).attr("name") === "sundayTimeEnd") {
        datepicker.sunday.push(d);
      }
    });
    
    let checking = this.wineryForm.controls;

    if (checking.mondayFriday.value === true) {
      if (datepicker.monFri[0] === undefined) {
        //TODO poruka lose upisan datum
        this.alert.showNotification(
          this.msg_alert_date_mon_fri_error,
          "danger",
          "notifications"
        );
        return false;
      } else if (datepicker.monFri[1] === undefined) {
        //TODO poruka lose upisan datum
        this.alert.showNotification(
          this.msg_alert_date_mon_fri_error,
          "danger",
          "notifications"
        );
        return false;
      } else {
        this.wineryForm.controls["ponpet"].setValue(datepicker.monFri);
      }
    }
    if (checking.saturday.value === true) {
      if (datepicker.saturday[0] === undefined) {
        this.alert.showNotification(
          this.msg_alert_date_sath_error,
          "danger",
          "notifications"
        );
        return false;
      } else if (datepicker.saturday[1] === undefined) {
        this.alert.showNotification(
          this.msg_alert_date_sath_error,
          "danger",
          "notifications"
        );
        return false;
      } else {
        this.wineryForm.controls["sub"].setValue(datepicker.saturday);
      }
    }
    if (checking.sunday.value === true) {
      if (datepicker.sunday[0] === undefined) {
        this.alert.showNotification(
          this.msg_alert_date_sunday_error,
          "danger",
          "notifications"
        );
        return false;
      } else if (datepicker.sunday[1] === undefined) {
        this.alert.showNotification(
          this.msg_alert_date_sunday_error,
          "danger",
          "notifications"
        );
        return false;
      } else {
        this.wineryForm.controls["ned"].setValue(datepicker.sunday);
      }
    }

    let languages: any[] = [];

    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "description",
        value: formGroup.controls["wineryDesc"].value
      });
      languages.push({
        language_id: formGroup.controls.language.value,
        name: "name",
        value: formGroup.controls["wineryName"].value
      });
    });

    let isRecommended = this.wineryForm.controls.recommended.value;
    let isHighlighted = this.wineryForm.controls.highlighted.value;

    if (isRecommended == true) {
      this.wineryForm.controls.recommended.setValue(1);
    }
    if (isRecommended == false) {
      this.wineryForm.controls.recommended.setValue(0);
    }
    if (isHighlighted == true) {
      this.wineryForm.controls.highlighted.setValue(1);
    }
    if (isHighlighted == false) {
      this.wineryForm.controls.highlighted.setValue(0);
    }
    this.wineryForm.controls.languages.setValue(languages);
    this.wineryForm.controls.address.setValue(
      this.searchElementRef.nativeElement.value
    );
    let formInput = this.wineryForm.value;
    delete formInput.items;
    delete formInput.mondayFriday;
    delete formInput.sunday;
    delete formInput.saturday;
    delete formInput.listOfRegions;
    delete formInput.listOfReons;
    delete formInput.listOfVinogorja;

    if (this.logoFile !== undefined) {
      fd.append("logo", this.logoFile);
    }
    if (this.coverFile) {
      fd.append("cover", this.coverFile);
    }
    if (this.videoFile) {
      fd.append("video", this.videoFile);
    }
    let marker = {
      lat: this.lat,
      lng: this.lng
    };
    formInput.point = marker;
    /** FormData niz za slike gallery[] */
    for (let i = 0; i < this.files.length; i++) {
      if(this.files[i].type === 'image') {
         fd.append("gallery[]", this.files[i].file);
      }
    }
    this.videos.forEach(video => {
      fd.append("gallery[]", video.file);
    });

    fd.append("json", JSON.stringify(formInput));
    let isFormValid = true;
    this.fileValidator.forEach(element => {
      if (element.isValid === false) {
        isFormValid = false;
        this.alert.showNotification(
          this.msg_alert_file_false_error + element.name,
          "danger",
          "notifications"
        );
        return false;
      }
    });
    // Display the key/value pairs
     
      // console.log(fd.getAll('gallery'), fd.get('json'));

    if (isFormValid) {
      swal({ title: this.msg_loading_upload_title, allowOutsideClick: false });
      swal.showLoading();
      this.http.postFormData("create/winery", fd).subscribe(
        httpResponse => {
          //   if (event.type === HttpEventType.UploadProgress) {
          //     console.log(
          //       "Upload progress:" +
          //         Math.round(event.loaded / event.total * 100) +
          //         "%"
          //     )
          //     console.log(event);
          //   }
          if (httpResponse.status === 201) {
            swal.close();
            let controls = this.wineryForm.get("items") as FormArray;
            while (controls.length !== 0) {
              controls.removeAt(0);
            }
            this.http.get("dropdown/language", 1).subscribe(httpResponse => {
              this.langs = httpResponse.json();
              this.addItem(this.defaultLangs);
            });
            // this.wineryForm.reset({mondayFriday: "", saturday: "", sunday: ""});
            // clear image tumbnails
            this.logoPath.nativeElement.click();
            this.coverPath.nativeElement.click();
            this.videoPath.nativeElement.click();
            this.coverFile = null;
            this.logoFile = null;
            this.videoFile = null;
            this.videos = [];
            this.images = [];
            this.files = [];
            // reseting form
            this.resetForm();
            this.wineryForm.controls.recommended.setValue(0);
            this.wineryForm.controls.highlighted.setValue(0);
            this.inputImageGallery.nativeElement.value = "";
            this.inputVideoGallery.nativeElement.value = "";
            $("input#image_gallery").html();

            $("input.timepicker").each((index, value) => {
              $($(value).data("date", "")).val("");
            });
            this.alert.showNotification(
              this.msg_alert_success_created,
              "success",
              "notification"
            );
          }
        },
        error => {
          swal.close();
          this.inputImageGallery.nativeElement.value = "";
          this.inputVideoGallery.nativeElement.value = "";
          this.alert.showNotification(
            this.msg_server_error,
            "danger",
            "error"
          );
        }
      );
    } else {
      this.inputImageGallery.nativeElement.value = "";
      this.inputVideoGallery.nativeElement.value = "";
      this.alert.showNotification(
        this.msg_alert_files_not_valid,
        "danger",
        ""
      );
    }
  }
  /** destroying subscription and dragulaService */
  ngOnDestroy() {
    this.subscriptionarea.unsubscribe();
    this.subscriptionlang.unsubscribe();
    this.dragulaService.destroy("gallery-bag");
  }
}
