import { Component, OnDestroy, OnInit, NgZone, ViewChild, ElementRef } from "@angular/core";
import {
  HttpClientModule,
  HttpClient,
  HttpEventType,
  HttpRequest
} from "@angular/common/http";
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from '@angular/common';

import { HttpService } from "../../services/http.service";
import { NotificationsService } from "../../notifications/notifications.service";
import { Subscription } from "rxjs/Subscription";
import { createLanguageService } from "tslint";
import { LogicalOperator } from "typescript";
import { headersToString } from "selenium-webdriver/http";

import { AgmCoreModule, MapsAPILoader, GoogleMapsAPIWrapper } from "@agm/core";
import { } from 'googlemaps';
import { ImageVideoValidatorsService } from "../../services/image-video-validators.service";
import { MapOptions } from "@agm/core/services/google-maps-types";
import { Globals } from "../../model/globals";
import { MAT_OPTION_PARENT_COMPONENT } from "@angular/material";
import { DragulaService } from "ng2-dragula";
import { Constants } from '../../services/endpoint.service'
import { TranslateHelperService } from './../../services/translate-helper.service';
import { HelperService } from './../../services/helper.service';


declare var $: any;
declare var swal: any;
declare var google: any;

const datepicker = {
  monFri: [],
  saturday: [],
  sunday: []
};
// value of checked field
const dateValue = {
  mondayFridayCheck: false,
  saturdayCheck: false,
  sundayCheck: false
};

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"]
})
export class EditComponent implements OnInit, OnDestroy {
  /** Variables */
  options: any;
  wineryForm: FormGroup;
  mondayFridayTimeStart: Date;
  items: FormArray;
  langs: any[] = [];
  areas: any[] = [];
  langForm: any;
  subscriptionarea: Subscription;
  subscriptionlang: Subscription;
  subscriptiondata: Subscription;
  subscriptionparams: Subscription;
  serverData: any;
  id: any;
  logo_image: string = null;
  cover_image: string = null;
  video: string;
  lat: number;
  lng: number;
  logoFile: any;
  coverFile: any;
  videoFile: any;
  videoImage: string;
  ponpetTime: string;
  subTime: string;
  nedTime: string;
  contactNo = "^[0-9]+"; // pattern for contact number
  websiteUrl = "https?://.+" // pattern for web site url
  fileValidator: any[] = []; // array contain objects for file validation
  isLogoValid: any;
  mapOptions: MapOptions;
  modules: any;
  /** list of regions */
  regionsList: any[] = [];
  /** list of reons */
  reonsList: any[] = [];
  /** list of vinogorja */
  vinogorjaList: any[] = [];
  /** id for region */
  region_id: number;
  /** id for reon */
  reon_id: number;
  /** list of winery admins */
  listOfAdmins: any[] = [];
  /** gallery files */
  images: Array<{position: number, file: any, type: string, src: string}> = new Array();
  videos: Array<{file: any, type: string}> = new Array();
  file: any[] = [];
  files: Array<File> = new Array();

  /** translate strings */
  msg_server_error: string;
  msg_success_saved_lang: string;
  msg_succcess_edited: string;
  msg_swal_delete_language_title: string;
  msg_swal_delete_language_text: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_swal_delete_language_success_title: string;
  msg_swal_delete_language_success_text: string;
  msg_swal_delete_unsuccess: string;
  msg_alert_file_is_not_video: string;
  msg_alert_format_is_not_video: string;
  msg_alert_file_is_not_image: string;
  msg_alert_format_is_not_image: string;
  msg_swal_delete_video_title: string;
  msg_swal_delete_video_text: string;
  msg_swal_delete_video_success_title: string;
  msg_swal_delete_video_success_text: string;
  msg_swal_delete_video_unsucces: string;
  msg_swal_loading: string;
  msg_swal_delete_image_title: string;
  msg_swal_delete_image_text: string;
  msg_swal_delete_image_success_title: string;
  msg_swal_delete_image_success_text: string;
  msg_swal_delete_image_unsuccess: string;
  msg_alert_save_current_language: string;
  msg_alert_monday_friday_not_valid: string;
  msg_alert_sath_not_valid: string;
  msg_alert_sunday_not_valid: string;
  msg_file_invalid: string;
  msg_image_is_not_addded: string;
  msg_uploaded_file_error: string;
  msg_image_is_not_added: string;

  @ViewChild("address") public searchElementRef: ElementRef;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private httpc: HttpClient,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private validate: ImageVideoValidatorsService,
    private globals: Globals,
    private dragulaService: DragulaService,
    private endpoint: Constants,
    private translate: TranslateHelperService,
    private helper: HelperService,
    private loc: Location
  ) {
     dragulaService.setOptions("gallery-bag", {
      moves: function(el, container, handle) {
        return handle.className === "drag_indicator preview";
      }
      // removeOnSpill: true,
      // copy: false,
    });

    dragulaService.dropModel.subscribe(value => {
      this.onDropModel(value.slice(1));
    });
    dragulaService.removeModel.subscribe(value => {
      this.onRemoveModel(value.slice(1));
    });
    dragulaService.dragend.subscribe((value) => { 
      this.repositionFiles(value);
    });
    this.modules = globals.editor_settings;

     if(this.helper.isWineryAdmin()) {
      const userData = JSON.parse(localStorage.getItem('user_data'));
      loc.subscribe((val) => {
         if(val.url !== '/winery/edit/' + this.id) {
           return loc.replaceState('/winery/edit/' + this.id);
         }
       });
    }
  }

  ngOnInit() {
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) =>{
      this.msg_server_error = val;
    });
    this.translate.getTranslate('LANG_ALERT_SUCCESS_SAVED').then((val: string) =>{
      this.msg_success_saved_lang = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) =>{
      this.msg_swal_delete_language_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) =>{
      this.msg_swal_delete_language_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) =>{
      this.msg_swal_delete_language_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) =>{
      this.msg_swal_delete_language_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) =>{
      this.msg_swal_delete_unsuccess = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_VIDEO_FILE_IS_NOT_VIDEO').then((val: string) =>{
      this.msg_alert_file_is_not_video = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_VIDEO_FILE_FORMAT').then((val: string) =>{
      this.msg_alert_format_is_not_video = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) =>{
      this.msg_alert_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) =>{
      this.msg_alert_format_is_not_image = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) =>{
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) =>{
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_VIDEO_REMOVE_TITLE').then((val: string) =>{
      this.msg_swal_delete_video_title = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_VIDEO_REMOVE_TEXT').then((val: string) =>{
      this.msg_swal_delete_video_text = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_VIDEO_REMOVE_SUCCESS_TITLE').then((val: string) =>{
      this.msg_swal_delete_video_success_title = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_VIDEO_REMOVE_SUCCESS_TEXT').then((val: string) =>{
      this.msg_swal_delete_video_success_text = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_VIDEO_REMOVE_UNSUCCESS').then((val: string) =>{
      this.msg_swal_delete_video_unsucces = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) =>{
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_IMAGE_REMOVE_TITLE').then((val: string) =>{
      this.msg_swal_delete_image_title = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_IMAGE_REMOVE_TEXT').then((val: string) =>{
      this.msg_swal_delete_image_text = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_IMAGE_REMOVE_SUCCESS_TITLE').then((val: string) =>{
      this.msg_swal_delete_image_success_title = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_IMAGE_REMOVE_SUCCESS_TEXT').then((val: string) =>{
      this.msg_swal_delete_image_success_text = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_IMAGE_REMOVE_UNSUCCESS').then((val: string) =>{
      this.msg_swal_delete_image_unsuccess = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_SAVE_LANG_ALERT').then((val: string) =>{
      this.msg_alert_save_current_language = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_MONDAY_FRIDAY_NOT_VALID').then((val: string) =>{
      this.msg_alert_monday_friday_not_valid = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_SATURDAY_NOT_VALID').then((val: string) =>{
      this.msg_alert_sath_not_valid = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_SUNDAY_NOT_VALID').then((val: string) =>{
      this.msg_alert_sunday_not_valid = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_FALSE_FILE').then((val: string) => {
      this.msg_file_invalid = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
      this.msg_succcess_edited = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_GALLERY_IMAGE_ERR').then((val: string) => {
      this.msg_image_is_not_added = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_NOT_VALID_UPLOADED_FILES').then((val: string) => {
      this.msg_uploaded_file_error = val;
    });



    this.subscriptionparams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    this.http.get('user/winery_admin/dropdown', 1).subscribe(res => {
      if(res.status === 200) {
        this.listOfAdmins = res.json();
      }
    });

    this.subscriptionlang = this.http
      .get("dropdown/language", 1)
      .subscribe(httpresponse => {
        this.langs = httpresponse.json();
        // this.langs.splice(0, 1);
      });
    this.subscriptionarea = this.http
      .get("area/dropdown/nested", 1)
      .subscribe(httpresponse => {
        this.regionsList = httpresponse.json();
        this.initLoadingData();
      });
    
    this.wineryForm = this.fb.group({
      address: ["", Validators.required],
      contact: ["", Validators.pattern(this.contactNo)],
      contact_person: [""],
      webpage: ["", Validators.pattern(this.websiteUrl)],
      mondayFriday: [""],
      saturday: [""],
      sunday: [""],
      ponpet: [""],
      sub: [""],
      ned: [""],
      area_id: ["", Validators.required],
      recommended: [""],
      highlighted: [""],
      admins: ["", Validators.required],
      items: this.fb.array([]),
      languages: [''],
      listOfRegions: ["", Validators.required],
      listOfReons: ["", Validators.required],
      listOfVinogorja: ["", Validators.required],
      point: {
        lat: this.lat,
        lng: this.lng
      }
    });

    //init data for address
    // this.getLatLan(this.wineryForm.controls['address'].value);
    let addressControl = this.wineryForm.controls['address'].value;
    //  Init Bootstrap Select Picker
    if ($(".selectpicker").length !== 0) {
      $(".selectpicker").selectpicker();
    }
    $(".timepicker").datetimepicker({
      format: "H:mm", // use this format if you want the 24hours timepicker
      // format: 'h:mm A',    //use this format if you want the 12hours timpiecker with AM/PM toggle
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: "fa fa-chevron-left",
        next: "fa fa-chevron-right",
        today: "fa fa-screenshot",
        clear: "fa fa-trash",
        close: "fa fa-remove",
        inline: true,
        sideBySide: true
      },
      stepping: 30
    });
    //set current position
    this.setCurrentPosition();
    //get lng and lat for map
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
       // Set initial restrict to the greater list of countries.
       autocomplete.setComponentRestrictions(
        {'country': ['rs']});
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        });
      });
    });
    if(this.videoImage == null) {
      this.videoImage = './assets/img/placeholder-video.jpg';
    } else {
      this.videoImage = './assets/img/placeholder-video.jpg';
    }
  }
  /** set value of dropdown for reons */
  dropdownValueRegion(id) {
    if(this.regionsList !== undefined) {
      this.regionsList.forEach(region => {
        if(region.id == id) {
          this.reonsList = [];
          this.reonsList = region.children;
          this.region_id = id;
          this.vinogorjaList = [];
        }
      });
    }
  }
  /** set value of dropdown for vinogorje */
  dropdownValueReon(id) {
    if(this.reonsList !== undefined) {
      this.reonsList.forEach(reon => {
        if(reon.id == id) {
          this.vinogorjaList = [];
          this.vinogorjaList = reon.children;
          this.reon_id = reon.id;
        }
      });
    }
  }
  /** set value for final selected region */
  dropdownValueVinogorje(id) {
    this.wineryForm.controls.area_id.setValue(id);
  }

  initLoadingData(onSave = false) {
    this.subscriptiondata = this.http
      .get(`patch/initialize/winery/` + this.id, 1)
      .subscribe(
        httpResponse => {
          let adminsList: any[] = [];
          let serverData = this.serverData = httpResponse.json();
          this.wineryForm.controls["address"].setValue(this.serverData.address);
          this.wineryForm.controls["webpage"].setValue(this.serverData.webpage);
          this.wineryForm.controls["contact"].setValue(this.serverData.contact);
          this.wineryForm.controls["contact_person"].setValue(this.serverData.contact_person);
          this.serverData.admin.forEach(admin => {
            adminsList.push(admin.id);
          });
          this.wineryForm.controls.admins.setValue(adminsList);
          this.lat = serverData.pin.lat;
          this.lng = serverData.pin.lng;
          this.ponpetTime = serverData.ponpet;
          this.subTime = serverData.sub;
          this.nedTime = serverData.ned;

          if(serverData.recommended == 1) {
            this.wineryForm.controls.recommended.setValue(true);
          }
          if(serverData.recommended == 0) {
            this.wineryForm.controls.recommended.setValue(false);
          }
          if(serverData.highlighted == 1) {
            this.wineryForm.controls.highlighted.setValue(true);
          }
          if(serverData.highlighted == 0) {
            this.wineryForm.controls.highlighted.setValue(false);
          }
          this.regionsList.forEach(parent => {
            parent.children.forEach(childrenReon => {
              childrenReon.children.forEach(childrenVinogorje => {
                if(childrenVinogorje.id === serverData.area.id) {
                  this.wineryForm.controls.listOfVinogorja.setValue(childrenVinogorje.id);
                  this.wineryForm.controls.listOfReons.setValue(childrenReon.id);
                  this.wineryForm.controls.listOfRegions.setValue(parent.id);
                  this.wineryForm.controls.area_id.setValue(this.wineryForm.controls.listOfVinogorja.value);
                  // console.log(this.wineryForm.value);
                  this.reonsList = parent.children;
                  this.vinogorjaList = childrenReon.children;
                }
              });
            });
          });
          

          // this.wineryForm.controls["area_id"].setValue(this.serverData.area.id);

          this.logo_image = this.serverData.logo_image;
          this.cover_image = this.serverData.cover_image;
          this.video = this.serverData.video;
          
          this.http.get('gallery/' + this.id + '/get', 1).subscribe(httpResponse => {
             if(httpResponse.status === 200) {
               let galleryData = httpResponse.json();
               let ext: string;
               galleryData.forEach(fileName => {
                 ext = fileName.filename.substr(fileName.filename.lastIndexOf('.') + 1);
                 if(ext==='jpg' || ext==='jpeg' || ext==='png') {
                    // let name = imgName.substr(0, imgName.lastIndexOf('.'));
                    let name = fileName.filename;
                    let src = fileName.url;

                    let data = {
                      id: fileName.id,
                      name: name,
                      type: 'image/' + ext,
                      src: src,
                      position: fileName.position
                    }
                    this.images.push({position: fileName.position, file: data, type: 'image', src: src});
                 }
                 if(ext === 'mp4') {
                   let name = fileName.filename;
                   // let src = this.endpoint.endpoint + '/gallery/' + this.id + '/get/' + fileName;
                   let data = {
                     id: fileName.id,
                     name: name,
                     type: 'video/mp4',
                     position: fileName.position,
                     // src: fileName.url
                   }
                   this.videos.push({type: 'video', file: data});
                 }
               });
             }
          }, err => {
            this.alert.showNotification(this.msg_server_error, 'danger', '');
          });

          serverData.languages.forEach((lang, langIndex) => {
            let name = "",
                desc = "";
            let fieldsindex;
            let name_id;
            let desc_id;

            lang.fields.forEach((field, fieldIndex) => {
              fieldsindex = fieldIndex;
              if (field.name === "name") {
                name = field.value;
                name_id = field.id;
              }
              if (field.name === "description") {
                desc = field.value;
                desc_id = field.id;
              }
            });
            if(!onSave) {
              var index = this.langs.findIndex(
                item => item.name === lang.language
              ); // SELECTING INDEX OF OBJECT IN ARRAY BY PROPERTY *(etc. name)
              this.langs.splice(index, 1);
              }
            let language_name = lang.language;
            let language_id = lang.language_id;
            this.createItem(
              name_id,
              desc_id,
              name,
              desc,
              language_name,
              language_id,
              false
            );
          });
        },
        error => {
          // console.log(error);
        }
      );
  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      });
    }
  }
  private onDropModel(args) {
    let [el, target, source] = args;
    // do something else
  }

  private onRemoveModel(args) {
    let [el, source] = args;
    // do something else
  }
  onRemoveTime(day: string) {
    if(day === 'ponpet'){
      this.ponpetTime = '/';
      this.wineryForm.controls["mondayFriday"].setValue(false);
    }
    if(day === 'sub'){
      this.subTime = '/';
      this.wineryForm.controls["saturday"].setValue(false);
    }
    if(day === 'ned'){
      this.nedTime = '/';
      this.wineryForm.controls["sunday"].setValue(false);
    }
  }
  
  onSaveLanguage(value) {
    let languageFormFields: any[] = [];
    languageFormFields.push({
      language_id: value.controls.name_id.value,
      name: "description",
      value: value.controls.wineryDesc.value
    });
    languageFormFields.push({
      language_id: value.controls.name_id.value,
      name: "name",
      value: value.controls.wineryName.value
    });
    let postData = {
      languages: languageFormFields
    };
    this.http
      .post("add/language/winery/" + this.id, postData)
      .subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          let itemArray = this.wineryForm.controls['items'] as FormArray;
         itemArray.controls.forEach(element =>{
           element.markAsUntouched;
         });
          this.alert.showNotification(
            this.msg_success_saved_lang,
            "success",
            "notifications"
          );
          this.removeItem()
          this.initLoadingData(true);
        }
      }, error => {
        if(error.status === 500) {
          this.alert.showNotification(this.msg_server_error, 'danger', 'notifications');
        }
      });
  }

  onRemoveLangs(language_id, language_name, index) {
    let selected = this.wineryForm.get("items") as FormArray;
    
    swal({
      title: this.msg_swal_delete_language_title + ` ${language_name}`,
      text: this.msg_swal_delete_language_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        this.http
          .delete("delete/language/winery/" + this.id + "/" + language_id)
          .subscribe(httpResponse => {
            if (httpResponse.status === 204) {
              swal({
                title: this.msg_swal_delete_language_success_title,
                text: `${language_name} ` + this.msg_swal_delete_language_success_text,
                type: "success",
                confirmButtonClass: "btn btn-success",
                buttonsStyling: false
              });
              selected.removeAt(index);
              let renewLang = {
                name: language_name,
                id: language_id
              };
              this.langs.push(renewLang);
            } else if (httpResponse.status !== 204) {
            }
          },error => {
            if(error.status === 500) {
              this.alert.showNotification(this.msg_server_error, 'danger', 'notifications');
            }
            this.alert.showNotification(
              this.msg_swal_delete_unsuccess,
              "danger",
              "error"
            );
          });
      }, (dismiss) => {

      });
  }

  onChoseLocation(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
  }
  repositionFiles(value) {
    // TODO kada slika promeni mesto upisti position njenu trenutnu poziciju u nizu + 1 mesto 
    this.images.forEach(img => {
      if(img.file.id == value[1].id) {
        let index = this.images.indexOf(img, 0);
        this.http.get('gallery/' + this.id + '/reposition/' + value[1].id + '/' + (index + 1), 1).subscribe(httpResponse => {
          if(httpResponse.status === 204) {
            // TODO 
          }
        }, err => {
          this.alert.showNotification(this.msg_server_error, 'danger', '');
        });
      }
    });
  }
  /** uploading videos for gallery */
  onUploadVideoFiles(event) {
    let videoValid: any = {};
    let files = <Array<File>>event.target.files;
    for (let i = 0; i < files.length; i++) {
      if (event.target.files[i] !== undefined) {
        if (event.target.files[i].type.indexOf("video") == -1) {
          this.alert.showNotification(
            this.msg_alert_file_is_not_video,
            "danger",
            ""
          );
          videoValid.isValid = false;
          this.fileValidator[4] = videoValid;
          return false;
        }
        if (!this.validate.validateVideo(event.target.files[i].name)) {
          this.alert.showNotification(
            this.msg_alert_format_is_not_video,
            "danger",
            ""
          );
          videoValid.isValid = false;
          this.fileValidator[4] = videoValid;
          return false;
        }
        // this.files.push(event.target.files[i]);
        this.videos.push({file: event.target.files[i], type: 'video'});
      }
    }
  }
  /** uploading images for gallery */
  onUploadImageFiles(event) {
    // this.http.uploadGallery('create/winery', event.target.files, 'gallery', 1).subscribe(res => {
    //   console.log(res);
    // });
    // let preview = document.querySelector('#preview');
    let logoValid: any = {};
    let files = event.target.files;
    for (let i = 0; i < files.length; i++) {
      if (event.target.files[i] !== undefined) {
        if (event.target.files[i].type.indexOf("image") == -1) {
          this.alert.showNotification(
            this.msg_alert_file_is_not_image,
            "danger",
            ""
          );
          logoValid.isValid = false;
          this.fileValidator[3] = logoValid;
          return false;
        }
        if (!this.validate.validateImage(event.target.files[i].name)) {
          this.alert.showNotification(
            this.msg_alert_format_is_not_image,
            "danger",
            ""
          );
          logoValid.isValid = false;
          this.fileValidator[3] = logoValid;
          return false;
        }
        let reader = new FileReader();
        reader.addEventListener(
          "load",
          (e: any) => {

            // console.log(e);
            // this.file[i] = {
            //   name: event.target.files[i].name,
            //   size: event.target.files[i].size,
            //   type: event.target.files[i].type,
            //   src: e.srcElement.result
            // };
            this.images.push({position: null, file: event.target.files[i], type: 'image', src: e.srcElement.result});
            
          },
          false
        );
        reader.readAsDataURL(event.target.files[i]);
      }
      /** dodavanje slike u niz */
      // this.files.push(event.target.files[i]);
    }
    logoValid.isValid = true;
    this.fileValidator[0] = logoValid;
    // this.files = <File>event.target.files;
  }
  /** remove video from gallery */
  removeVideo(index, video) {
    if(("id" in video.file)) {
    swal({
      title: this.msg_swal_delete_video_title + ` ${name}`,
      text: this.msg_swal_delete_video_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
          swal({title: this.msg_swal_loading, allowOutsideClick: false});
          swal.showLoading();
           this.http.get('gallery/' + this.id + '/remove/' + video.file.id, 1).subscribe(httpResponse => {
                if(httpResponse.status === 204) {
                  swal.close();
                  swal({
                    title: this.msg_swal_delete_video_success_title,
                    text: `${name} ` + this.msg_swal_delete_video_success_text,
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                  });
                  this.videos.splice(index, 1);
                }
          },error => {
            if(error.status === 500) {
              this.alert.showNotification(this.msg_server_error, 'danger', 'notifications');
            }
            this.alert.showNotification(
              this.msg_swal_delete_video_unsucces,
              "danger",
              "error"
            );
          });
      }, (dismiss) => {

        });
    } else {
      this.videos.splice(index, 1);
    }
    
  }
  /** remove image from gallery */
  removeImage(index, image) {
    // console.log(image);
    if(("id" in image.file)) {
    swal({
      title: this.msg_swal_delete_image_title + ` ${image.name}`,
      text: this.msg_swal_delete_image_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
          swal({title: this.msg_swal_loading, allowOutsideClick: false});
          swal.showLoading();
           this.http.get('gallery/' + this.id + '/remove/' + image.file.id, 1).subscribe(httpResponse => {
                if(httpResponse.status === 204) {
                  swal.close();
                  swal({
                    title: this.msg_swal_delete_image_success_title,
                    text: `${image.name} ` + this.msg_swal_delete_image_success_text,
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                  });
                  this.images.splice(index, 1);
                  
                }
          },error => {
            if(error.status === 500) {
              swal.close();
              this.alert.showNotification(this.msg_server_error, 'danger', 'notifications');
            }
            this.alert.showNotification(
              this.msg_swal_delete_image_unsuccess,
              "danger",
              "error"
            );
          });
      }, (dismiss) => {

        });
    } else {
      this.images.splice(index, 1);
    }
  }

  onUploadLogo(event) {
    let file = <File>event.target.files[0];
    let logoValid: any = {};
    logoValid.name = 'logo';
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_alert_file_is_not_image, 'danger', '');
      logoValid.isValid = false;
      this.fileValidator[0] = logoValid;
      return false;
    }
    if(!this.validate.validateImage(file.name)) {
      this.alert.showNotification(this.msg_alert_format_is_not_image, 'danger', '');
      logoValid.isValid = false;
      this.fileValidator[0] = logoValid;
      return false;
    } else {
      logoValid.isValid = true;
      this.fileValidator[0] = logoValid;
      this.logoFile = <File>event.target.files[0];
    }
  }
  onUploadCover(event) {
    let coverValid: any = {};
    coverValid.name = 'cover';
    let file = <File>event.target.files[0];
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_alert_file_is_not_image, 'danger', '');
      coverValid.isValid = false;
      this.fileValidator[1] = coverValid;
      return false;
    }
    if(!this.validate.validateImage(file.name)) {
      this.alert.showNotification(this.msg_alert_format_is_not_image, 'danger', '');
      coverValid.isValid = false;
      this.fileValidator[1] = coverValid;
      return false;
    } else {
      coverValid.isValid = true;
      this.fileValidator[1] = coverValid;
      this.coverFile = <File>event.target.files[0];
    }
  }
  onUploadVideo(event) {
    let videoValid: any = {};
    videoValid.name = 'video';
    let file = <File>event.target.files[0];
    if(file.type.indexOf('video') == -1) {
      this.alert.showNotification(this.msg_alert_file_is_not_video, 'danger', '');
      videoValid.isValid = false;
      this.fileValidator[2] = videoValid;
      return false;
    }
    if(!this.validate.validateVideo(file.name)) {
      this.alert.showNotification(this.msg_alert_format_is_not_video, 'danger', '');
      videoValid.isValid = false;
      this.fileValidator[2] = videoValid;
      return false;
    } else {
      videoValid.isValid = true;
      this.fileValidator[2] = videoValid;
      this.videoFile = <File>event.target.files[0];
    }
  }
  createLanguage(nameId, descId, wName, wDesc, lName, lId, isNew = true): FormGroup {
    let fg = this.fb.group({
      wineryName: [wName, Validators.required],
      wineryDesc: [wDesc, Validators.required],
      name_id: nameId,
      desc_id: descId,
      language_name: [lName],
      language_id: [lId],
      flag: [""]
    });
    if (isNew) fg.controls['flag'].setValue(1);
    return fg;
  }

  // adding new item to FormArray items and splice item from langsArray
  addItem(value): void {
    let isSaved;
    this.items = this.wineryForm.get("items") as FormArray;
    this.items.controls.forEach(item => {
      let items = item as FormGroup;
      if (items.controls.flag.value === 1) {
        isSaved = true;
        swal({
          title: this.msg_alert_save_current_language,
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success"
      });
      return;
      }
      isSaved = false;
    });
    if (!isSaved) {
    this.items.push(
      this.createLanguage(value.id, null , "", "", value.name, value.id)
    );
    let index = this.langs.findIndex(e => e.id==value.id);
    // let index = this.langs.indexOf(value, 0);
    this.langs.splice(index, 1);
  }
  }
  removeItem() {
    let controls = this.wineryForm.get('items') as FormArray;
    while (controls.length !== 0) {
      controls.removeAt(0);
    }
  }

  createItem(nameId, descId, wName, wDesc, lname, lid, isNew = true): void {
    this.items = this.wineryForm.get("items") as FormArray;
    this.items.push(this.createLanguage(nameId, descId, wName, wDesc, lname, lid, isNew));
  }

  // setting name of selected item from list on button
  // onSelectDropdown(id: number, name: string) {
  //   let dropValue: HTMLElement = document.getElementById("areaDropdown");
  //   dropValue.innerText = name;
  //   this.wineryForm.controls["area_id"].setValue(id);
  // }

  onSubmit() {
    const fd: FormData = new FormData();
    const fdGallery: FormData = new FormData();
    let galleryValid = false;

    let langForm = this.wineryForm.get("items") as FormArray; // vrednosti FormArray 'items'

    datepicker.monFri = [];
    datepicker.saturday = [];
    datepicker.sunday = [];

    dateValue.mondayFridayCheck = this.wineryForm.controls["mondayFriday"].value;
    dateValue.saturdayCheck = this.wineryForm.controls["saturday"].value;
    dateValue.sundayCheck = this.wineryForm.controls["sunday"].value;

    // loop for checked field and push time to Obj
    $("input.timepicker").each((index, value) => {
      let d = $(value).data("date"); // init - empty : undefined
      if (dateValue.mondayFridayCheck === true) {
        if ($(value).attr("name") === "mondayFridayTimeStart") {
          datepicker.monFri.push(d);
        }
        if ($(value).attr("name") === "mondayFridayTimeEnd") {
          datepicker.monFri.push(d);
        }
      }
      if (dateValue.saturdayCheck === true) {
        if ($(value).attr("name") === "saturdayTimeStart") {
          datepicker.saturday.push(d);
        }
        if ($(value).attr("name") === "saturdayTimeEnd") {
          datepicker.saturday.push(d);
        }
      }
      if (dateValue.sundayCheck === true) {
        if ($(value).attr("name") === "sundayTimeStart") {
          datepicker.sunday.push(d);
        }
        if ($(value).attr("name") === "sundayTimeEnd") {
          datepicker.sunday.push(d);
        }
      } 
    });
    

    // checking datepicker and set value to FormControler for time (ponpet, sub, ned)
    if (datepicker.monFri.length !== 0) {
      if (
        datepicker.monFri[0] !== undefined &&
        datepicker.monFri[1] !== undefined
      ) {
        this.wineryForm.controls["ponpet"].setValue(datepicker.monFri);
      } else {
        this.alert.showNotification(this.msg_alert_monday_friday_not_valid, 'danger', 'notifications');
        return false;
      }
    } else {
      if(this.ponpetTime !== '/') {
        let formattingTime = this.serverData.ponpet;
        let timeArray: string[] = [];
        timeArray[0] = formattingTime.substring(0,5);
        timeArray[1] = formattingTime.substring(8);
        this.wineryForm.controls["ponpet"].setValue(timeArray);
      } else {
        this.wineryForm.controls["ponpet"].setValue("");
      }
        // ponpet empty
    }
    if (datepicker.saturday.length !== 0) {
      if (
        datepicker.saturday[0] !== undefined &&
        datepicker.saturday[1] !== undefined
      ) {
        this.wineryForm.controls["sub"].setValue(datepicker.saturday);
      } else {
        this.alert.showNotification(this.msg_alert_sath_not_valid, 'danger', 'notifications');
        return false;
      }
    } else {
      if(this.subTime !== '/') {
        let formattingTime = this.serverData.sub;
        let timeArray: string[] = [];
        timeArray[0] = formattingTime.substring(0,5);
        timeArray[1] = formattingTime.substring(8);
        this.wineryForm.controls["sub"].setValue(timeArray);
      } else {
        this.wineryForm.controls["sub"].setValue("");
      }
    }
    if (datepicker.sunday.length !== 0) {
      if (
        datepicker.sunday[0] !== undefined &&
        datepicker.sunday[1] !== undefined
      ) {
        this.wineryForm.controls["ned"].setValue(datepicker.sunday);
      } else {
        this.alert.showNotification(this.msg_alert_sunday_not_valid, 'danger', 'notifications');
        return false;
      }
    } else {
       if(this.nedTime !== '/') {
        let formattingTime = this.serverData.ned;
        let timeArray: string[] = [];
        timeArray[0] = formattingTime.substring(0,5);
        timeArray[1] = formattingTime.substring(8);
        this.wineryForm.controls["ned"].setValue(timeArray);
      } else {
        this.wineryForm.controls["ned"].setValue("");
      }
    }
    let languages: any[] = [];
    
    // parse data from FormArray items to this.language array
    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      languages.push({
        id: formGroup.controls.desc_id.value,
        name: "description",
        value: formGroup.controls["wineryDesc"].value
      });
      languages.push({
        id: formGroup.controls.name_id.value,
        name: "name",
        value: formGroup.controls["wineryName"].value
      });
    });
    this.wineryForm.controls.languages.setValue(languages);
    this.wineryForm.controls.address.setValue(this.searchElementRef.nativeElement.value);

    let isRecommended = this.wineryForm.controls.recommended.value;
    let isHighlighted = this.wineryForm.controls.highlighted.value;

    if(isRecommended == true) {
      this.wineryForm.controls.recommended.setValue(1);
    }
    if(isRecommended == false) {
      this.wineryForm.controls.recommended.setValue(0);
    }
    if(isHighlighted == true) {
      this.wineryForm.controls.highlighted.setValue(1);
    }
    if(isHighlighted == false) {
      this.wineryForm.controls.highlighted.setValue(0);
    }
    let formInput = this.wineryForm.value;
    delete formInput.items;
    delete formInput.mondayFriday;
    delete formInput.sunday;
    delete formInput.saturday;
    delete formInput.listOfRegions;
    delete formInput.listOfReons;
    delete formInput.listOfVinogorja;
    if (this.logoFile !== undefined) {
      fd.append("logo", this.logoFile);
    }
    if (this.coverFile !== undefined) {
      fd.append("cover", this.coverFile);
    }
    if (this.videoFile !== undefined) {
      fd.append("video", this.videoFile);
    }


    let marker = {
      lat: this.lat,
      lng: this.lng
    };
    formInput.point = marker;
     /** FormData niz za galeriju slika file[] */
    for (let i = 0; i < this.images.length; i++) {
      if(!("id" in this.images[i].file)) {
        galleryValid = true;
         fdGallery.append("file[]", this.images[i].file);
      }
    }
     /** FormData niz za galeriju video snimka file[] */
    for (let i = 0; i < this.videos.length; i++) {
      if(!("id" in this.videos[i].file)) {
        galleryValid = true;
         fdGallery.append("file[]", this.videos[i].file);
      }
    }
    fd.append("json", JSON.stringify(formInput));
    let isFormValid = true;
    this.fileValidator.forEach(element => {
      if(element.isValid === false) {
        isFormValid = false;
        this.alert.showNotification(this.msg_file_invalid + element.name, 'danger', 'notifications');
        return false;
      }
    })
    if(isFormValid) {
      swal({title: this.msg_swal_loading, allowOutsideClick: false});
      swal.showLoading();
    this.http.postFormData("patch/winery/" + this.id, fd).subscribe(
      httpResponse => {
        //   if (event.type === HttpEventType.UploadProgress) {
        //     console.log(
        //       "Upload progress:" +
        //         Math.round(event.loaded / event.total * 100) +
        //         "%"
        //     )
        //     console.log(event);
        //   }
        if (httpResponse.status === 204) {
          // console.log(galleryValid);
          if(galleryValid) {
            this.http.postFormData('gallery/' + this.id + '/add', fdGallery).subscribe(httpResponse => {
              if(httpResponse.status === 204) {
                swal.close();
                this.alert.showNotification(
                  this.msg_succcess_edited,
                  "success",
                  "notification"
                );
                this.removeItem();
                this.initLoadingData(true);
                this.images = [];
                this.videos = [];
              }
            }, err => {
              if(err.status === 400) {
                swal.close();
                this.alert.showNotification(this.msg_image_is_not_addded, 'danger', '');
              }
            });
        } else {
          swal.close();
          this.alert.showNotification(
            this.msg_succcess_edited,
            "success",
            "notification"
          );
          this.removeItem();
          this.initLoadingData(true);
          this.images = [];
          this.videos = [];
        }
      }
      },  error => {
        swal.close();
        if(error.status === 500) {
          this.alert.showNotification(this.msg_server_error, 'danger', 'notifications');
        }
      this.alert.showNotification(
        this.msg_server_error,
        "danger",
        "error"
      );
    });
  } else {
    swal.close();
    this.alert.showNotification(this.msg_uploaded_file_error, 'danger','');
  }
  }
  
  // destroying subscripions
  ngOnDestroy() {
    this.subscriptionlang.unsubscribe();
    this.subscriptionarea.unsubscribe();
    this.subscriptionparams.unsubscribe();
    this.subscriptiondata.unsubscribe();
    this.dragulaService.destroy("gallery-bag");
  }
}
