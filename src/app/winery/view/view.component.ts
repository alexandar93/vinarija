import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { PageEvent, MatPaginator } from '@angular/material';
import { NotificationsService } from '../../notifications/notifications.service';
import { HelperService } from '../../services/helper.service';
import { Constants } from '../../services/endpoint.service';
import { Gallery, GalleryRef, ImageItem, VideoItem, YoutubeItem } from '@ngx-gallery/core';
import { TranslateHelperService } from './../../services/translate-helper.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  // variables
  subscriptionData: Subscription;
  subscriptionparams: Subscription;
  id: number;
  serverData: any;
  languages: any[] = [];
  categoriesList: any[] = [];
  model: string = '';
  winesList: any[] = [];
  currentPage: number;
  totalPages: number;
  videoFile: string = null;
  logoImage: string = null;
  coverImage: string = null;
  categoryId: number;
  /** latitude */
  lat: number;
  /** longitude */
  lng: number;
  /** list of regions */
  regionsList: any[] = [];
  /** list of reons */
  reonsList: any[] = [];
  /** list of vinogorja */
  vinogorjaList: any[] = [];
  /** region name */
  regionName: string = '';
  /** reon name */
  reonName: string = '';
  /** vinogorje name */
  vinogorjeName: string = '';
  galleryId = 'mixed';
  galleryRef: GalleryRef = this.gallery.ref(this.galleryId);
  gallerycounter = null;
  galleryData: any[] = [];
  /** msgs */
  msg_server_error: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private http: HttpService,
    private route: ActivatedRoute,
    private alert: NotificationsService,
    private helperHttp: HelperService,
    private router: Router,
    private endpoint: Constants,
    private gallery: Gallery,
    private translate: TranslateHelperService,
    private loc: Location
  ) { 
    if(this.helperHttp.isWineryAdmin()) {
      const userData = JSON.parse(localStorage.getItem('user_data'));
      loc.subscribe((val) => {
         if(val.url !== '/winery/view/' + this.id) {
           return loc.replaceState('/winery/view/' + this.id);
         }
       });
    }
  } 

  ngOnInit() {
    this.translate.getTranslate('FILES_ALERT_MSG_SERVER_ERRROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.subscriptionparams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    this.subscriptionData = this.http.get('patch/initialize/winery/' + this.id, 1).subscribe(httpResponse => {
      if(httpResponse.status == 200) {
        this.serverData = httpResponse.json();
        this.videoFile = this.serverData.video;
        this.logoImage = this.serverData.logo_image;
        this.coverImage = this.serverData.cover_image;
        this.lat = this.serverData.pin.lat;
        this.lng = this.serverData.pin.lng;
        this.initRegions();
        this.serverData.languages.forEach(lang => {
          this.languages.push(lang);
        });
        this.serverData.categories.forEach(list => {
          this.categoriesList.push(list);
        });
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger' , '');
    })
    this.getImages();
  }
  /** loading images*/
  getImages(){
    this.http.get('gallery/' + this.id + '/get', 1).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        let res = httpResponse.json();
      res.forEach(file => {
        this.gallerycounter++;

        let ext = file.filename.substr(file.filename.lastIndexOf('.') + 1);
        if(ext==='jpg' || ext==='jpeg' || ext==='png') {
          this.galleryData.push({
            id: file.id,
            type: 'image',
            src: file.url,
            thumb: file.url
          });
        }
        if(ext === 'mp4') {
           this.galleryData.push({
            id: file.id,
            type: 'video',
            src: file.url,
            thumb: 'https://diypbx.com/wp-content/uploads/2016/02/video-placeholder.jpg'
          });
        }
            });
        const switchItem = (item) => {
      switch (item.type) {
        case 'image':
          return new ImageItem(item.src, item.thumb);
        case 'video':
          return new VideoItem(item.src, item.thumb);
        case 'youtube':
          return new YoutubeItem(item.src);
      }
      };
      this.galleryData.map(item => {
        this.galleryRef.add(switchItem(item));
        });
      }
    }); 
  }
  /** loading regions */
  initRegions() {
    this.http.get('area/dropdown/nested', 1).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        this.regionsList = httpResponse.json();
        this.regionsList.forEach(parent => {
          parent.children.forEach(childrenReon => {
            childrenReon.children.forEach(childrenVinogorje => {
              if(childrenVinogorje.id === this.serverData.area.id) {
                this.vinogorjeName = childrenVinogorje.name;
                this.reonName = childrenReon.name;
                this.regionName = parent.name;
              }
            });
          });
        });
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '');
    });
  }
  /** open edit page for this winery */
  editWinery() {
    this.router.navigate(['winery/edit/' + this.id]);
  }
  /** navigate to comment page and filter that page for this winery */ 
  listOfComments() {
    this.router.navigate(['/rate/' + 'winery/' + this.id]);
  }
  /** list of wines with searching filter model variable contain value from search input */
  searchWineList(page, id, string : string) {
    // error wont change on next wine kategories
    // if(this.currentPage === page) {
    //   return false;
    // }
    this.categoryId = id;
    // za promenu jezika u helper service ! dodati parametar za jezik! i ovde takodje 
    this.helperHttp.getWineInCategories(page, this.id, id, string).subscribe(res =>{
        //console.log(res.data)
        this.helperHttp.handleResponse(res);
        this.winesList = res.data;
        this.currentPage = res.current_page;
        // this.totalPages = Math.ceil(res.total / res.per_page);
        this.totalPages = res.total;
    }, err=>{
      this.alert.showNotification(this.msg_server_error, 'danger', '');
        // console.error(err);
    });
  }
  getStars(rating) {
    // Get the value
    var val = parseFloat(rating);
    // Turn value into number/100
    var size = val/5*100;
    return size + '%';
  }

}
