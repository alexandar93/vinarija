import { Component, OnDestroy, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import { SelectionModel, DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatTableDataSource } from '@angular/material';

import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import { Subscription } from "rxjs/Subscription";
import { HttpService } from "../services/http.service";
import { NotificationsService } from "../notifications/notifications.service";
import { WineryDataSource} from '../services/winery.datasource';
import { TranslateHelperService } from './../services/translate-helper.service';
import { HelperService } from './../services/helper.service';

declare var swal: any;
declare var $: any;

@Component({
  selector: "app-winery",
  templateUrl: "./winery.component.html",
  styleUrls: ["./winery.component.css"]
})

export class WineryComponent implements OnInit, AfterViewInit, OnDestroy {
  // variables
  subscription: Subscription; // subscription for list of  winery
  subscriptionLang: Subscription; // subscription for languages
  selectedLanguage: number; // language id 
  languages: any[] = []; // storage list of all languages
  defaultLanguage: number = 1; // default language srpski = 1
  total; // total number of pages
  pageSize; // how many elements per page
  sort: string = 'asc'; // sorting

  /** route depends on logged user */
  http_route: string;

  dataSource: WineryDataSource;
  displayedColumns = ['name', 'address', 'options', 'actions'];
  
  msg_server_error: string;
  msg_activate: string;
  msg_deactivate: string;
  msg_swal_delete_title: string;
  msg_swal_delete_text: string;
  msg_swal_success_delete_title: string;
  msg_swal_success_delete_text: string;
  msg_swal_unsuccess_p1: string;
  msg_swal_unsuccess_p2: string;
  msg_button_yes: string;
  msg_button_no: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private router: Router,
    private http: HttpService,
    private alert: NotificationsService,
    private translate: TranslateHelperService,
    private helper: HelperService
  ) {
    if(helper.isWineryAdmin()) {
      this.http_route = 'my/wineries';
    } else {
      this.http_route = 'get/winery';
    }
  }

  ngOnInit() {
    // translate paginator
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('WINERY_TABLE_ALERT_MSG_DEACTIVATE').then((val: string) => {
      this.msg_deactivate = val;
    });
    this.translate.getTranslate('WINERY_TABLE_ALERT_MSG_ACTIVATE').then((val: string) => {
      this.msg_activate = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_TABLE_ALERTS_DELETE_WINERY_TITLE').then((val: string) => {
      this.msg_swal_delete_title = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_TABLE_ALERTS_DELETE_WINERY_TEXT').then((val: string) => {
      this.msg_swal_delete_text = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_TABLE_ALERTS_DELETE_WINERY_SUCCESS_TITLE').then((val: string) => {
      this.msg_swal_success_delete_title = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_TABLE_ALERTS_DELETE_WINERY_SUCCESS_TEXT').then((val: string) => {
      this.msg_swal_success_delete_text = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_TABLE_ALERTS_DELETE_WINERY_UNSUCCESS_PART1').then((val: string) => {
      this.msg_swal_unsuccess_p1 = val;
    });
    this.translate.getTranslate('WINERY_ALERT_MSG_TABLE_ALERTS_DELETE_WINERY_UNSUCCESS_PART2').then((val: string) => {
      this.msg_swal_unsuccess_p2 = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_button_no = val;
    });
    
    // dataSource init
    this.dataSource = new WineryDataSource(this.http);
    this.dataSource.loadWineries(1, this.sort, this.defaultLanguage, this.http_route);
    
    this.selectedLanguage = this.defaultLanguage;
    // get all languages
    this.subscriptionLang = this.http.get("dropdown/language", 1).subscribe(httpResponse => {
      this.languages = httpResponse.json();
    });
    // fetch data for table
    this.subscription = this.http
      .get(this.http_route, this.selectedLanguage)
      .subscribe(httpResponse => {
        if(httpResponse.status === 200) {
            this.total = httpResponse.json().total;
            this.pageSize = httpResponse.json().per_page;
           }
        }, error => {
          this.alert.showNotification(this.msg_server_error, 'danger', '')
        });
  }
  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.loadWineryPage())
    ).subscribe();
  }
  loadWineryPage() {
    this.dataSource.loadWineries(
        this.paginator.pageIndex + 1, this.sort, this.selectedLanguage, this.http_route);
  }
  
  /**  */
  submitOptions(id, option, value) {
    const fd = new FormData();
    console.log(value);
    let msg = this.msg_activate;
    let msgType = 'success';
    let data = {
      recommended: null,
      highlighted: null
    }
    
    if(option === 'recommended') {
      value = value==1?0:1;
      data.recommended = value;
      delete data.highlighted;
      if(data.recommended == 0) {msg = this.msg_deactivate, msgType = 'warning'};
    }
    if(option === 'highlighted') {
      value = value==1?0:1;
      data.highlighted = value;
      delete data.recommended;
      if(data.highlighted == 0) {msg = this.msg_deactivate, msgType = 'warning'};
    }
    
    fd.append('json', JSON.stringify(data));
    this.http.postFormData('patch/winery/' + id, fd).subscribe(res => {
      if(res.status === 204) {
        this.alert.showNotification(msg, msgType, '');
        this.loadWineryPage();
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '')
    });
  }
  setRecommended(id, element) {
    // this.submitOptions()
    console.log(id, element);
    this.submitOptions(id, 'recommended', element.recommended);
  }
  setHighlighted(id, element) {
    this.submitOptions(id, 'highlighted', element.highlighted);
  }

  sortData(event) {
    console.log(event);
    if(event.direction == 'asc') {
      this.sort = 'asc';
      this.loadDataSource();
    }
    if(event.direction == 'desc') {
      this.sort = 'desc';
      this.loadDataSource();
    }
  }
  loadDataSource() {
    this.dataSource.loadWineries(this.paginator.pageIndex, this.sort, this.selectedLanguage, this.http_route);
    this.paginator.firstPage();
  }
      
  onChangeLanguage(id: number, name: string) {
    this.selectedLanguage = id;
    this.dataSource = new WineryDataSource(this.http);
    this.loadDataSource();
    this.subscription = this.http
      .get(this.http_route, this.selectedLanguage)
      .subscribe(httpResponse => {
        if(httpResponse.status === 200) {
            this.total = httpResponse.json().total;
            this.pageSize = httpResponse.json().per_page;
           }
        });
  }
  OnAddWinery() {
    this.router.navigate(["winery/add"]);
  }

  OnEditWinery(id) {
    this.router.navigate(["winery/edit", id]);
  }
  onViewWinery(id) {
    this.router.navigate(["winery/view", id]);
  }

  OnDeleteWinery(id, name) {
    swal({
      title: `${this.msg_swal_delete_title} ${name} ?`,
      text: this.msg_swal_delete_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_button_yes,
      buttonsStyling: false
    }).then(() => {
      this.http.delete("delete/winery/" + id).subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          swal({
            title: this.msg_swal_success_delete_title,
            text: `${name} ` + this.msg_swal_success_delete_text ,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          this.http.get(this.http_route, this.selectedLanguage).subscribe(data => {
            this.paginator.length = data.json().total;
          })
          
          this.dataSource = new WineryDataSource(this.http);

          this.loadDataSource();
        }
        
      }, error => {
        this.alert.showNotification(
          this.msg_swal_unsuccess_p1 + name + this.msg_swal_unsuccess_p2,
          "danger",
          "error"
        );
      });
    }, (dismiss) => {

    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscriptionLang.unsubscribe();
  }
}
