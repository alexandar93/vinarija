import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { WineryTable } from './winery.routing';

import { WineryComponent } from './winery.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTableModule, MatSortModule, MatPaginatorModule, MatProgressSpinnerModule, MatTooltipModule, MatCheckboxModule, MatMenuModule } from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { ViewComponent } from './view/view.component';
import { QuillModule } from 'ngx-quill';
import { DragulaModule } from 'ng2-dragula';
import { NgxUploaderModule } from 'ngx-uploader';
import { GalleryModule } from '@ngx-gallery/core';
import { TranslateModule } from '@ngx-translate/core';

// import { DataTablesModule } from 'angular-datatables';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(WineryTable),
        FormsModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        AgmCoreModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatCheckboxModule,
        DragulaModule,
        QuillModule,
        NgxUploaderModule,
        GalleryModule,
        MatMenuModule,
        TranslateModule
        // DataTablesModule
    ],
    declarations: [
        WineryComponent,
        EditComponent,
        AddComponent,
        ViewComponent
    ],
    exports: [
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatCheckboxModule,
        MatMenuModule
    ]
})

export class WineryModule {}