import { Routes } from '@angular/router';

import { WineryComponent } from './winery.component';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';

export const WineryTable: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: WineryComponent
        }]
    }, {
        path: '',
        children: [{
            path: 'edit/:id',
            component: EditComponent,
            pathMatch: 'full'
        }]
    }, {
        path: '',
        children: [{
            path: 'add',
            component: AddComponent,
            pathMatch: 'full'
        }]
    }, {
        path: '',
        children: [{
            path: 'view/:id',
            component: ViewComponent,
            pathMatch: 'full'
        }]
    }
];