import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  RequiredValidator
} from "@angular/forms";
import { HttpService } from "../../services/http.service";
import { Subscription } from "rxjs/Subscription";
import { validateConfig } from "@angular/router/src/config";
import { NotificationsService } from "../../notifications/notifications.service";
import { Globals } from "../../model/globals";
import { TranslateHelperService } from './../../services/translate-helper.service';
import { HelperService } from './../../services/helper.service';

declare var swal;

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.css"]
})
export class AddComponent implements OnInit, OnDestroy {
  /** Variables */
  wineForm: FormGroup; // formBuilder / formGroup
  subscriptionCategory: Subscription; // subscription for areas
  subscriptionArea: Subscription; // subscription for categories
  subscriptionWinery: Subscription; // subscription for winery
  subscriptionLang: Subscription; // subscription for languages
  subscriptionSort: Subscription // subscription for sorts of wine
  subscriptionClassification: Subscription; /** Subscription for list of classifications */
  defaultLanguage: number = 1; // default language srpski - id; 1
  categories: any[] = []; // storage result of get request for list of categories
  areas: any[] = []; // storage result of get request for list of areas
  wineryList: any[] = []; // storage result of get request for list of wineries
  transparent: number; // saljem 0 ili 1 za sliku da li je transparenta ili ne
  classes: any[] = []; // storage for all sorts(classes)
  wineImage: any = null; // store uploaded image
  languagesData: any[] = []; // store all languages
  classificationList: any[] = []; /** store for list of classification */
  isFormValid;
  /** list of regions */
  regionsList: any[] = [];
  /** list of reons */
  reonsList: any[] = [];
  /** wineries route for logged user */
  http_wineries_route: string;
  /** list of vinogorja */
  vinogorjaList: any[] = [];
  defaultLangs: any = {
    id: '',
    name: ''
}
  modules: any;

  /** messages */
  msg_server_error: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_file_image_is_not_valid: string;
  msg_format_image_is_not_valid: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_lang_unsuccess: string;
  msg_swal_loading: string;
  msg_success_created: string;
  msg_unsuccess_create: string;
  msg_submitted_image_invalid: string;



@ViewChild('f') myNgForm: any;
@ViewChild('bottleImage') bottlePath: any;

  constructor(
    private fb: FormBuilder, 
    private http: HttpService, 
    private alert: NotificationsService,
    private globals: Globals,
    private translate: TranslateHelperService,
    private helper: HelperService
  ) {
    this.modules = this.globals.editor_settings;
    if(helper.isWineryAdmin()) {
      this.http_wineries_route = 'my/wineries/dropdown';
    } else {
      this.http_wineries_route = 'dropdown/winery';
    }
  }

  ngOnInit() {

    /** translate */
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_file_image_is_not_valid = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_format_image_is_not_valid = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_lang_unsuccess = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_SUCCESS_CREATE').then((val: string) => {
      this.msg_success_created = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_UNSUCCESS_CREATE').then((val: string) => {
      this.msg_unsuccess_create = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_SUBMIT_INVALID_IMAGE').then((val: string) => {
      this.msg_submitted_image_invalid = val;
    })
    /** get all categories */  
    this.subscriptionCategory = this.http
      .get("dropdown/category", 1)
      .subscribe(httpResponse => {
        this.categories = httpResponse.json();
      });
    /** get all areas */
    this.subscriptionArea = this.http
      .get("area/dropdown/nested", 1)
      .subscribe(httpresponse => {
        this.regionsList = httpresponse.json();
      });
    /** get all sorts */
    this.subscriptionSort = this.http
      .get("dropdown/wineClass", 1)
      .subscribe(httpResponse => {
        this.classes = httpResponse.json();
      });
    /** get list of classifications */
    this.subscriptionClassification = this.http
      .get("dropdown/wineClassification", 1)
      .subscribe(httpResponse => {
        this.classificationList = httpResponse.json();
      })

    /** get all wineries */
    this.subscriptionWinery = this.http
      .get(this.http_wineries_route, 1)
      .subscribe(httpresponse => {
        this.wineryList = httpresponse.json();
      });
    /** get all languages */
    this.subscriptionLang = this.http
      .get("dropdown/language", 1)
      .subscribe(httpresponse => {
        this.languagesData = httpresponse.json();
        this.languagesData.forEach(item => {
          if(item.id == this.defaultLanguage) {
            this.defaultLangs.id = item.id;
            this.defaultLangs.name = item.name;
          }
        })
        this.addItem(this.defaultLangs);
      });
    // declaration formBuilder and all his attrib
    this.wineForm = this.fb.group({
      classes: [[], Validators.required],
      area_id: ["", Validators.required],
      category_id: ["", Validators.required],
      winery_id: ["", Validators.required],
      harvest_year: ["", Validators.required],
      alcohol: ["", Validators.required],
      serving_temp: ["", Validators.required],
      background: ["", Validators.required],
      classification_id: [""],
      recommended: [""],
      highlighted: [""],
      items: this.fb.array([]),
      languages: [""],
      listOfRegions: ["", Validators.required],
      listOfReons: ["", Validators.required],
      listOfVinogorja: ["", Validators.required]
    });
  }
  /** set value of dropdown for reons */
  dropdownValueRegion(id) {
    if(this.regionsList !== undefined) {
      this.regionsList.forEach(region => {
        if(region.id == id) {
          this.reonsList = region.children;
        }
      });
    }
  }
  /** set value of dropdown for vinogorje */
  dropdownValueReon(id) {
    if(this.reonsList !== undefined) {
      this.reonsList.forEach(reon => {
        if(reon.id == id) {
          this.vinogorjaList = reon.children;
        }
      });
    }
  }
  /** set value for final selected region */
  dropdownValueVinogorje(id) {
    this.wineForm.controls.area_id.setValue(id);
  }
  tempFormatting() {
    this.wineForm.controls.serving_temp.setValue(Math.round(this.wineForm.controls.serving_temp.value));
  }
  alcoholFormatting() {
    let half = this.wineForm.controls.alcohol.value;
    this.wineForm.controls.alcohol.setValue( (Math.round(half * 2) / 2).toFixed(1));
  }
  // on uploading image 
  onUploadWineImage(event) {
    let file = <File>event.target.files[0];
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_file_image_is_not_valid, 'danger', '');
      this.isFormValid = false;
      return false;
    }
    if(!this.validateImage(file.name)) {
      this.alert.showNotification(this.msg_format_image_is_not_valid, 'danger', '');
      this.isFormValid = false;
      return false;
    } else {
      this.isFormValid = true;
      this.wineImage = <File>event.target.files[0];
    }
  }
  // func for validation uploaded image
  validateImage(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'png') {
      return true;
    } 
    if (ext.toLowerCase() == 'jpg') {
      return true;
    } 
    if (ext.toLowerCase() == 'jpeg') {
      return true;
    } 
      return false;
  }
  // removing selected language description and name from array / display
  onRemoveLangs(languageName, languageId, index) {
    let selected = this.wineForm.get("items") as FormArray;

    swal({
      title: this.msg_delete_lang_title + ` ${languageName}`,
      text: this.msg_delete_lang_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        swal({
          title: this.msg_delete_lang_success_title,
          text: `${languageName} ` + this.msg_delete_lang_success_text,
          type: "success",
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        });
        selected.removeAt(index);
        let renewLang = {
          name: languageName,
          id: languageId
        };
        this.languagesData.push(renewLang);
      },
      error => {
        this.alert.showNotification(
          this.msg_delete_lang_unsuccess,
          "danger",
          "error"
        );
      }
    );
  }
  // function for creating interface for FormArray items
  createLanguage(languageId, languageName): FormGroup {
    return this.fb.group({
      wineName: ["", Validators.required],
      wineDesc: ["", Validators.required],
      language_id: [languageId],
      language_name: [languageName]
    });
  }
  // function for adding new language in FormArray
  addItem(value): void {
    let items = this.wineForm.get("items") as FormArray;
    items.push(this.createLanguage(value.id, value.name));
    // let index = this.languagesData.indexOf(value.id, 0);
    let index = this.languagesData.findIndex(e => e.id==value.id);
    this.languagesData.splice(index, 1);
  }
  resetForm() {
    this.myNgForm.resetForm();
  }
  // submitting form 
  onSubmit() {
    const fd: FormData = new FormData();
    let langForm = this.wineForm.get('items') as FormArray;
    let languages: any[] = [];

    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      languages.push({
        language_id: formGroup.controls.language_id.value,
        name: "description",
        value: formGroup.controls["wineDesc"].value
      });
      languages.push({
        language_id: formGroup.controls.language_id.value,
        name: "name",
        value: formGroup.controls["wineName"].value
      });
    });
    this.wineForm.controls.languages.setValue(languages);
    
    let isRecommended = this.wineForm.controls.recommended.value;
    let isHighlighted = this.wineForm.controls.highlighted.value;

    if(isRecommended == true) {
      this.wineForm.controls.recommended.setValue(1);
    }
    if(isRecommended == false) {
      this.wineForm.controls.recommended.setValue(0);
    }
    if(isHighlighted == true) {
      this.wineForm.controls.highlighted.setValue(1);
    }
    if(isHighlighted == false) {
      this.wineForm.controls.highlighted.setValue(0);
    }

    let formInput = this.wineForm.value;
    delete formInput.items;
    delete formInput.listOfRegions;
    delete formInput.listOfReons;
    delete formInput.listOfVinogorja;

    if (this.wineImage !== undefined) {
      fd.append("bottle", this.wineImage);
    }
    fd.append("json", JSON.stringify(formInput));
    if(this.isFormValid) {
      swal({title: this.msg_swal_loading, allowOutsideClick: false});
      swal.showLoading();
    this.http.postFormData("create/wine", fd).subscribe(
      httpResponse => {
        if (httpResponse.status === 201) {
          swal.close();
          let controls = this.wineForm.get('items') as FormArray;
          while (controls.length !== 0) {
            controls.removeAt(0);
          }
          this.http.get('dropdown/language', 1).subscribe(lang => {
            this.languagesData = lang.json();
            this.addItem(this.defaultLangs);
          });
          this.wineImage = null;
          languages = [];
          this.bottlePath.nativeElement.click();
          this.resetForm();
          this.alert.showNotification(
            this.msg_success_created,
            "success",
            "notification"
          );
        }
      },
      error => {
        swal.close();
        this.alert.showNotification(
          this.msg_unsuccess_create,
          "danger",
          "error"
        );
      }
    );
  } else {
    this.alert.showNotification(this.msg_submitted_image_invalid, 'danger', '');
  }
  }
  // destroying all subscriptions
  ngOnDestroy(): void {
    this.subscriptionArea.unsubscribe();
    this.subscriptionCategory.unsubscribe();
    this.subscriptionWinery.unsubscribe();
    this.subscriptionLang.unsubscribe();
  }
}
