import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  RequiredValidator,
  FormGroupDirective,
  FormControl,
  NgForm,
  NgModel
} from "@angular/forms";
import { Location } from '@angular/common';
import { Subscription } from "rxjs/Subscription";
import { HttpService } from "../../services/http.service";
import { NotificationsService } from "../../notifications/notifications.service";
import { ActivatedRoute } from "@angular/router";
import { ErrorStateMatcher } from "@angular/material/core";
import { VIEWPORT_RULER_PROVIDER_FACTORY } from "@angular/cdk/scrolling";
import { Globals } from "../../model/globals";
import { TranslateHelperService } from './../../services/translate-helper.service';
import { ImageVideoValidatorsService } from './../../services/image-video-validators.service';
import { HelperService } from './../../services/helper.service';

declare var swal; // declaration for alert modals

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"]
})
export class EditComponent implements OnInit {
  /** Variables */
  wineForm: FormGroup; // formBuilder / formGroup
  subscriptionCategory: Subscription; // subscription for areas
  subscriptionArea: Subscription; // subscription for categories
  subscriptionWinery: Subscription; // subscription for winery
  subscriptionLang: Subscription; // subscription for languages
  subscriptionData: Subscription; // subscription for server-data patch init
  subscriptionParams: Subscription; // subscription for get params from URL
  subscriptionClassification: Subscription; /** subscription for list of classifications */
  categories: any[] = []; // storage result of get request for list of categories
  areas: any[] = []; // storage result of get request for list of areas
  wineryList: any[] = []; // storage result of get request for list of wineries
  languagesData: any[] = []; // storage result of all languages
  classificationList: any[] = []; /** storage result of list classifications */
  serverData: any; // storage result get data for patch init
  id: number; // storage id for selected wine
  matcher = new MyErrorStateMatcher(); // error message for Angular Material Inputs
  uploadedImage;
  bottleImage: string = null; // image
  saved = false; // language saved
  modules: any;
  classes: any[] = [];
  selectedClasses: any[] = [];
  /** list of regions */
  regionsList: any[] = [];
  /** list of reons */
  reonsList: any[] = [];
  /** list of vinogorja */
  vinogorjaList: any[] = [];
  /** is image valid */
  isFormValid = true;
  /** wineries route for logged user */
  http_wineries_route: string;

  /** translate strings */
  msg_alert_save_previous_lang: string;
  msg_success_saved_lang: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;
  msg_delete_lang_title: string;
  msg_delete_lang_text: string;
  msg_delete_lang_success_title: string;
  msg_delete_lang_success_text: string;
  msg_delete_unsuccess: string;
  msg_server_error: string;
  msg_swal_loading: string;
  msg_alert_file_is_not_image: string;
  msg_alert_file_is_not_format: string;
  msg_success_edited: string;
  msg_uploaded_file_invalid: string;


  @ViewChild("bottlePath") bottlePath: any;
  @ViewChild("selectedClass") selectedClass: any;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private alert: NotificationsService,
    private route: ActivatedRoute,
    private globals: Globals,
    private translate: TranslateHelperService,
    private validate: ImageVideoValidatorsService,
    private helper: HelperService,
    private loc: Location
  ) {
    this.modules = this.globals.editor_settings;
    if(helper.isWineryAdmin()) {
      this.http_wineries_route = 'my/wineries/dropdown';
    } else {
      this.http_wineries_route = 'dropdown/winery';
    }

    if(this.helper.isWineryAdmin()) {
      const userData = JSON.parse(localStorage.getItem('user_data'));
      loc.subscribe((val) => {
         if(val.url !== '/wines/edit/' + this.id) {
           return loc.replaceState('/wines/edit/' + this.id);
         }
       });
    }
  }

  ngOnInit() {

    /** translate */
    this.translate.getTranslate('LANG_ALERT_REQ_SAVE_PREVIOUS_LANG').then((val: string) => {
      this.msg_alert_save_previous_lang = val;
    });
    this.translate.getTranslate('LANG_ALERT_SUCCESS_SAVED').then((val:string) => {
      this.msg_success_saved_lang = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TITLE').then((val: string) => {
      this.msg_delete_lang_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_TEXT').then((val: string) => {
      this.msg_delete_lang_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_lang_success_title = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_lang_success_text = val;
    });
    this.translate.getTranslate('SWAL_DELETE_LANG_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('LOADING_TITLE').then((val: string) => {
      this.msg_swal_loading = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_IS_NOT_IMAGE').then((val: string) => {
      this.msg_alert_file_is_not_image = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_IMAGE_FILE_FORMAT').then((val: string) => {
      this.msg_alert_file_is_not_format = val;
    });
    this.translate.getTranslate('FILES_ALERT_MSG_INVALID_IMAGE').then((val: string) => {
      this.msg_uploaded_file_invalid = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_SUCCESS_EDITED').then((val: string) => {
      this.msg_success_edited = val;
    });

    // get get url params for wine ID
    this.subscriptionParams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    // get all categories
    this.subscriptionCategory = this.http
      .get("dropdown/category", 1)
      .subscribe(httpResponse => {
        this.categories = httpResponse.json();
      });
    // get all areas
    this.subscriptionArea = this.http
      .get("area/dropdown/nested", 1)
      .subscribe(httpresponse => {
        this.regionsList = httpresponse.json();
      });
    // get all wineries
    this.subscriptionWinery = this.http
      .get("dropdown/winery", 1)
      .subscribe(httpresponse => {
        this.wineryList = httpresponse.json();
      });
    // get all sorts(classes)
    this.subscriptionWinery = this.http
      .get("dropdown/wineClass", 1)
      .subscribe(httpresponse => {
        this.classes = httpresponse.json();
      });
    /** get list of classification */
    this.subscriptionClassification = this.http
      .get("dropdown/wineClassification", 1)
      .subscribe(httpResponse => {
        this.classificationList = httpResponse.json();
      })
    // get all languages
    this.subscriptionLang = this.http
      .get("dropdown/language", 1)
      .subscribe(httpresponse => {
        this.languagesData = httpresponse.json();
      });
      this.initLoadingData();
    // declaration formBuilder and all his attrib
    this.wineForm = this.fb.group({
      classes: ["", Validators.required],
      area_id: ["", Validators.required],
      category_id: ["", Validators.required],
      winery_id: ["", Validators.required],
      harvest_year: ["", Validators.required],
      alcohol: ["", Validators.required],
      serving_temp: ["", Validators.required],
      background: ["", Validators.required],
      recommended: [""],
      highlighted: [""],
      classification_id: [""],
      items: this.fb.array([]),
      languages: [""],
      listOfRegions: ["", Validators.required],
      listOfReons: ["", Validators.required],
      listOfVinogorja: ["", Validators.required]
    });
  }
  /** set value of dropdown for reons */
  dropdownValueRegion(id) {
    if(this.regionsList !== undefined) {
      this.regionsList.forEach(region => {
        if(region.id == id) {
          this.reonsList = region.children;
        }
      });
    }
  }
  /** set value of dropdown for vinogorje */
  dropdownValueReon(id) {
    if(this.reonsList !== undefined) {
      this.reonsList.forEach(reon => {
        if(reon.id == id) {
          this.vinogorjaList = reon.children;
        }
      });
    }
  }
  /** set value for final selected region */
  dropdownValueVinogorje(id) {
    this.wineForm.controls.area_id.setValue(id);
  }
  // get all data from server to set it on form
  initLoadingData(onSave = false) {
    this.subscriptionData = this.http
      .get("patch/initialize/wine/" + this.id, 1)
      .subscribe(
        httpResponse => {
          if (httpResponse.status === 200) {
            let serverData = (this.serverData = httpResponse.json());
            // filter only compare id's from both arrays.
            this.classes.forEach((a, i) => {
              this.serverData.classes.forEach((sd, s) => {
                if (a.id == sd.id) {
                  this.selectedClasses[s] = a;
                }
              });
            });
            this.wineForm.controls.classes.setValue(this.selectedClasses);
            this.wineForm.controls.area_id.setValue(this.serverData.area_id);
            this.wineForm.controls.background.setValue(
              this.serverData.background
            );
            this.wineForm.controls.harvest_year.setValue(
              this.serverData.harvest_year
            );
            this.wineForm.controls.alcohol.setValue(this.serverData.alcohol);
            this.wineForm.controls.winery_id.setValue(
              this.serverData.winery.id
            );
            this.wineForm.controls.category_id.setValue(
              this.serverData.category.id
            );
            this.wineForm.controls.serving_temp.setValue(
              this.serverData.serving_temp
            );
            if(serverData.recommended === 1) {
              this.wineForm.controls.recommended.setValue(true);
            }
            if(serverData.recommended === 0) {
              this.wineForm.controls.recommended.setValue(false);
            }
            if(serverData.highlighted === 1) {
              this.wineForm.controls.highlighted.setValue(true);
            }
            if(serverData.highlighted === 0) {
              this.wineForm.controls.highlighted.setValue(false);
            }
            this.regionsList.forEach(parent => {
              parent.children.forEach(childrenReon => {
                childrenReon.children.forEach(childrenVinogorje => {
                  if(childrenVinogorje.id === serverData.area.id) {
                    this.wineForm.controls.listOfVinogorja.setValue(childrenVinogorje.id);
                    this.wineForm.controls.listOfReons.setValue(childrenReon.id);
                    this.wineForm.controls.listOfRegions.setValue(parent.id);
                    this.wineForm.controls.area_id.setValue(this.wineForm.controls.listOfVinogorja.value);
                    this.reonsList = parent.children;
                    this.vinogorjaList = childrenReon.children;
                  }
                });
              });
            });
            this.wineForm.controls.classification_id.setValue(serverData.classification_id);
            
            this.bottleImage = this.serverData.bottle_image;
            serverData.languages.forEach((lang, langIndex) => {
              let name = "";
              let desc = "";
              let fieldsindex;
              let name_id;
              let desc_id;

              lang.fields.forEach((field, fieldIndex) => {
                fieldsindex = fieldIndex;
                if (field.name === "name") {
                  name = field.value;
                  name_id = field.id;
                }
                if (field.name === "description") {
                  desc = field.value;
                  desc_id = field.id;
                }
              });
              if (!onSave) {
                var index = this.languagesData.findIndex(
                  item => item.name === lang.language
                ); // SELECTING INDEX OF OBJECT IN ARRAY BY PROPERTY *(etc. name)
                this.languagesData.splice(index, 1);
              }
              let language_name = lang.language;
              let language_id = lang.language_id;
              this.createItem(
                name_id,
                desc_id,
                name,
                desc,
                language_name,
                language_id,
                false
              );
            });
          }
        },
        error => {}
      );
  }
  // creating new formGroup for languages inputs
  createLanguage(
    nameId,
    descId,
    wName,
    wDesc,
    lName,
    lId,
    isNew = true
  ): FormGroup {
    let fg = this.fb.group({
      wineName: [wName, Validators.required],
      wineDesc: [wDesc, Validators.required],
      name_id: nameId,
      desc_id: descId,
      language_name: [lName],
      language_id: [lId],
      flag: [""]
    });
    if (isNew) fg.controls["flag"].setValue(1);
    return fg;
  } 
  /** adding new item to FormArray items and splice item from langsArray */
  addItem(value): void {
    let isSaved;
    let items = this.wineForm.get("items") as FormArray;
    items.controls.forEach(item => {
      let items = item as FormGroup;
      if (items.controls.flag.value === 1) {
        isSaved = true;
        swal({
          title: this.msg_alert_save_previous_lang,
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success"
        });
        return;
      }
      isSaved = false;
    });
    if (!isSaved) {
      items.push(
        this.createLanguage(value.id, null, "", "", value.name, value.id)
      );
      let index = this.languagesData.indexOf(value, 0);
      this.languagesData.splice(index, 1);
    }
  }
  // remove all from items formArray
  removeItem() {
    let controls = this.wineForm.get("items") as FormArray;
    while (controls.length !== 0) {
      controls.removeAt(0);
    }
  }
  // generating new languages with data from server
  createItem(nameId, descId, wName, wDesc, lname, lid, isNew = true): void {
    let items = this.wineForm.get("items") as FormArray;
    items.push(
      this.createLanguage(nameId, descId, wName, wDesc, lname, lid, isNew)
    );
  }
  // saving when add new language
  onSaveLanguage(value) {
    let languageFormFields: any[] = [];
    languageFormFields.push({
      language_id: value.controls.name_id.value,
      name: "description",
      value: value.controls.wineDesc.value
    });
    languageFormFields.push({
      language_id: value.controls.name_id.value,
      name: "name",
      value: value.controls.wineName.value
    });
    let postData = {
      languages: languageFormFields
    };
    this.http
      .post("add/language/wine/" + this.id, postData)
      .subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          let itemArray = this.wineForm.controls["items"] as FormArray;
          itemArray.controls.forEach(element => {
            element.markAsUntouched;
          });
          this.alert.showNotification(
            this.msg_success_saved_lang,
            "success",
            "notifications"
          );
          this.removeItem();
          this.initLoadingData(true);
        }
      });
  }

  // uploading picture of wine
  onUploadWineImage(event) {
    let file = <File>event.target.files[0];
    let logoValid: any = {};
    logoValid.name = 'logo';
    if(file.type.indexOf('image') == -1) {
      this.alert.showNotification(this.msg_alert_file_is_not_image, 'danger', '');
      this.isFormValid = false;
      return false;
    }
    if(!this.validate.validateImage(file.name)) {
      this.alert.showNotification(this.msg_alert_file_is_not_format, 'danger', '');
      this.isFormValid = false;
      return false;
    } else {
      this.isFormValid = true;
      this.uploadedImage = <File>event.target.files[0];
    }
  }
  // removing language form
  onRemoveLangs(language_name, language_id, index) {
    let selected = this.wineForm.get("items") as FormArray;

    swal({
      title: this.msg_delete_lang_title + ` ${language_name}`,
      text: this.msg_delete_lang_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(
      () => {
        this.http
          .delete("delete/language/wine/" + this.id + "/" + language_id)
          .subscribe(
            httpResponse => {
              if (httpResponse.status === 204) {
                swal({
                  title: this.msg_delete_lang_success_title,
                  text: `${language_name} ` + this.msg_delete_lang_success_text,
                  type: "success",
                  confirmButtonClass: "btn btn-success",
                  buttonsStyling: false
                });
                selected.removeAt(index);
                let renewLang = {
                  name: language_name,
                  id: language_id
                };
                this.languagesData.push(renewLang);
              } else if (httpResponse.status !== 204) {
                this.alert.showNotification(
                  this.msg_delete_unsuccess,
                  "danger",
                  "notifications"
                );
              }
            },
            error => {
              this.alert.showNotification(
                this.msg_server_error,
                "danger",
                ""
              );
            }
          );
      },
      dismiss => {}
    );
  }
  // unsave() {
  //   this.saved = false;
  // }
  // submitting form

  // classesEvent(event) {
  //   console.log(event);
  // }

  onSubmit() {
    const fd: FormData = new FormData();

    let languages: any[] = [];
    let langForm = this.wineForm.get("items") as FormArray;

    // parse data from FormArray items to this.language array
    langForm.controls.forEach(element => {
      let formGroup = element as FormGroup;
      languages.push({
        id: formGroup.controls.desc_id.value,
        name: "description",
        value: formGroup.controls["wineDesc"].value
      });
      languages.push({
        id: formGroup.controls.name_id.value,
        name: "name",
        value: formGroup.controls["wineName"].value
      });
    });
    this.wineForm.controls.languages.setValue(languages);
    let isRecommended = this.wineForm.controls.recommended.value;
    let isHighlighted = this.wineForm.controls.highlighted.value;

    if(isRecommended == true) {
      this.wineForm.controls.recommended.setValue(1);
    }
    if(isRecommended == false) {
      this.wineForm.controls.recommended.setValue(0);
    }
    if(isHighlighted == true) {
      this.wineForm.controls.highlighted.setValue(1);
    }
    if(isHighlighted == false) {
      this.wineForm.controls.highlighted.setValue(0);
    }
    let formInput = this.wineForm.value;
    delete formInput.items;

    if (this.uploadedImage !== undefined) {
      fd.append("bottle", this.uploadedImage);
    }
    fd.append("json", JSON.stringify(formInput));
    // loading spinner on submit
    if(this.isFormValid) {
      // odkometarisati u koliko ima gresaka oko Sorta vina (classes)
    //   let classesArray: any[] = this.wineForm.controls.classes.value;
    //   console.log("array", classesArray);
    //   classesArray.forEach((classA, i) => {
    //     delete classA["name"];
    //   });
    // this.wineForm.controls.classes.setValue(classesArray);

    swal({ title: this.msg_swal_loading, allowOutsideClick: false });
    swal.showLoading();
    // patch
    this.http.postFormData("patch/wine/" + this.id, fd).subscribe(
      httpResponse => {
        //   if (event.type === HttpEventType.UploadProgress) {
        //     console.log(
        //       "Upload progress:" +
        //         Math.round(event.loaded / event.total * 100) +
        //         "%"
        //     )
        //     console.log(event);
        //   }
        if (httpResponse.status === 204) {
          swal.close();
          this.removeItem();
          // get all sorts(classes)
          this.subscriptionWinery = this.http
            .get("dropdown/wineClass", 1)
            .subscribe(httpresponse => {
              this.classes = httpresponse.json();
            });
          this.initLoadingData(true);
          this.alert.showNotification(
            this.msg_success_edited,
            "success",
            "notification"
          );
        }
      },
      error => {
        swal.close();
        this.alert.showNotification(
          this.msg_server_error,
          "danger",
          "error"
        );
      }
    );
  } else {
    swal.close();
    this.alert.showNotification(this.msg_uploaded_file_invalid, 'danger', '');
  }
  }
}
