import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { NotificationsService } from '../../notifications/notifications.service';
import { HelperService } from './../../services/helper.service';  

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit, OnDestroy {
  // variables
  subscriptionparams: Subscription;
  subscriptionData: Subscription;
  subscriptionCategory: Subscription;
  subscriptionArea: Subscription;
  subscriptionWinery: Subscription;
  subscriptionClasses: Subscription;

  id: number; // id wine
  serverData: any;
  languages: any[] = [];
  coverImage: string = null;
  categories: any;
  areas: any;
  wineryList: any;
  categoryName: string;
  areaName: string;
  wineryName: string;
  className: any[] = [];
  /** list of regions */
  regionsList: any[] = [];
  /** list of reons */
  reonsList: any[] = [];
  /** list of vinogorja */
  vinogorjaList: any[] = [];
  /** region name */
  regionName: string = '';
  /** reon name */
  reonName: string = '';
  /** vinogorje name */
  vinogorjeName: string = '';

  constructor(
    private http: HttpService,
    private route: ActivatedRoute,
    private alert: NotificationsService,
    private router: Router,
    private helper: HelperService,
    private loc: Location
  ) {
    if(this.helper.isWineryAdmin()) {
      const userData = JSON.parse(localStorage.getItem('user_data'));
      loc.subscribe((val) => {
         if(val.url !== '/wines/view/' + this.id) {
           return loc.replaceState('/wines/view/' + this.id);
         }
       });
    }
   }

  ngOnInit() {
    this.subscriptionparams = this.route.params.subscribe(
      params => (this.id = params.id)
    );
    this.subscriptionData = this.http.get('patch/initialize/wine/' + this.id, 1).subscribe(httpResponse => {
      this.serverData = httpResponse.json();

      this.coverImage = this.serverData.bottle_image;

      this.serverData.languages.forEach(lang => {
        this.languages.push(lang);
      });
      this.getAreas();
      this.getCategories();
      this.getWineries();
      this.getClasses();
      this.initRegions();

    }, err => {
      this.alert.showNotification('Greska na serveru, pokušajte kansije', 'danger', '');
    })
  }
   /** loading regions */
   initRegions() {
    this.http.get('area/dropdown/nested', 1).subscribe(httpResponse => {
      if(httpResponse.status === 200) {
        this.regionsList = httpResponse.json();
        this.regionsList.forEach(parent => {
          parent.children.forEach(childrenReon => {
            childrenReon.children.forEach(childrenVinogorje => {
              if(childrenVinogorje.id === this.serverData.area.id) {
                this.vinogorjeName = childrenVinogorje.name;
                this.reonName = childrenReon.name;
                this.regionName = parent.name;
              }
            });
          });
        });
      }
    }, err => {
      this.alert.showNotification('Greska na serveru, pokušajte kasnije', 'danger', '');
    });
  }
  getClasses() {
    this.subscriptionClasses = this.http.get('dropdown/wineClass', 1).subscribe(httpResponse => {
      let wineClasses = httpResponse.json();
      wineClasses.forEach((wineClass, i) => {
        this.serverData.classes.forEach((sd, s) => {
          if(wineClass.id == sd.id) {
            this.className[s] = wineClass.name;
          }
        });
      });
    });
  }
  getAreas() {
    this.subscriptionArea = this.http.get('dropdown/area', 1).subscribe(httpResponse => {
      let areaData = httpResponse.json();
      areaData.forEach(area => {
        if(area.id === this.serverData.area.id) {
          this.areaName = area.name;
        }
      })
    }, err => {
      this.alert.showNotification('Greska na serveru, pokušajte kasnije', 'danger', '');
    })
  }
  getCategories() {
    this.subscriptionCategory = this.http.get('dropdown/category', 1).subscribe(httpResponse => {
      let categoryData = httpResponse.json();
      categoryData.forEach(category => {
        if(category.id === this.serverData.category.id) {
          this.categoryName = category.name;
          // console.log(this.categoryName);
        }
      })
    }, err => {
      this.alert.showNotification('Greska na serveru, pokušajte kasnije', 'danger', '');
    })
  }
  getWineries() {
    this.subscriptionWinery = this.http.get('dropdown/winery', 1).subscribe(httpResponse => {
      let wineryData = httpResponse.json();
      wineryData.forEach(winery => {
        if(winery.id === this.serverData.winery.id) {
          this.wineryName = winery.name;
        }
      })
    }, err => {
      this.alert.showNotification('Greska na serveru, pokušajte kasnije', 'danger', '');
    })
  }
  // open edit page for this winery
  editWines() {
    this.router.navigate(['wines/edit/' + this.id]);
  }
  // navigate to comment page and filter that page for this winery
  listOfComments() {
    this.router.navigate(['/rate/' + 'wine/' + this.id]);
  }
  getStars(rating) {
    // Get the value
    var val = parseFloat(rating);
    // Turn value into number/100
    var size = val/5*100;
    return size + '%';
  }

  ngOnDestroy() {
    this.subscriptionparams.unsubscribe();
    this.subscriptionData.unsubscribe();
    this.subscriptionCategory.unsubscribe();
    this.subscriptionArea.unsubscribe();
    this.subscriptionWinery.unsubscribe();
  }

}
