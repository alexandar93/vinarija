import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { SelectionModel, DataSource } from '@angular/cdk/collections';
import { Subscription } from 'rxjs/Subscription';
import { debounceTime, distinctUntilChanged, startWith, tap, delay } from 'rxjs/operators';
import { NotificationsService } from '../notifications/notifications.service';
import { WineDataSource } from '../services/wine.datasource';
import { HttpService } from '../services/http.service';
import { Wine } from '../model/wine';
import { TranslateHelperService } from './../services/translate-helper.service';
import { HelperService } from './../services/helper.service';

declare var $;
declare var swal;

@Component({
  selector: 'app-wines',
  templateUrl: './wines.component.html',
  styleUrls: ['./wines.component.css']
})
export class WinesComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  subscriptionLang: Subscription;
  defaultLanguage: number = 1;
  selectedLanguage: number;
  languages: any[] = [];
  total;
  pageSize;
  sort: string = 'asc';
  /** route for logged user */
  http_route: string;

  dataSource: WineDataSource;
  displayedColumns = ['name', 'harvest_year', 'winery_name', 'options', 'actions'];
  
  /** messages */
  msg_server_error: string;
  msg_deactivated: string;
  msg_activated: string;
  msg_delete_title: string;
  msg_delete_text: string;
  msg_delete_success_title: string;
  msg_delete_success_text: string;
  msg_delete_unsuccess: string;
  msg_swal_button_yes: string;
  msg_swal_button_no: string;


  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private router: Router,
    private alert: NotificationsService,
    private http: HttpService,
    private translate: TranslateHelperService,
    private helper: HelperService
  ) { 
    if(helper.isWineryAdmin()) {
      this.http_route = 'my/wines';
    } else {
      this.http_route = 'get/wine';
    }
  }

  ngOnInit() {
    this.translate.getTranslate('TABLES_ELEMENTS_PER_PAGE').then((val: string) => {
      this.paginator._intl.itemsPerPageLabel = val;
    });
    this.translate.getTranslate('TABLES_PREVIOUS_PAGE_TOOLTIP').then((val: string) => {
      this.paginator._intl.previousPageLabel = val;
    });
    this.translate.getTranslate('TABLES_NEXT_PAGE_TOOPTIP').then((val: string) => {
      this.paginator._intl.nextPageLabel = val;
    });
    this.translate.getTranslate('ALERT_SERVER_ERROR').then((val: string) => {
      this.msg_server_error = val;
    });
    this.translate.getTranslate('WINES_TABLE_ALERT_MSG_DEACTIVATE').then((val: string) => {
      this.msg_deactivated = val;
    });
    this.translate.getTranslate('WINES_TABLE_ALERT_MSG_ACTIVATE').then((val: string) => {
      this.msg_activated = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_DELETE_TITLE').then((val: string) => {
      this.msg_delete_title = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_DELETE_TEXT').then((val: string) => {
      this.msg_delete_text = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_DELETE_SUCCESS_TITLE').then((val: string) => {
      this.msg_delete_success_title = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_DELETE_SUCCESS_TEXT').then((val: string) => {
      this.msg_delete_success_text = val;
    });
    this.translate.getTranslate('WINE_ALERT_MSG_DELETE_UNSUCCESS').then((val: string) => {
      this.msg_delete_unsuccess = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_YES').then((val: string) => {
      this.msg_swal_button_yes = val;
    });
    this.translate.getTranslate('ALERT_BUTTONS_NO').then((val: string) => {
      this.msg_swal_button_no = val;
    });
    // data for table init
    this.dataSource = new WineDataSource(this.http);
    this.dataSource.loadWines(1, this.sort, this.defaultLanguage, this.http_route);
    
    this.selectedLanguage = this.defaultLanguage;
    this.subscriptionLang = this.http.get("dropdown/language", 1).subscribe(httpResponse => {
      this.languages = httpResponse.json();
    });

    // fetch data  for table
    this.subscription = this.http
      .get(this.http_route, this.selectedLanguage)
      .subscribe(httpResponse => {
        if(httpResponse.status === 200) {
          this.total = httpResponse.json().total;
          this.pageSize = httpResponse.json().per_page;
        }
      }, error => {
        this.alert.showNotification(this.msg_server_error, 'danger', '');
      });
  }
  ngAfterViewInit() {
    this.paginator.page.pipe(
      tap(() => this.loadWinesPage())
    ).subscribe();
  }
  loadWinesPage() {
    this.dataSource.loadWines(
        this.paginator.pageIndex + 1, this.sort, this.selectedLanguage, this.http_route);
  }
  sortData(event) {
    if(event.direction === 'asc') {
      this.sort = 'asc';
      this.dataSource.loadWines(this.paginator.pageIndex, this.sort, this.selectedLanguage, this.http_route);
      this.paginator.firstPage();
    }
    if(event.direction === 'desc') {
      this.sort = 'desc';
      this.dataSource.loadWines(this.paginator.pageIndex, this.sort, this.selectedLanguage, this.http_route);
      this.paginator.firstPage();
    }
  }
  submitOptions(id, option, value) {
    const fd = new FormData();
    let msg = this.msg_activated;
    let msgType = 'success';
    let data = {
      recommended: null,
      highlighted: null
    }
    
    if(option === 'recommended') {
      value = value==1?0:1;
      data.recommended = value;
      delete data.highlighted;
      if(data.recommended == 0) {msg = this.msg_deactivated, msgType = 'warning'};
    }
    if(option === 'highlighted') {
      value = value==1?0:1;
      data.highlighted = value;
      delete data.recommended;
      if(data.highlighted == 0) {msg = this.msg_deactivated, msgType = 'warning'};
    }
    
    fd.append('json', JSON.stringify(data));
    this.http.postFormData('patch/wine/' + id, fd).subscribe(res => {
      if(res.status === 204) {
        this.alert.showNotification(msg, msgType, '');
        this.loadWinesPage();
      }
    }, err => {
      this.alert.showNotification(this.msg_server_error, 'danger', '')
    });
  }
  setRecommended(id, element) {
    // this.submitOptions()
    this.submitOptions(id, 'recommended', element.recommended);
  }
  setHighlighted(id, element) {
    this.submitOptions(id, 'highlighted', element.highlighted);
  }
  // sortWine(direction) {
  // }
  onChangeLanguage(id: number, name: string) {
    this.selectedLanguage = id;
    this.dataSource = new WineDataSource(this.http);
    this.dataSource.loadWines(this.paginator.pageIndex, this.sort, this.selectedLanguage, this.http_route);
    this.paginator.firstPage();

    this.subscription = this.http
      .get(this.http_route, this.selectedLanguage)
      .subscribe(httpResponse => {
        if(httpResponse.status === 200) {
          this.total = httpResponse.json().total;
          this.pageSize = httpResponse.json().per_page;
        }
      });
  }

  OnAddWine() {
    this.router.navigate(["wines/add"]);
  }

  OnEditWine(id) {
    this.router.navigate(["wines/edit", id]);
  }
  onViewWine(id) {
      this.router.navigate(["wines/view", id]);
  }

  OnDeleteWine(id, name) {
    swal({
      title: this.msg_delete_title + ` ${name} ?`,
      text: this.msg_delete_text,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: this.msg_swal_button_no,
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      confirmButtonText: this.msg_swal_button_yes,
      buttonsStyling: false
    }).then(() => {
      this.http.delete("delete/wine/" + id).subscribe(httpResponse => {
        if (httpResponse.status === 204) {
          swal({
            title: this.msg_delete_success_title,
            text: `${name} ` + this.msg_delete_success_text,
            type: "success",
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
          });
          this.http.get('get/wine', this.selectedLanguage).subscribe(data => {
            this.paginator.length = data.json().total;
          })
          
          this.dataSource = new WineDataSource(this.http);
          this.dataSource.loadWines(this.paginator.pageIndex, this.sort, this.selectedLanguage, this.http_route);
          this.paginator.firstPage();
        }
        error => {
          this.alert.showNotification(
            this.msg_delete_unsuccess,
            "danger",
            "error"
          );
        };
      });
    }, (dismiss) => {
      
    });

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscriptionLang.unsubscribe();
  }

}
