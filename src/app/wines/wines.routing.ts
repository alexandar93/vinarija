import { Routes } from '@angular/router';
import { WinesComponent } from './wines.component';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';


export const WinesTable: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: WinesComponent
        }]
    }, {
        path: '',
        children: [{
            path: 'edit/:id',
            component: EditComponent,
            pathMatch: 'full'
        }]
    }, {
        path: '',
        children: [{
            path: 'add',
            component: AddComponent,
            pathMatch: 'full'
        }]
    }, {
        path: '',
        children: [{
            path: 'view/:id',
            component: ViewComponent,
            pathMatch: 'full'
        }]
    }
];